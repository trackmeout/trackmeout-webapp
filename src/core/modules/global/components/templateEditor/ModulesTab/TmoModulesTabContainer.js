import React, {Component} from "react";
import {connect} from "react-redux";
import {allModulesTemplateSelector} from "../../../reducers/ModulesReducer";
import {allFormsTemplateSelector} from "../../../reducers/FormReducer";
import TmoFormComponent from "../../processus/TmoFormComponent";
import {fetchModuleTemplateById, fetchModuleTemplates, addFormToModule} from "../../../actions/ModuleAction";
import { fetchFormTemplates} from "../../../actions/FormAction";
import _ from "lodash";
import PropTypes from "prop-types";
import {Button, Card, Menu, Dropdown} from "antd";
import TmoTreeElementModalComposant, {
	ADD_MODULE_TEMPLATE_MODAL,
	ADD_FORM_TO_MODULE_TEMPLATE_MODAL, EDIT_MODULE_MODAL
} from "../../processus/TmoTreeElementModalComposant";

import {DELETE_MODULE_MODAL} from "../../common/TmoConfirmModal";


import {TEMPLATE_MODE} from "../../../Types";
import TmoConfirmModal from "../../common/TmoConfirmModal";

class TmoModulesTabContainer extends Component {
	constructor(props) {
		super(props);

		this.instanceData = this.props.nodeData;
		this.state = {
			activeModuleKey: null,
			activeModule: null,
			currentModule: null,
		};
		this.addNewModuleModal = null;
		this.formTemplatesList = [];
		this.shouldOpenModule = true;
	}

	componentWillMount = () => {
		const _this = this;
		_this.props.fetchModuleTemplates().then((item) => {
			const key = !!item.data.entities.modules ?  Object.keys(item.data.entities.modules)[0] : null;

			fetchModuleTemplateById(key).then((module) => {
				_this.setState({
					activeModuleKey: key,
					activeModule: module,
					currentModule : module,
				});
			});
		});

		_this.props.fetchFormTemplates().then((item) => {
			_this.formTemplatesList = [];
			_.each( item.data.entities.forms, function (current) {
				_this.formTemplatesList.push(current);
			});
		});

	};

	onModuleSelect = (item) => {

		if(this.shouldOpenModule){
			const _this = this;
			let moduleSelected = null;
			_.each(_this.moduleTemplates, (currentModule) => {
				if (currentModule._id === item.key) {
					moduleSelected = currentModule;
				}
			})
		}

		fetchModuleTemplateById(item.key).then((module) => {
			this.setState({
				activeModuleKey: item.key,
				activeModule: module
			});
		});
	};

	refreshPageFunction = () => {
		fetchModuleTemplateById(this.state.activeModuleKey).then((module) => {
			this.setState({
				activeModuleKey: this.state.activeModuleKey,
				activeModule: module
			});
		});
	};

	treeViewRightClick = (cm) => {
		this.setState({currentModule : cm});
		console.log(this.state.currentModule);
	};

	openTreeModal = (element) => {
		switch (element.item.props.modalType){
			case DELETE_MODULE_MODAL:
				this.deleteModuleModal.setState({
					isVisible: true
				});
				break;
			case EDIT_MODULE_MODAL:
				console.log("EDIT");
				this.editModuleModal.setState({
					isVisible: true
				});
				break;
			default:

		}
	};




	componentWillUpdate() {
		this.activeModule = _.find(this.props.modules, {id: this.state.activeModuleKey});
	}

	onFormTemplateClick = ( form ) => {
		const _this = this;
		addFormToModule({moduleId: this.state.activeModuleKey, formId:form._id }).then(( res ) =>{
			if(res.data.success === true){
				_this.refreshPageFunction();
			}
		});

	};

	addConvertScrollEvent = ( element ) => {
		if(element){
			var mouseSpeed = 0.75;
			element.addEventListener("mousewheel", (e) =>{
				e.preventDefault();
				element.scrollLeft += (e.deltaY * mouseSpeed);
			});
		}
	};

	render() {
		const _this = this;
		const menu = (
			<Menu onClick={this.openTreeModal}>
				<Menu.Item modalType={EDIT_MODULE_MODAL} key ="1">{this.context.t("Rename module")}</Menu.Item>
				<Menu.Item modalType={DELETE_MODULE_MODAL} key ="2">{this.context.t("Delete module")}</Menu.Item>
			</Menu>
		);

		const modules = this.props.modules.map((module) => {
			return (
				<Menu.Item key={module.id}>
					<Dropdown  onVisibleChange={this.visibilityChange} overlay={menu} trigger={["contextMenu"]}>
						<span key={module.id} onContextMenu={(() => this.treeViewRightClick(module))} className="tree-menu-line" style={{ userSelect: "none",  width:"175px" }}>{module.name}</span>
					</Dropdown>
					<TmoConfirmModal refreshPageFunction={_this.props.fetchModuleTemplates} ref={(child) => {_this.deleteModuleModal = child;}} modalData={this.state.currentModule} modalType={DELETE_MODULE_MODAL} />
				</Menu.Item>);
		});


		if (this.state.activeModule == null) {
			return (<div>
				<Button onClick={() => {
					this.addNewModuleModal.setState({isVisible: true});
				}} style={{border: "2px", textAlign: "right", marginTop:"10px", position: "absolute", right: "10px", top: "50px"}} icon="plus"
						size={"default"}>{this.context.t("Add a template")}</Button>
				<TmoTreeElementModalComposant refreshPageFunction={this.props.fetchModuleTemplates}
											  ref={(child) => {
												  _this.addNewModuleModal = child;
											  }} modalType={ADD_MODULE_TEMPLATE_MODAL}/>
				<div>{this.context.t("No module available")}</div>
			</div>);
		}



		const formTemplatesIdsAlreadyAdded = [];
		const currentModuleForms = this.state.activeModule.forms.map(function (form) {
			_.each(_this.formTemplatesList, function (currentTemplate) {
				if(currentTemplate._id == form.parentTemplate ){
					formTemplatesIdsAlreadyAdded.push(currentTemplate._id);
				}
			});

			return (<TmoFormComponent isPartOfOneTemplate={true}  refreshPageFunction={() => {
			}} key={form._id} data={form}/>);
		});



		const currentFormTemplates = _this.formTemplatesList.map(function (form) {
			return (<TmoFormComponent isPartOfOneTemplate={true}  onClickOnCard={() => {_this.onFormTemplateClick(form);}} refreshPageFunction={_this.refreshPageFunction} key={form._id} data={form}/>);
		});

		return (
			<div id="template-editor-module-container" className="tmo-flex-container">
				<div className="module-template-available-modules-list">
					<Card title={this.context.t("Template list")} bordered={false}>
						<div style={{minHeight: "580px"}}>
							<Menu selectedKeys={[this.state.activeModuleKey]} onSelect={this.onModuleSelect}
								  style={{marginTop: "10px"}}>
								{modules}
							</Menu>
							<Button onClick={() => {
								this.addNewModuleModal.setState({isVisible: true});
							}} style={{border: "none", width: "100%", textAlign: "left", marginTop: "10px"}} icon="plus"
									size={"default"}>{this.context.t("Add a template")}</Button>
							<TmoTreeElementModalComposant refreshPageFunction={this.props.fetchModuleTemplates}
														  ref={(child) => {
															  _this.addNewModuleModal = child;
														  }} modalType={ADD_MODULE_TEMPLATE_MODAL}/>
						</div>
					</Card>

				</div>
				<div style={{overflow: "auto"}} className="module-template-selected-module-container">
					<div className="tmo-relative-full-container tmo-flex-container">
						<div className="module-template-forms-in-module-list">
							<Card title={this.state.activeModule.name}
								  extra={<Button onClick={()=>{
									  this.addNewFormToModuleModal.setState({isVisible: true});
								  }}  icon="plus"
												 size={"default"}>{this.context.t("Add a form to this module")}</Button>}
								  bordered={false}>

								<TmoTreeElementModalComposant refreshPageFunction={_this.refreshPageFunction} param={this.state.activeModule._id}
															  ref={(child) => {
																  _this.addNewFormToModuleModal = child;
															  }} modalType={ADD_FORM_TO_MODULE_TEMPLATE_MODAL} modalData={this.state.activeModuleKey}/>
								<TmoTreeElementModalComposant refreshPageFunction={_this.refreshPageFunction}
															  ref={(child) => {
																  _this.editModuleModal = child;
															  }}  modalType={EDIT_MODULE_MODAL} modalData={this.state.currentModule}/>

								<div ref={(child) => { _this.wrapperContent1 = child; }} className="tmo-flex-wrapper-content">
									{currentModuleForms}
								</div>

							</Card>
						</div>
						<div className="module-template-forms-template-list tmo-flex-container-row">
							<Card title={this.context.t("Forms available")} bordered={false}>
								<div ref={(child) => { _this.wrapperContent2 = child; }} className="tmo-flex-wrapper-content">
									{ currentFormTemplates }
								</div>
							</Card>
						</div>
					</div>
				</div>
			</div>
		);
	}

	componentDidUpdate(){

		if(!!this.wrapperContent1){
			this.addConvertScrollEvent(this.wrapperContent1);
		}
		if(!!this.wrapperContent2){
			this.addConvertScrollEvent(this.wrapperContent2);
		}
	}
}

TmoModulesTabContainer.contextTypes = {
	t: PropTypes.func.isRequired
};

function mapStateToProps(state) {
	return {
		modules: allModulesTemplateSelector(state),
		lang: state.i18nState.lang
	};
}

export default connect(mapStateToProps, {fetchModuleTemplates, fetchFormTemplates})(TmoModulesTabContainer);
