import React, {Component} from "react";
import TmoModuleComponent from "../components/processus/TmoModuleComponent";
import TmoTreeMenuComponent from "../components/processus/TmoTreeMenuComponent";
import {allPatientProcessesSelector} from "../reducers/ProcessReducer";
import {fetchPatientProcesses, putFilter, createProcess} from "../actions/ProcessesAction";
import {Menu, Dropdown} from "antd";
import {fetchFormTemplates} from "../actions/FormAction";
import {fetchModuleTemplates} from "../actions/ModuleAction";
import {allFormsTemplateSelector} from "../reducers/FormReducer";
import {allModulesTemplateSelector} from "../reducers/ModulesReducer";
import {Layout} from "antd";
import TmoInputText from "../components/common/TmoInputText";
import {connect} from "react-redux";
import {Popover, Button, message} from "antd";
import PropTypes from "prop-types";
import TmoImportProcess from "../components/processus/TmoImportProcess";
import {READ_MODE} from "../Types";

const PROCESS_TYPE_NEW = "PROCESS_TYPE_NEW";
const PROCESS_TYPE_IMPORT = "PROCESS_TYPE_IMPORT";

class ProcessusPage extends Component {
  /* Constructions
        **********************************/
  constructor(props) {
    super(props);
    this.patientId = this.props.patientID;
    this.patientProcesses = [];
  }

  componentWillMount = () => {
    this.refreshProcessList();
    this.props.fetchFormTemplates();
    this.props.fetchModuleTemplates();
  };

  processMenuHandler = (element) => {
    switch (element.item.props.modalType) {
      case PROCESS_TYPE_NEW:
        this.popOverNewProcess.tooltip.onVisibleChange(true);
        break;
      case PROCESS_TYPE_IMPORT:
        this.importProcessModal.showModal();
        break;
    }

  };

  refreshProcessList = () => {
    const _this = this;
    _this.props.fetchPatientProcesses(this.patientId);
  };

  refreshPage = () => {
    const _this = this;
    setTimeout(function() {
      _this.refreshProcessList();
    }, 120);
  };

  closePopOverNewProcess = () => {
    this.popOverNewProcess.tooltip.onVisibleChange(false);
  };

  updateInputNewProcessValue = (newValue) => {
    this.newProcessName = newValue;
  }

  componentWillReceiveProps(nextProps) {
    this.patientProcesses = nextProps.patientProcesses;
  }

//Creation of a new process
  createNewProcess = () => {
    if (this.newProcessName !== "") {
      const process = {
        name: this.newProcessName,
        owner: this.patientId
      };

      createProcess(process);
      message.success(this.context.t("The process : ") + this.newProcessName + this.context.t(" was created correctly."));
      this.newProcessName = "";
      this.inputNewProcess.input.value = "";
      this.closePopOverNewProcess();
      this.refreshPage();
    } else {
      this.inputNewProcess.showError();
    }
    return;
  }

  render() {
    const _this = this;
    const {Sider, Content} = Layout;
    const newProcess = <div style={{
        marginBottom: 16,
        marginTop: 15
      }}>
      <TmoInputText ref={(child) => {
          this.inputNewProcess = child;
        }} data={{
          hintColor: "#999",
          focusLabelColor: "#108ee9",
          widthInput: "250px",
          label: this.context.t("Process name"),
          input: "",
          messageError: this.context.t("No value has been set")
        }} sendData={this.updateInputNewProcessValue}/>
      <Button type="primary" onClick={this.createNewProcess}>{this.context.t("Create the new process")}</Button>
    </div>;
    const addMenu = (<Menu onClick={this.processMenuHandler}>
      <Menu.Item modalType={PROCESS_TYPE_NEW} key="1">{(this.context.t("Create a new process"))}</Menu.Item>
      <Menu.Item modalType={PROCESS_TYPE_IMPORT} key="2">{(this.context.t("Import a process"))}</Menu.Item>
    </Menu>);
    const popOverNewProcess = <div style={{
        position: "absolute",
        right: "0",
        "top" : "-45px"
      }}>
      <Dropdown overlay={addMenu} trigger={["contextMenu", "click"]}>
        <Button>{(this.context.t("New process"))}</Button>
      </Dropdown>
      <Popover ref={(child) => {
          this.popOverNewProcess = child;
        }} placement="bottomRight" content={newProcess} trigger="click">
        <div style={{
            marginTop: "10px"
          }}></div>
      </Popover>
    </div>;

    if (Object.keys(this.patientProcesses).length > 0) {
      const processes = this.patientProcesses.map(function(process) {
        const modules = process.modules.map(function(module) {
          return (module.isVisible && <TmoModuleComponent currentMode={READ_MODE} refreshPageFunction={_this.refreshPage} key={module._id} data={module}/>);
        });
        return (process.isVisible && <div key={process.id}>
          <div>
            <h2 style={{
                marginTop: "0px",
                marginLeft: "0px",
                color: "#1890ff"
              }}>{process.name}</h2>
            {modules}
          </div>
        </div>);
      });

      return (<div style={{
          position: "relative"
        }}>
        {popOverNewProcess}
        <Layout>
          <Sider width="250" style={{
              background: "#f8f8f8"
            }}>
            <TmoTreeMenuComponent patientProcesses={this.patientProcesses} moduleTemplates={this.props.moduleTemplates} formTemplates={this.props.formTemplates} refreshPageFunction={this.refreshPage}/>
          </Sider>
          <Content style={{
              padding: "10px",
              background: "#fff"
            }}>
            {processes}
          </Content>
        </Layout>
        <TmoImportProcess patientID={this.patientId} refreshPageFunction={this.refreshPage} ref={(child) => {
            this.importProcessModal = child;
          }}/>
      </div>);
    }

    return (<div style={{
        position: "relative"
      }}>
      {popOverNewProcess}
      <TmoImportProcess patientID={this.patientId} refreshPageFunction={this.refreshPage} ref={(child) => {
          this.importProcessModal = child;
        }}/>
    </div>);
  }
}

ProcessusPage.contextTypes = {
  t: PropTypes.func.isRequired
};

function mapStateToProps(state) {
  return {patientProcesses: allPatientProcessesSelector(state), formTemplates: allFormsTemplateSelector(state), moduleTemplates: allModulesTemplateSelector(state), lang: state.i18nState.lang};
}

const mapDispatchToProps = {
  fetchPatientProcesses,
  fetchFormTemplates,
  fetchModuleTemplates
};

export default connect(mapStateToProps, mapDispatchToProps)(ProcessusPage);
