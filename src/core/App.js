import React from "react";
import TmoDashboard from "./modules/global/components/layout/TmoDefaulLayoutComponent";
import DashboardPage from "./modules/global/pages/DashboardPage";
import LoginPage from "./modules/global/pages/LoginPage";
import UserRoute from "./modules/global/routes/UserRoute";
import GuestRoute from "./modules/global/routes/GuestRoute";
import { Route,Switch, Redirect } from "react-router-dom";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import I18n from "redux-i18n";
import {translations} from "../locales/global";
import { withRouter } from 'react-router-dom'

//testing if the user is isAuthenticated
//Then generating GuestRoute ou UserRoute depending if the user is connected or not
const App = ({ location, isAuthenticated }) => (
	<div>
		<I18n translations={translations} >
			<GuestRoute
			 location={location}
			 path="/"
			 exact component={()=><LoginPage languages= {['Français', 'English', 'Deutsch']} />}
			/>
			<GuestRoute
			 location={location}
			 path="/login"
			 exact component={()=><LoginPage languages= {['Français', 'English', 'Deutsch']} />}
			/>
	    <UserRoute
	      path="/dashboard"
				location={location}
	      exact component={()=><TmoDashboard comp={"DashboardPage"}  languages= {['Français', 'English', 'Deutsch']} />}
	    />
		</I18n>
	</div>
);


App.propTypes = {
  isAuthenticated: PropTypes.bool.isRequired
};

function mapStateToProps(state) {
  return {
    isAuthenticated: !!state.user.email
  };
}

export default withRouter(connect(mapStateToProps)(App))
