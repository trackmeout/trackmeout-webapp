import { PROCESS_FETCHED,PROCESS_CREATED, PROCESS_UPDATED ,UPDATE_FILTER_LIST, FETCH_FILTER_LIST, PROCESS_EMPTY, PROCESS_TEMPLATE_FETCHED } from "../Types";
import api from "../utils/ApiRequests";
import { processSchema } from "../Schemas";
import {normalize} from "normalizr";

export const updateFilterList = data => ({
	type: UPDATE_FILTER_LIST,
	data
});

export const processFetched = data => ({
	type: PROCESS_FETCHED,
	data
});

export const processTemplateFetched = data => ({
	type: PROCESS_TEMPLATE_FETCHED,
	data
});

export const processCreated = data => ({
	type: PROCESS_CREATED,
	data
});

export const processEmpty = data => ({
	type: PROCESS_EMPTY,
	data
});

export const processUpdated = data => ({
	type: PROCESS_UPDATED,
	data
});

export const fetchPatientProcesses = ( patientId ) => dispatch => {
	api.processes.fetchPatientProcessesByPatientId( patientId ).then(process => {
		dispatch(processFetched(process === undefined ? [] : normalize(process, [processSchema])));
	});
};

export const archiveProcess = ( processId ) =>
	api.processes
		.archive(processId)
		//.then(processes => dispatch(processUpdated(normalize(processes, [processSchema]))));

export const convertProcessAsTemplate = ( process ) =>
	api.processes
		.convertAsTemplate(process);

export const createProcess = ( process ) =>
	api.processes.create(process);

export const addModuleToProcess = ( data ) =>
	api.processes
		.addModuleToProcess( data );

export const addModuleTempToProcessTemp = ( id, data ) =>
	api.processes.addModuleTempToProcessTemp( id, data );

export const editProcess = ( process ) =>
	api.processes.edit(process);

export const addProcessesFromTemplate = ( processes, patientID ) =>
	api.processes.addProcessesFromTemplate(processes, patientID).then( res => { return res } );

export const emptyProcess = data => dispatch => dispatch(processEmpty(data));

export const updateProcess = ( process ) => api.processes.update( process );

export const updateFilter = data => dispatch => dispatch(updateFilterList( normalize(data, [processSchema])));

export const fetchProcessTemplatesWithPromise = () =>
	api.processes.fetchProcessTemplates().then( res => { return res } );

export const fetchProcessTemplates = () => dispatch =>
    api.processes
        .fetchProcessTemplates()
        .then(processes => {
            return dispatch(processTemplateFetched(normalize(processes, [processSchema])));
        });
