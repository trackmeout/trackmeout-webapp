import { schema } from "normalizr";

//all schema - patient, processes, modules, tasks, form
export const patientSchema = new schema.Entity(
	"patients",
	{},
	{ idAttribute: "_id" }
);
export const processSchema = new schema.Entity(
	"processes",
	{},
	{ idAttribute: "_id" }
);
export const moduleSchema = new schema.Entity(
	"modules",
	{},
	{ idAttribute: "_id" }
);
export const taskSchema = new schema.Entity(
	"tasks",
	{},
	{ idAttribute: "_id" }
);
export const formSchema = new schema.Entity(
	"forms",
	{},
	{ idAttribute: "_id" }
);
