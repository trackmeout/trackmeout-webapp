import {normalize} from "normalizr";
import {MODULE_CREATED, MODULE_DELETED, MODULE_TEMPLATE_FETCHED, MODULE_BY_PROCESS_TEMPLATE_FETCHED} from "../Types";
import api from "../utils/ApiRequests";
import { moduleSchema } from "../Schemas";


const moduleCreated = data => ({
	type: MODULE_CREATED,
	data
});

const moduleDeleted = data => ({
	type: MODULE_DELETED,
	data
});

const moduleTemplatesFetched = data => ({
	type: MODULE_TEMPLATE_FETCHED,
	data
});

const moduleTemplatesByProcessFetched = data => ({
	type: MODULE_BY_PROCESS_TEMPLATE_FETCHED,
	data
});


export const fetchModuleTemplateById = (moduleId) =>
	api.modules
		.fetchModuleTemplateById(moduleId).then( (moduleTemplates) =>  moduleTemplates );

export const fetchModuleTemplatesByParentsId = (parentId) =>
	api.modules
		.fetchModuleTemplatesByParentsId(parentId).then( (moduleTemplatesParent) =>  moduleTemplatesParent );

export const fetchModuleTemplates = () => dispatch =>
	api.modules
		.fetchModuleTemplates()
		.then(moduleTemplates => {
			return dispatch(moduleTemplatesFetched(normalize(moduleTemplates, [moduleSchema]))); });

export const convertModuleAsTemplate = ( module ) =>
	api.modules
		.convertAsTemplate(module);

export const createModule = ( module ) =>
	api.modules
		.create( module );

export const addFormToModule = ( data ) =>
	 api.modules
		.addFormToModule( data );

export const addFormsToModules = ( data ) =>
	 api.modules
		.addFormsToModules( data );


export const addModulesFromTemplate = ( modules, processID ) =>
	api.modules.addModulesFromTemplate(modules, processID).then( res => { return res; } );


export const editModule = ( module ) =>
	api.modules
		.edit( module );

export const deleteModule = (module) =>
	api.modules
		.delete(module);

export const fetchModuleTemplatesWithPromise = () =>
	api.modules.fetchModuleTemplates().then( res => { return res } );
//.then(modules => dispatch(moduleDeleted(normalize(modules, [moduleSchema]))));
