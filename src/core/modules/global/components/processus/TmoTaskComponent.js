import React, { Component } from "react";
import { Row, Col, Badge, DatePicker, Select, Icon, Tooltip  } from "antd";
import TmoModal from "../common/TmoModal";
import TmoInputText from "../common/TmoInputText";
import TmoCreateTask from "../processus/TmoCreateTask";
import TmoUploadTask from "../processus/TmoUploadTask";
import TmoConfirmModal, { DELETE_TASK_MODAL } from "../common/TmoConfirmModal";
import { editTask } from "../../actions/TaskAction";
import {
    LIST, DATE, STRING, DEFAULT, SUCCESS, RESP_MODAL_OK, RESP_MODAL_CANCEL, EDIT_MODE,
    READ_MODE
} from "../../Types";
import PropTypes from "prop-types";
import { deleteTask } from "../../actions/TaskAction";
import _ from "lodash";
import moment from "moment";
import {connect} from "react-redux";
const Option = Select.Option;

class TmoTaskComponent extends Component {

	/* Constructions
  **********************************/
	constructor( props ) {
		super( props );
		this.state = {
			modalInputVisible: false,
			modalDateVisible: false,
			modalListVisible: false
		};
		this.newValue = "";
		this.childDataSet = null;
		this.sizeColumn = 0;
		this.task = this.props.data;
		this.childDataSet = this.task.childDataSet;

	}

	componentWillMount() {
		this.getCurrentListElement();
	}

	componentWillReceiveProps( nextProps ) {
		this.props = nextProps;
		this.task = this.props.data;
		this.getCurrentListElement();
	}

	/* Fonctions
  **********************************/
	showModalInput = () => {
		this.setState({ modalInputVisible: true });
	}

	showModalDate = () => {
		this.setState({ modalDateVisible: true });
	}

	showModalList = () => {
		this.setState({ modalListVisible: true });
	}

	showModalDeleteTask = () => {
		this.deleteTask.showModal();
	}

	showModalUpdateTask = () => {
		this.editTaskModal.showModalNewTask();
	}

	showModalUploadTask = () => {
		this.uploadModal.showModal();
	}

	getCurrentListElement = () => {
		if(this.task.type === LIST){
			this.childDataSet = this.task.childDataSet;
			if(this.childDataSet){
                const defaultValueID = this.task.defaultValueID;
                this.currentListItem = _.find( this.childDataSet[0].dataset, function(o) { return o.id === defaultValueID; } );
                this.task.state = this.currentListItem.status;
                this.task.value = this.currentListItem.title;
			}
		}
	}

	getData = ( newValue ) =>{
		this.newValue = newValue;
	}

	//Can choose between 3 choices, text, list or date

	getResponseModalInput = ( resp ) =>{
		//Faire test
		switch ( resp ) {
		case RESP_MODAL_OK:
			if(this.newValue.trim() !== ""){
				this.setState({ modalInputVisible:false });
				this.task.state = SUCCESS;
				this.task.value = this.newValue;
				editTask( this.task );
				this.props.refreshPageFunction();
			}
			else{
				this.setState({ modalInputVisible:false });
				this.task.state = DEFAULT;
				this.task.value = "-";
				editTask( this.task );
				this.props.refreshPageFunction();
			}
			break;
		case RESP_MODAL_CANCEL:
			this.setState({ modalInputVisible:false });
			break;
		default:
			break;
		}
	}

	getResponseModalList= (resp) =>{
		//Faire test
		switch (resp) {
		case RESP_MODAL_OK:
			this.setState({ modalListVisible:false });
			this.task.state = this.newValue.status;
			this.task.value = this.newValue.title;
			editTask(this.task);
			this.props.refreshPageFunction();
			break;
		case RESP_MODAL_CANCEL:
			this.setState({ modalListVisible:false });
			break;
		default:
			break;
		}
	}

	getResponseModalDate= ( resp ) =>{
		//Faire test
		switch ( resp ) {
		case RESP_MODAL_OK:
			if(this.newValue.trim() !== ""){
				this.setState({ modalDateVisible:false });
				this.task.state = SUCCESS;
				this.task.value = this.newValue;
				editTask( this.task );
				this.props.refreshPageFunction();
			  }
			else{
				this.setState({ modalDateVisible:false });
				this.task.state = DEFAULT;
				this.task.value = "-";
				editTask( this.task );
				this.props.refreshPageFunction();
			}
			break;
		case RESP_MODAL_CANCEL:
			this.setState({ modalDateVisible:false });
			break;
		default:
			break;
		}
	}

	onChangeDate = ( date, dateString ) => {
		this.newValue =  dateString;
	}

	setNewValueList = ( value ) => {
		this.newValue = _.find( this.childDataSet[0].dataset, function(o) { return o.id === value; });
		this.task.defaultValueID = this.newValue.id;
	}

	/* Render
  **********************************/
	render() {
		const modeEditionVisible = this.props.modeEdit;
		const { modalInputVisible, modalListVisible, modalDateVisible } = this.state;
		const nameForm = this.props.nameForm;
		let dataModalInput = null ;
		let dataModalDate = null ;
		let dataModaList = null ;
		let colorPDF = "";

		this.task.documents.length === 0 ?  colorPDF = {color:''} : colorPDF = {color:"limegreen"} ;

		this.sizeColumn=8;

		/************************** Create Modal **********************************************/
		switch (this.task.type) {
		//If text option selected, check that a value has been set
		case STRING:
			/************************* Modal Text **********************************************/
			const dataInput = {
					 label: this.context.t("New value"),
					 messageError: this.context.t("No value has been set"),
					 hintColor: "#999",
					 focusLabelColor: "#108ee9",
					 widthInput: "450px",
					 input: this.task.value === "-" ? "" : this.task.value
				 };

			const contentInput = <TmoInputText ref={(child) => { this.inputModalText = child; }} data = { dataInput } sendData={ this.getData } />;

			dataModalInput = {
					 title: nameForm+" / "+this.task.label,
					 typeContent: "react-component",
					 valueContent: contentInput,
					 closable: false,
					 input:"",
					 buttons:[
						 {
							 key: "cancel",
							 type: "",
							 text: this.context.t("Cancel"),
							 size:"large",
							 function: "handleCancel",
							 hasLoading: false
						 },
						 {
							 key: "ok",
							 type: "primary",
							 text:"Ok",
							 size:"large",
							 function: "handleOk",
							 hasLoading: false
						 }
					 ]
				 };
			break;
		case DATE:
			/************************* Modal Date **********************************************/
				//If date option selected, check the format
			const dateFormat = "DD.MM.YYYY";
			let valueDatepicker = this.task.value === "-" ? null :  moment( this.task.value, dateFormat );
			const contentDate = <DatePicker ref={(child) => { this.inputModalDate = child; }} onChange={ this.onChangeDate} defaultValue={ valueDatepicker } format={ dateFormat } />;

			dataModalDate = {
					 title: nameForm+" / "+this.task.label,
					 typeContent: "react-component",
					 valueContent: contentDate,
					 closable: false,
					 buttons:[
						 {
							 key: "cancel",
							 type: "",
							 text: this.context.t("Cancel"),
							 size:"large",
							 function: "handleCancel",
							 hasLoading: false
						 },
						 {
							 key: "ok",
							 type: "primary",
							 text:"Ok",
							 size:"large",
							 function: "handleOk",
							 hasLoading: false
						 }
					 ]
				 };
			break;
		case LIST:
			/************************* Modal List **********************************************/
					 const options = [];
					 if(this.childDataSet){
                         _.each( this.childDataSet[0].dataset, function ( currentItem ) {
                             options.push(<Option value={ currentItem.id } key={ currentItem.id }><Badge status={ currentItem.status } />{ currentItem.title }</Option>);
                         });
                     }

			const contentList = (
				<div>
					<Row style = {{ marginTop:30 }}>
						<Col span={12}> <h3> {this.context.t("Default value")} </h3> </Col>
						<Col span={12}>
							<Select style={{ width: 215 }} onChange={ this.setNewValueList } defaultValue={ this.task.defaultValueID } >
								{options}
							</Select>
						</Col>
					</Row>
				</div>
			);

			dataModaList = {
						 title: nameForm+" / "+this.task.label,
						 typeContent: "react-component",
						 valueContent: contentList,
						 closable: false,
						 buttons:[
							 {
								 key: "cancel",
								 type: "",
								 text:this.context.t("Cancel"),
								 size:"large",
								 function: "handleCancel",
								 hasLoading: false
							 },
							 {
								 key: "ok",
								 type: "primary",
								 text:"Ok",
								 size:"large",
								 function: "handleOk",
								 hasLoading: false
							 }
						 ]
					 };
			break;
		default:

		}

		//TEMPLATE OR NOT?
		const badge = (<Col key={11} span={ 1 } > <Badge status={ this.task.state } /> </Col>);
		const labelSize11 = (<Col key={12} span={ 11 } >{ this.task.label } </Col>);
		const labelSize12 = (<Col key={13} span={ 12 } >{ this.task.label } </Col>) ;
		if(this.task.textAide === undefined)
			this.task.textAide = "";

		this.isTemplateMode = !!this.task.isPartOfOneTemplate || !!this.props.isPartOfOneTemplate;

		/******************************* Tasks **********************************************/
		const resultTask = (
			<div>
				{(() => {
					switch(this.task.type) {
					case STRING:
						return (
							<Row gutter={ 8 } className="marginTask">
								{ !this.isTemplateMode  ? [badge , labelSize11] :  labelSize12  }
								<Col span={ this.sizeColumn }  >
									{ this.isTemplateMode  ? (<div style={{ textAlign: "right" }}>{ this.task.type }</div>) : (<div onClick={ this.showModalInput }>{ this.task.value }</div>) }
									{ modalInputVisible && (<TmoModal data= { dataModalInput } openModal={ modalInputVisible } sendResponseModal = { this.getResponseModalInput }/>) }
								</Col>
								{ (!modeEditionVisible &&  !this.isTemplateMode) &&  (<Col span={ 2 }  ><Icon style={colorPDF} type="file-pdf" className="editFormIcons" onClick={ this.showModalUploadTask }/></Col>)}
								{ (!modeEditionVisible &&  !this.isTemplateMode) &&  (<Col span={ 2 }  ><Tooltip title={this.task.textAide}> { this.task.textAide !== "" && <Icon type="question-circle-o" className="editFormIcons" /> }</Tooltip> </Col>)}
								{ modeEditionVisible &&  (<Col span={ 2 }  ><Icon type="edit" className="editFormIcons" onClick={ this.showModalUpdateTask }/> </Col>)}
								{ modeEditionVisible &&  (<Col span={ 2 }  ><Icon type="delete" className="editFormIcons" onClick={ this.showModalDeleteTask }/> </Col>)}
								</Row>);
					case DATE:
						return (
							<Row gutter={ 8 } className="marginTask">
								{ !this.isTemplateMode ? [badge , labelSize11] : labelSize12  }
								<Col span={ this.sizeColumn }  >
								  { this.isTemplateMode ? (<div style={{ textAlign: "right" }}>{ this.task.type }</div>) : (<div onClick={ this.showModalDate }>{ this.task.value }</div>)}
									{ modalDateVisible && (<TmoModal data= { dataModalDate } openModal={ modalDateVisible } sendResponseModal = { this.getResponseModalDate }/>) }
								</Col>
								{ (!modeEditionVisible &&  !this.isTemplateMode) &&  (<Col span={ 2 }  ><Icon type="file-pdf" style={colorPDF} className="editFormIcons" onClick={ this.showModalUploadTask }/> </Col>)}
								{ (!modeEditionVisible &&  !this.isTemplateMode) &&  (<Col span={ 2 }  ><Tooltip title={this.task.textAide}> { this.task.textAide !== "" && <Icon type="question-circle-o" className="editFormIcons" /> }</Tooltip> </Col>)}
								{ modeEditionVisible &&  (<Col span={ 2 }  ><Icon type="edit" className="editFormIcons" onClick={ this.showModalUpdateTask }/> </Col>)}
								{ modeEditionVisible &&  (<Col span={ 2 }  ><Icon type="delete" className="editFormIcons" onClick={ this.showModalDeleteTask }/> </Col>)}
								</Row>);
					case LIST:
						return (
							<Row gutter={ 8 } className="marginTask">
								{ !this.isTemplateMode ? [badge , labelSize11] : labelSize12 }
								<Col span={ this.sizeColumn }  >
									{ this.isTemplateMode ? (<div style={{ textAlign: "right" }}>{ this.task.type }</div>) : (<div onClick={ this.showModalList }>{ this.task.value }</div>)}
									{ modalListVisible && (<TmoModal data= { dataModaList } openModal={ modalListVisible } sendResponseModal = { this.getResponseModalList }/>) }
								</Col>
								{ (!modeEditionVisible &&  !this.isTemplateMode) &&  (<Col span={ 2 }  ><Icon type="file-pdf" style={colorPDF} className="editFormIcons" onClick={ this.showModalUploadTask }/> </Col>)}
								{ (!modeEditionVisible &&  !this.isTemplateMode) &&  (<Col span={ 2 }  ><Tooltip title={this.task.textAide}> { this.task.textAide !== "" && <Icon type="question-circle-o" className="editFormIcons"/> }</Tooltip> </Col>)}
								{ modeEditionVisible &&  (<Col span={ 2 }  ><Icon type="edit" className="editFormIcons" onClick={ this.showModalUpdateTask }/> </Col>)}
								{ modeEditionVisible &&  (<Col span={ 2 }  ><Icon type="delete" className="deleteFormIcons" onClick={ this.showModalDeleteTask }/> </Col>)}
								</Row>);
					default:
						return <div></div>;
					}
				})()}
			</div>);

		return (
			<div>
				{ resultTask }
				<TmoUploadTask  refreshPageFunction={ this.props.refreshPageFunction } ref={(child) => { this.uploadModal = child;}} data={ this.task } mode={ EDIT_MODE }/>
				<TmoCreateTask refreshPageFunction={ this.props.refreshPageFunction } ref={(child) => { this.editTaskModal = child;}} data={ this.task } mode={ EDIT_MODE }/>
				<TmoConfirmModal refreshPageFunction={ this.props.refreshPageFunction } ref={(child) => { this.deleteTask = child;}} modalData={ this.task._id } modalType={ DELETE_TASK_MODAL } />
			</div>
		);
	}
}

TmoModal.propTypes = {
	value: PropTypes.string,
	showModal: PropTypes.func
};

TmoTaskComponent.contextTypes = {
	t: PropTypes.func.isRequired
};

function mapStateToProps(state) {
	return {
		lang: state.i18nState.lang
	};
}



export default (TmoTaskComponent);
