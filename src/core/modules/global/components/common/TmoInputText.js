import React, {Component} from "react";

class TmoInputText extends Component {

	/* Constructions
    **********************************/
	constructor(props) {
		super(props);
		this.state = {
			errorVisible: false,
			colorText: this.props.data.hintColor
		};
}

	componentDidMount() {
		if(this.props.data.input !== '' || this.props.data.input!==undefined)
			this.setInputValue(this.props.data.input);
	}

	//Show error
	showError = () => {
		this.setState({
			errorVisible: true,
			colorText: "red"
		});
	}

	//Focus on selected element
	onFocus = () => {
		this.setState({
			colorText: this.props.data.focusLabelColor,
			errorVisible: false
		});
	}

	onBlur = () => {
		this.setState({
			colorText: this.props.data.hintColor
		});
	}

	updateInputValue = (e) => {
		this.props.sendData(e.target.value);
	}

	setInputValue = ( newValue ) => {
		this.input.value = newValue;
	}

	/* Render
    **********************************/
	render() {

		const { errorVisible, colorText } = this.state;
		const { label, messageError, widthInput, size } = this.props.data;
		return (
			<div className="group">
				<input  ref={(child) => { this.input = child; }} type="text" onFocus={this.onFocus} onBlur={this.onBlur} style={{ fontSize:size, display: "block", width: "100%"}} onChange={ this.updateInputValue } required/>
				<span className="highlight"></span>
				<span className="bar" style={{width: widthInput}}>
					<span className="bar-before" style={{background: colorText}}></span>
					<span className="bar-after" style={{background: colorText}}></span>
				</span>
				<label className="labelEffect" style={{color: colorText }}> {label} </label>
				<label className="errorInput"
					style={{visibility: errorVisible ? "visible" : "hidden"}}> {messageError} </label>
			</div>
		);
	}
}

export default TmoInputText;
