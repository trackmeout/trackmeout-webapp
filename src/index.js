import React from "react";
import ReactDOM from "react-dom";
import App from "./core/App";
import registerServiceWorker from "./core/registerServiceWorker";
import {Provider} from "react-redux";
import {applyMiddleware, createStore} from "redux";
import thunkMiddleware from "redux-thunk";
import promise from "redux-promise";
import rootReducer from "./core/rootReducer";
import {composeWithDevTools} from "redux-devtools-extension";
import { LocaleProvider } from "antd";
import frFR from "antd/lib/locale-provider/fr_FR";
import "antd/dist/antd.css";
import "./core/modules/global/css/global.css";
import { userLoggedIn } from "./core/modules/global/actions/UserAction";
import setAuthorizationHeader from "./core/modules/global/utils/setAuthorizationHeader";
import decode from "jwt-decode";
import { BrowserRouter, Route } from "react-router-dom";


const store = createStore(
	rootReducer,
	composeWithDevTools(
		applyMiddleware(thunkMiddleware, promise)
	)
);

if (localStorage.bookwormJWT) {

  const payload = decode(localStorage.bookwormJWT);

  const user = {
    token: localStorage.bookwormJWT,
    email: payload.email,
		firstname: payload.firstname,
		lastname: payload.lastname,
  };
  setAuthorizationHeader(localStorage.bookwormJWT);
  store.dispatch(userLoggedIn(user));
}

ReactDOM.render(
	<LocaleProvider locale={frFR}>
		<BrowserRouter>
			<Provider store={store}>
				<Route component={App} />
			</Provider>
		</BrowserRouter>
	</LocaleProvider>
	, document.getElementById("root")
);
registerServiceWorker();
