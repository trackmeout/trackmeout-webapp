import React, {Component} from "react";
import {  Icon } from "antd";
import _ from "lodash";
import TmoModal from "../common/TmoModal";
import {RESP_MODAL_CANCEL, RESP_MODAL_OK} from "../../Types";
import {TEMPLATE_MODE} from "../../Types";

import { addFormsToModules } from "../../actions/ModuleAction";

import TmoFormComponent from "./TmoFormComponent";
import TmoTreeElementModalComposant from "./TmoTreeElementModalComposant";
import PropTypes from "prop-types";

class TmoImportForm extends Component {

	//Class to import a form from template

	/* Constructions
  **********************************/
	constructor(props) {
		super(props);
		this.state = {
			modalVisible: false,
			moduleSelected: false,
			searchValue: ""
		};
	}

	componentWillReceiveProps(nextProps) {
		this.processModules = JSON.parse(JSON.stringify(nextProps.modalData));
		this.formTemplates = JSON.parse(JSON.stringify(this.props.formTemplates));
	}


    getResponseModalTask = (resp) => {
		const _this = this;
    	//Faire test
    	switch (resp) {
    	case RESP_MODAL_OK:
    	    var response = [];

    	    _.each(this.processModules.modules, ( module ) => {
                _.each(module.forms, ( form ) => {
                    if(form.isTemplate){
                        response.push({
                            moduleId: module.id,
                            formId: form.id
                        })
                    }
                });
            });

            addFormsToModules(response)
				.then(( response ) => {
					_this.props.refreshPageFunction();
                    this.setState({modalVisible: false});
				})
				.catch(( response ) =>{
					console.error(response);
				});

    		break;
    	case RESP_MODAL_CANCEL:
    		this.setState({modalVisible: false});
    		break;
    	default:
    		break;
    	}
    };

    selectModule = (module) => {
    	const { moduleSelected } = this.state;
    	if(moduleSelected && module.id === moduleSelected.id) { return; }

    	this.setState({moduleSelected: module});
    };

    addFormOnModule = ( form ) => {
    	this.state.moduleSelected.forms.push(form);
    	this.forceUpdate();
    };

    removeFormOnModule = ( formToDelete ) => {

        var indexToDelete = this.state.moduleSelected.forms.indexOf(formToDelete);
        this.state.moduleSelected.forms.splice(indexToDelete, 1);

        this.forceUpdate();
    };

    onSearchChange = ( event ) => {
        this.setState({searchValue: event.target.value});
    };

    getFormTemplatesNotUsed = () => {

    	const _this = this;
    	var results =  JSON.parse(JSON.stringify(this.formTemplates));
    	var currentIds = [];
    	_.each(this.state.moduleSelected.forms, ( current ) => {
            currentIds.push(current.id);
		});

        results = _.remove(results, function( currentForm ) {
            return !_.includes(currentIds,currentForm.id);
        });

        results = _.filter(results, function( currentForm ) {
            return _this.state.searchValue.trim() === "" || _.includes(currentForm.name.toLowerCase() ,_this.state.searchValue.toLowerCase());
        });

		return results;
    };

    addConvertScrollEvent = ( element ) => {
    	if(element){
    		var mouseSpeed = 0.75;

    		element.addEventListener("mousewheel", (e) =>{
    			e.preventDefault();
    			element.scrollLeft += (e.deltaY * mouseSpeed);
    		});
    	}
    };

    render() {
    	const _this = this;

        if(!this.processModules){
            return <div style={{display:"none"}}></div>;
        }

        const {modalVisible, moduleSelected} = this.state;
        var { modules } = this.processModules;

        if( _.isString(moduleSelected) ){
            var newModuleSelected = _.find(modules, function(currentModule) { return currentModule.id === moduleSelected; });
            this.setState({
				moduleSelected: newModuleSelected
			});
            this.forceUpdate();
        }

    	const modulesToRender = modules.map(function ( module ) {
    		const formsToRender = module.forms.map(function ( form ) {
    			const isNewFormClass = form.isTemplate ? "form-new" : "";
    			return  <li className={isNewFormClass} key={form.id} >
							<span>{ form.name }</span>
							<span  onClick={() =>  _this.removeFormOnModule( form ) }>
								<Icon className="btn-close" type="close" />
							</span>
						</li>;
    		});

    		const isSelectedClass = moduleSelected && module.id === moduleSelected.id ? "selected" : "";
    		return  <li className={isSelectedClass} key={module.id} onClick={() =>  _this.selectModule( module ) }>
    				<span>{ module.name }</span>
    				<ul className="forms">
    					{ formsToRender }
    				</ul>
    				</li>;
    	});

    	var formsToRender;
    	if(!moduleSelected){
    		formsToRender = <div className="aligner " style={{overflow: "auto", flex:"1 1"}}><p className="align-center text-not-selectable" style={{fontSize: "26px", color: "#d6d6d6"}}>{this.context.t("Please select a module to add forms")}</p></div>;
    	}else{

    		var formTemplatesToRender = this.getFormTemplatesNotUsed().map(function (form) {
    			return (<TmoFormComponent isPartOfOneTemplate={true} onClickOnCard={() => {_this.addFormOnModule(form);}} refreshPageFunction={_this.refreshPageFunction} key={form._id} data={form}/>);
    		});

    		formsToRender = (
    			<div className="forms-template-list">
    				<input type="text" value={this.state.searchValue} onChange={this.onSearchChange} placeholder={this.context.t("Text search")} style={{width: "100%"}}/>
    				<div style={{minHeight:"500px"}} className="tmo-flex-wrapper-content" ref={(child) => { _this.wrapperContent = child; }} >
    					{ formTemplatesToRender }
    				</div>
    			</div>
    		);
    	}

    	const contentModalChoice = (
    		<div className="tmo-flex-wrapper"  >
    			<ul className="tmo-flex-wrapper-menu module-list">
    				{ modulesToRender }
    			</ul>
    			{formsToRender}
    		</div>
    	);


    	var processName = this.processModules.name;
    	const dataModal = {
    		title: this.context.t("Importing forms in ") + processName,
    		typeContent: "react-component",
    		valueContent: contentModalChoice,
    		width: "90%",
    		bodyStyle: {
    			minHeight: "600px"
    		},
    		closable: false,
    		buttons: [
    			{
    				key: "cancel",
    				type: "",
    				text: this.context.t("Cancel"),
    				size: "large",
    				function: "handleCancel",
    				hasLoading: false
    			},
    			{
    				key: "ok",
    				type: "primary",
    				text: this.context.t("Save all changes"),
    				size: "large",
    				function: "handleOk",
    				hasLoading: false
    			}
    		]
    	};

    	return (
    		<div>
    			{modalVisible && (<TmoModal input="dada" data={dataModal} openModal={modalVisible}
    				sendResponseModal={this.getResponseModalTask}/>)}
    		</div>
    	);
    }

    componentDidUpdate(){
    	this.addConvertScrollEvent(this.wrapperContent);
    }
}

TmoImportForm.contextTypes = {
	t: PropTypes.func.isRequired
};

function mapStateToProps(state) {
	return {
		lang: state.i18nState.lang
	};
}

export default TmoImportForm;
