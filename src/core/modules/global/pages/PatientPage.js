import React, {Component} from "react";
import {Table} from "antd";
import PropTypes from "prop-types";
import {connect} from "react-redux";
import {allPatientsSelector} from "../reducers/PatientsReducer";
import TmoNewPatient from "../components/patient/TmoNewPatient";
import TmoProgressBar from "../components/common/TmoProgressBar";
import {fetchPatients} from "../actions/PatientAction";
import {emptyProcess} from "../actions/ProcessesAction";

class PatientPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showCreatePatient: false,
      patient: null
    };
    this.onCreatePatientClick = this.onCreatePatientClick.bind(this);
    this.onChange = this.onChange.bind(this);
    this.onRowClick = this.onRowClick.bind(this);
  }

  submit = (data) => {
    this.setState({patient: data});
  };

  componentDidMount = () => this.onInit(this.props);

  onInit = props => {
    props.fetchPatients();
    props.emptyProcess();
  };

  onCreatePatientClick = () => {
    this.setState({showCreatePatient: true});
  }

  onRowClick(patient, filters, sorter) {
    this.props.setPatient(patient.key, patient.lastName + " " + patient.firstName);

  }

  onChange(pagination, filters, sorter) {
    console.log("params", pagination, filters, sorter);
  }

  render() {
    const {patients} = this.props;
    const name = this.context.t("Name");
    const firstname = this.context.t("Firstname");
    const module = this.context.t("Module");
    const form = this.context.t("Form");
    const task = this.context.t("Task");

    const columns = [
      {
        title: name,
        dataIndex: "lastName",
        sorter: (a, b) => a.lastName.length - b.lastName.length
      }, {
        title: firstname,
        dataIndex: "firstName",
        sorter: (a, b) => a.firstName.length - b.firstName.length
      }, {
        title: module,
        dataIndex: "module",
        sorter: (a, b) => a.module.length - b.module.length
      }, {
        title: form,
        dataIndex: "form",
        sorter: (a, b) => a.form.length - b.form.length
      }, {
        title: task,
        dataIndex: "task",
        sorter: (a, b) => a.task.length - b.task.length
      }
    ];
    const dataSource = [];
    patients.map((patient) => {
      dataSource.push({
        key: patient._id, lastName: patient.lastName, firstName: patient.firstName, module: <TmoProgressBar nbDone={patient.totalModulesDone} nbTotal={patient.totalModules}/>,
        form: <TmoProgressBar nbDone={patient.totalFormsDone} nbTotal={patient.totalForms}/>,
        task: <TmoProgressBar nbDone={patient.totalTasksDone} nbTotal={patient.totalTasks}/>
      });
    });
    return (<div>
      <TmoNewPatient patient={this.state.patient}/>
      <Table columns={columns} dataSource={dataSource} onChange={this.onChange} onRowClick={this.onRowClick}/>
    </div>);
  }
}

PatientPage.propTypes = {
  fetchPatients: PropTypes.func.isRequired
};

PatientPage.contextTypes = {
  t: PropTypes.func.isRequired
}

function mapStateToProps(state) {
  return {patients: allPatientsSelector(state), lang: state.i18nState.lang};
}

export default connect(mapStateToProps, {fetchPatients, emptyProcess})(PatientPage);
