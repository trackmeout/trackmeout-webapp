import React, {Component} from "react";
import {message} from "antd";
import _ from "lodash";
import {editProcess, createProcess} from "../../actions/ProcessesAction";
import {createModule, editModule} from "../../actions/ModuleAction";
import {createForm, editForm} from "../../actions/FormAction";
import {convertProcessAsTemplate} from "../../actions/ProcessesAction";
import {convertModuleAsTemplate} from "../../actions/ModuleAction";
import {convertFormAsTemplate} from "../../actions/FormAction";
import TmoInputText from "../common/TmoInputText";
import TmoModal from "../common/TmoModal";
import {RESP_MODAL_CANCEL, RESP_MODAL_OK} from "../../Types";
import PropTypes from "prop-types";

export const CONVERT_PROCESS_MODAL = "CONVERT_PROCESS_MODAL";
export const CONVERT_MODULE_MODAL = "CONVERT_MODULE_MODAL";
export const CONVERT_FORM_MODAL = "CONVERT_FORM_MODAL";
export const IMPORT_PROCESS_MODAL = "IMPORT_PROCESS_MODAL";
export const IMPORT_MODULE_FROM_TEMPLATE = "IMPORT_MODULE_FROM_TEMPLATE";
export const ADD_MODULE_MODAL = "ADD_MODULE_MODAL";
export const ADD_MODULE_TEMPLATE_MODAL = "ADD_MODULE_TEMPLATE_MODAL";
export const EDIT_PROCESS_MODAL = "EDIT_PROCESS_MODAL";
export const EDIT_MODULE_MODAL = "EDIT_MODULE_MODAL";
export const EDIT_FORM_TASKS = "EDIT_FORM_TASKS";
export const ADD_FORM_MODAL = "ADD_FORM_MODAL";
export const IMPORT_FORM_MODAL_FROM_PROCESS = "IMPORT_FORM_MODAL_FROM_PROCESS";
export const IMPORT_FORM_MODAL_FROM_MODULE = "IMPORT_FORM_MODAL_FROM_MODULE";
export const ADD_FORM_TEMPLATE_MODAL = "ADD_FORM_TEMPLATE_MODAL";
export const ADD_FORM_TO_MODULE_TEMPLATE_MODAL = "ADD_FORM_TO_MODULE_TEMPLATE_MODAL";
export const EDIT_FORM_MODAL = "EDIT_FORM_MODAL";
export const ADD_TASK_MODAL = "ADD_TASK_MODAL";
export const ADD_TASK_MODAL_FROM_TEMPLATE = "ADD_TASK_MODAL_FROM_TEMPLATE";
export const ADD_PROCESS_TEMPLATE_MODAL = "ADD_PROCESS_TEMPLATE_MODAL";
export const ADD_MODULE_TO_PROCESS_TEMPLATE_MODAL = "ADD_MODULE_TO_PROCESS_TEMPLATE_MODAL";

class TmoTreeElementModalComposant extends Component {

	/* Constructions
    **********************************/
	constructor(props) {
		super(props);
		this.modalType = this.props.modalType;
		this.modalData = this.props.modalData;

		this.state = {
			isVisible: false,
		};
		this.error_modal = false;
	}

	getModalContent = () => {
		let dataInput = {
			hintColor: "#999",
			focusLabelColor: "#108ee9",
			widthInput: "450px",
			label: "Nom",
		};
		switch (this.modalType) {
			case CONVERT_PROCESS_MODAL: //Convert a process into template
				dataInput.messageError = this.context.t("No value has been set");
				dataInput.input = "";
				break;
			case CONVERT_MODULE_MODAL: //Convert a module into template
				dataInput.messageError = this.context.t("No value has been set");
				dataInput.input = "";
				break;
			case CONVERT_FORM_MODAL:  //Convert a form in tempalte
				dataInput.messageError = this.context.t("No value has been set");
				dataInput.input = "";
				break;
			case EDIT_PROCESS_MODAL: // Edit process
				// this.modalData = this.props.modalData();
				if(_.isFunction(this.props.modalData)){
					this.modalData = this.props.modalData();
				}


				if(this.modalData){
					dataInput.messageError = this.context.t("No value has been set");
					dataInput.input = this.modalData.name;
				}

				break;
			case ADD_MODULE_MODAL: // Add module
			case ADD_MODULE_TEMPLATE_MODAL: // Add module template
				dataInput.messageError = this.context.t("No value has been set");
				dataInput.input = "";
				break;
			case ADD_PROCESS_TEMPLATE_MODAL: // Add process template
				dataInput.messageError = this.context.t("No value has been set");
				dataInput.input = "";
				break;
			case EDIT_MODULE_MODAL: // Edit module
				dataInput.messageError = this.context.t("No value has been set");
				dataInput.input = this.modalData.name;
				break;
			case ADD_FORM_MODAL: // Add form
				dataInput.messageError = this.context.t("No value has been set");
				dataInput.input = "";
				break;
			case ADD_FORM_TEMPLATE_MODAL: // Add module template
				dataInput.messageError = this.context.t("No value has been set");
				dataInput.input = "";
				break;
			case ADD_FORM_TO_MODULE_TEMPLATE_MODAL : // Add form to a module template
				dataInput.messageError = this.context.t("No value has been set");
				dataInput.input = "";
				break;
			case ADD_MODULE_TO_PROCESS_TEMPLATE_MODAL : // Add module to a process template
				dataInput.messageError = this.context.t("No value has been set");
				dataInput.input = "";
				break;
			case EDIT_FORM_MODAL: // Edit form
				dataInput.messageError = this.context.t("No value has been set");
				dataInput.input = this.modalData.name;
				break;
			default:
				break;
		}
		return <TmoInputText data={dataInput} ref={(child) => {
			this.inputText = child;
		}} sendData={this.getData}/>;
	};

	getModalData = () => {

		let dataModal = {
			typeContent: "react-component",
			valueContent: this.getModalContent(),
			closable: false,
			buttons: [
				{
					key: "cancel",
					type: "",
					text: this.context.t("Cancel"),
					size: "large",
					function: "handleCancel",
					hasLoading: false
				},
				{
					key: "ok",
					type: "primary",
					text: this.context.t("Confirm"),
					size: "large",
					function: "handleOk",
					hasLoading: false
				}
			]
		};

		switch (this.modalType) {
			case CONVERT_PROCESS_MODAL:
				dataModal.title = this.context.t("Choosing a new name for this model");
				break;
			case CONVERT_MODULE_MODAL:
				dataModal.title = this.context.t("Choosing a new name for this model");
				break;
			case CONVERT_FORM_MODAL:
				dataModal.title = this.context.t("Choosing a new name for this model");
				break;
			case EDIT_PROCESS_MODAL:
				dataModal.title = this.context.t("Renaming process");
				break;
			case ADD_MODULE_TO_PROCESS_TEMPLATE_MODAL :
				dataModal.title = this.context.t("Add a new module template");
				break;
			case ADD_PROCESS_TEMPLATE_MODAL :
				dataModal.title = this.context.t("Add a new process template");
				break;
			case ADD_MODULE_MODAL :
				dataModal.title = this.context.t("Add new module");
				break;
			case EDIT_MODULE_MODAL :
				dataModal.title = this.context.t("Renaming module");
				break;
			case ADD_FORM_MODAL :
				dataModal.title = this.context.t("Add new form");
				break;
			case ADD_FORM_TEMPLATE_MODAL :
				dataModal.title = this.context.t("Add a new form template");
				break;
			case ADD_FORM_TO_MODULE_TEMPLATE_MODAL :
				dataModal.title = this.context.t("Add a new form to this module");
				break;
			case EDIT_FORM_MODAL :
				dataModal.title = this.context.t("Renaming form");
				break;
			default:
				break;
		}
		return dataModal;
	};

	getData = (newValue) => {
		this.name = newValue;
	};

	//Confirm if the option selected is successful or failed
	getModalResponse = (response) => {
		const _this = this;
		switch (response) {
			case RESP_MODAL_OK:
				switch (this.modalType) {
					case CONVERT_PROCESS_MODAL :
						if (this.name !== "" && this.name !== undefined) {
							const process = {
								name: this.name,
								id: this.modalData.id,
							};
							convertProcessAsTemplate(process).then(() => {
								message.success(this.context.t("The process has been correctly defined as a template, new name : ") + this.name);
							}).catch((err) => {
								message.error(this.context.t("The process could not be created"));
							});
						} else {
							this.inputText.showError();
							this.error_modal = true;
						}
						break;
					case CONVERT_MODULE_MODAL :
						if (this.name !== "") {
							const module = {
								name: this.name,
								id: this.modalData.id,
							};
							convertModuleAsTemplate(module).then(() => {
								message.success(this.context.t("The module has been correctly defined as a template, new name : ") + this.name);
							}).catch((err) => {
								message.error(this.context.t("The module could not be created"));
							});
						} else {
							this.inputText.showError();
							this.error_modal = true;
						}
						break;
					case CONVERT_FORM_MODAL :
						if (this.name !== "") {
							const form = {
								name: this.name,
								id: this.modalData.id,
							};
							convertFormAsTemplate(form).then(() => {
								message.success(this.context.t("The form has been correctly defined as a template, new name : ") + this.name);
							}).catch((err) => {
								message.error(this.context.t("The form could not be created"));
							});
						}
						else {
							this.inputText.showError();
							this.error_modal = true;
						}
						break;
					case EDIT_PROCESS_MODAL :
						if (this.name !== "" && this.name !== undefined) {
							const process = {
								name: this.name,
								id: this.modalData.id,
							};
							editProcess(process);
							message.success(this.context.t("The process was renamed correctly, new name : ") + this.name);
						} else {
							this.inputText.showError();
							this.error_modal = true;
						}
						break;
					case ADD_MODULE_TEMPLATE_MODAL :
						if (this.name !== "") {
							const module = {
								name: this.name,
								parentProcess: null,
								parentTemplate: null,
								isTemplate: true,
							};
							createModule(module);
							message.success(this.context.t("The module : ") + this.name + this.context.t(" was created correctly."));
						} else {
							this.inputText.showError();
							this.error_modal = true;
						}
						break;
					case ADD_MODULE_TO_PROCESS_TEMPLATE_MODAL :

						if (this.name !== "" && !!this.props.parentElement.state.selectedProcess) {
							const module = {
								name: this.name,
								parentProcess: this.props.parentElement.state.selectedProcess.id,
								parentTemplate: null,
								isTemplate: true,
							};
							createModule(module);
							message.success(this.context.t("The module : ") + this.name + this.context.t(" was created correctly."));
						} else {
							this.inputText.showError();
							this.error_modal = true;
						}
						break;
					case ADD_PROCESS_TEMPLATE_MODAL :
						if (this.name !== "") {
							const processus = {
								name: this.name,
								parentProcess: null,
								parentTemplate: null,
								isTemplate: true,
							};
							createProcess(processus);
							message.success(this.context.t("The process : ") + this.name + this.context.t(" was created correctly."));
						} else {
							this.inputText.showError();
							this.error_modal = true;;
						}
						break;
					case ADD_MODULE_MODAL :
						if (this.name !== "") {
							const module = {
								name: this.name,
								parentProcess: this.modalData.id,
							};
							createModule(module);
							message.success(this.context.t("The module : ") + this.name + this.context.t(" was created correctly."));
						} else {
							this.inputText.showError();
							this.error_modal = true;
						}
						break;
					case EDIT_MODULE_MODAL :
						if (this.name !== "") {
							const module = {
								name: this.name,
								id: this.modalData.id,
							};
							editModule(module).then(res => {
								message.success(this.context.t("The module was renamed correctly, new name : ") + this.name);
								_this.props.refreshPageFunction();
							});
						} else {
							this.inputText.showError();
							this.error_modal = true;
						}
						break;
					case ADD_FORM_MODAL :
						if (this.name !== "") {
							const form = {
								name: this.name,
								parentModule: this.modalData.id,
							};
							createForm(form);
							message.success(this.context.t("The form : ") + form.name + this.context.t(" was created correctly."));
						} else {
							this.inputText.showError();
							this.error_modal = true;
						}
						break;
					case ADD_FORM_TEMPLATE_MODAL :
						if (this.name !== "") {
							const form = {
								name: this.name,
								parentForm: null,
								parentTemplate: null,
								isTemplate: true,
							};
							createForm(form);
							message.success(this.context.t("The form : ") + this.name + this.context.t(" was created correctly."));
						} else {
							this.inputText.showError();
							this.error_modal = true;
						}
						break;

					case ADD_FORM_TO_MODULE_TEMPLATE_MODAL :
						if (this.name !== "") {
							const form = {
								name: this.name,
								parentForm: null,
								parentModule: this.props.param,
								parentTemplate: null,
								isTemplate: false,
								isPartOfOneTemplate: true,
							};
							createForm(form);
							message.success(this.context.t("The form : ") + this.name + this.context.t(" was created correctly in this module."));
						} else {
							this.inputText.showError();
							this.error_modal = true;
						}
						break;

					case EDIT_FORM_MODAL :
						if (this.name !== "") {
							const form = {
								name: this.name,
								id: this.modalData.id,
							};
							editForm(form);
							message.success(this.context.t("The form was renamed correctly, new name : ") + this.name);
						} else {
							this.inputText.showError();
							this.error_modal = true;
						}
						break;
					default:
						break;
				}


				if (this.error_modal === false) {
					this.setState({
						value: this.newValue,
						isVisible: false
					});

					setTimeout(() => {
						this.props.refreshPageFunction();
					}, 80);
				}

				break;
			case RESP_MODAL_CANCEL:
				this.setState({
					isVisible: false
				});
				break;
			default:
				break;
		}
	};

	/* Render
	  **********************************/
	render() {

		return (
			<TmoModal data={this.getModalData()} openModal={this.state.isVisible}
					  sendResponseModal={this.getModalResponse}/>
		);
	}
}

TmoTreeElementModalComposant.contextTypes = {
	t: PropTypes.func.isRequired
};


export default TmoTreeElementModalComposant;
