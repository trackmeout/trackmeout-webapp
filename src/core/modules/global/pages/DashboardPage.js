import React, {Component} from "react";
import {PropTypes} from "prop-types";
import {connect} from "react-redux";


class DashboardPage extends Component {


	render() {
		return (
			<div>
				<h1>{this.context.t("Dashboard")}</h1>
				<div>
					<h4>Comming Soon !!</h4>
				</div>
			</div>
		);
	}
}

DashboardPage.contextTypes = {
	t: PropTypes.func.isRequired
};

function mapStateToProps(state) {
	return {
		lang: state.i18nState.lang
	};
}

export default connect(mapStateToProps)(DashboardPage);
