import { TASK_DELETED, TASK_ADDED } from "../Types";
import api from "../utils/ApiRequests";

// data.entities.patients
const taskDeleted = data => ({
	type: TASK_DELETED,
	data
});

const taskAdded = data => ({
	type: TASK_ADDED,
	data
});

export const deleteTask = ( task ) =>
	api.tasks
		.delete( task );

export const editTask = ( task ) =>
	api.tasks
		.edit( task );

export const createTask = ( task ) =>
	api.tasks
		.create( task );
		//.then(tasks => dispatch(taskAdded(normalize(forms, [formSchema]))));
