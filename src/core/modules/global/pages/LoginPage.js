import React, {Component} from "react";
import {connect} from "react-redux";
import LoginForm from "../components/User/LoginForm";
import { login } from "../actions/UserAction";
import logo from "../media/img/logo.png";
import { Layout } from "antd";
import {setLanguage} from "redux-i18n";
import { bindActionCreators } from 'redux';
const {Header, Content, Footer} = Layout;

class LoginPage extends Component {

	componentWillMount() {
		if(this.props.lang === "" || this.props.lang === "en")
			this.props.dispatch(setLanguage("Français"));
	}


	onChangeLanguage = (e) => {
		this.props.dispatch(setLanguage(e.target.value));
	}

	render() {
		//rendering the loginpage with his form
		return (
			<Layout>
				<Header style={{background: "#fff", padding: 0}}>
					<div className=" headerLogoMenu ">
						<img src={logo} alt="logo" className=" logoTmo "/>
					</div>
					<div className=" headerUser ">
						<select className="select-style" style={{marginRight: "30px", width:"80px" }} value={this.props.lang} onChange={this.onChangeLanguage}>
							{this.props.languages.map(lang => <option key={lang} value={lang}> {lang} </option>)}
						</select>
					</div>
				</Header>
				<Content className="loginBody">
					<div align="center" style={{width: 250, margin: "0px auto", paddingTop: "150px"}}>
						<h1>Login</h1>
						<LoginForm />
					</div>
				</Content>
      	<Footer className="loginFooter">
					TrackMeOut ©2018
				</Footer>
			</Layout>
		);
	}
}

function mapStateToProps(state) {
	return {
		lang: state.i18nState.lang,
		isAuthenticated: !!state.user.email
	};
}

function mapDispatchToProps(dispatch) {
    const actionCreators = bindActionCreators({
        login,
    }, dispatch);
    return { ...actionCreators, dispatch };
}


export default connect(mapStateToProps, mapDispatchToProps)(LoginPage);
