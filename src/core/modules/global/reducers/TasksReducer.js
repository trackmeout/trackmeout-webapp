import {createSelector} from "reselect";
import { TASK_DELETED, TASK_ADDED } from "../Types";

//TASKS REDUCER
export default function tasks (state = {}, action = {}) {
	switch (action.type) {
	case TASK_DELETED:
		return {...state, ...action.data.entities.tasks};
	case TASK_ADDED:
		return {...state, ...action.data.entities.tasks};
	default:
		return state;
	}
}
