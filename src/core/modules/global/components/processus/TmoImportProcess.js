import React, {Component} from "react";
import { Checkbox, Icon, Col, Row, message  } from "antd";
import _ from "lodash";
import TmoModal from "../common/TmoModal";
import {RESP_MODAL_CANCEL, RESP_MODAL_OK} from "../../Types";
import {TEMPLATE_MODE} from "../../Types";
import { addFormsToModules } from "../../actions/ModuleAction";
import { fetchProcessTemplatesWithPromise, addProcessesFromTemplate } from "../../actions/ProcessesAction";
import TmoFormComponent from "./TmoFormComponent";
import TmoTreeElementModalComposant from "./TmoTreeElementModalComposant";
import PropTypes from "prop-types";

class TmoImportProcess extends Component {

	/* Constructions
  **********************************/
	constructor(props) {
		super(props);
		this.state = {
			modalVisible: false,
			checkedList: [],
	    indeterminate: true,
	    checkAll: false,
		};
		this.checked;
		this.listProcess={};
	}

	componentWillReceiveProps(nextProps) {
        fetchProcessTemplatesWithPromise().then(res => {
			this.listProcess = res;
		});
	}

	componentWillMount() {
        fetchProcessTemplatesWithPromise().then(res => {
			this.listProcess = res;
		});
	}

	showModal = () => {
		this.setState({ modalVisible : true});
	}

	onChange = (checkedValues) => {
		this.checked = checkedValues;
	}

  getResponseModalTask = (resp) => {
  	switch (resp) {
  	case RESP_MODAL_OK:
			addProcessesFromTemplate(this.checked, this.props.patientID).then( res => {
				if(res === true){
					message.success(this.context.t("Import done"));
					this.props.refreshPageFunction();
				}
				else{
					message.error(this.context.t("Import failed"));
				}
			});
  		break;
  	case RESP_MODAL_CANCEL:
  		break;
  	default:
  		break;
  	}
		this.setState({ modalVisible : false});
  };


  render() {
  	const { modalVisible }  = this.state;
		const CheckboxGroup = Checkbox.Group;


		//Generate list
		let list=[];
		if(this.listProcess.length > 0){
			for (let i = 0; i < this.listProcess.length; i++) {
			    list.push(<Col key={ this.listProcess[i]._id }><Checkbox  value={ this.listProcess[i]._id }>{ this.listProcess[i].name }</Checkbox></Col>);
			}
		}

  	const content = (
			<div style={{ overflowY: 'auto', height: '300px' }}>
        <br />
				<Checkbox.Group  onChange={this.onChange}>
			    <Row>
					  { list }
			    </Row>
			  </Checkbox.Group>,
      </div>
  	);



    //Popup to confirm the import of the process or not
  	const dataModal = {
  		title: this.context.t("Import new process"),
  		typeContent: "react-component",
  		valueContent: content,
  		width: "40%",
  		bodyStyle: {
  			height: "400px"
  		},
  		closable: false,
  		buttons: [
  			{
  				key: "cancel",
  				type: "",
  				text: this.context.t("Cancel"),
  				size: "large",
  				function: "handleCancel",
  				hasLoading: false
  			},
  			{
  				key: "ok",
  				type: "primary",
  				text: this.context.t("Import processes"),
  				size: "large",
  				function: "handleOk",
  				hasLoading: false
  			}
  		]
  	};

  	return (
  		<div>
  			{modalVisible && (<TmoModal data={dataModal} openModal={modalVisible} sendResponseModal={this.getResponseModalTask}/>)}
  		</div>
  	);
  }
}

TmoImportProcess.contextTypes = {
	t: PropTypes.func.isRequired
};

function mapStateToProps(state) {
	return {
		lang: state.i18nState.lang
	};
}

export default TmoImportProcess;
