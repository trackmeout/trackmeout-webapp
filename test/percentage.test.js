var Percentage = require("../lib/Percentage");
var assert = require("chai").assert;
var expect = require("chai").expect;

describe ("percentage", function () {

	describe("#evolution", function(){

		it("sould give an evolution", function(){

			expect(Percentage.evolution(100,200)).to.equal(100);
			expect(Percentage.evolution(100,150)).to.equal(50);
			expect(Percentage.evolution(100,50)).to.equal(-50);

		});

		it("should handle 0 evolution", function(){

			expect(Percentage.evolution(0,100)).to.equal(Infinity);

		});

		it("should round value", function(){

			expect(Percentage.evolution(30,100)).to.equal(233.33);

		});

	});

});