import {combineReducers} from "redux";
import {i18nState} from "redux-i18n";
import patientProcesses from "./modules/global/reducers/ProcessReducer";
import processes from "./modules/global/reducers/ProcessReducer";
import patients from "./modules/global/reducers/PatientsReducer";
import modules from "./modules/global/reducers/ModulesReducer";
import forms from "./modules/global/reducers/FormReducer";
import user from "./modules/global/reducers/UsersReducer";

const rootReducer = combineReducers({
	i18nState,
	patientProcesses,
	patients,
	modules,
	forms,
	user,
	processes
});

export default rootReducer;
