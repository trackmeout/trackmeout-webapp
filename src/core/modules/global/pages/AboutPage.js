import React, {Component} from "react";
import {PropTypes} from "prop-types";
import {connect} from "react-redux";
import jack from "../media/img/jack.png";
import tiago from "../media/img/tiago.png";
import christian from "../media/img/christian.png";
import jon from "../media/img/jon.jpeg";
import xavier from "../media/img/xavier.jpg";
import sam from "../media/img/sam.png";
import { Card, Row, Col} from 'antd';
const { Meta } = Card;


class DashboardPage extends Component {


	render() {
		return (
			<div>
				<h1>{this.context.t("About")}</h1>
				<Card>
				<h2>The team</h2>
					<Row type="flex" justify="space-around">
			      <Col span={6}>
							<Card
						    hoverable
						    style={{ width: 240 }}
						    cover={<img alt="example" src={jack} />}
						  >
						    <Meta
						      title="Jacques Herren"
						    />
						  </Card>
						</Col>
			      <Col span={6}>
							<Card
						    hoverable
						    style={{ width: 240 }}
						    cover={<img alt="example" src={xavier} />}
						  >
						    <Meta
						      title="Xavier Nendaz"
						    />
						  </Card>
						</Col>
			      <Col span={6}>
							<Card
						    hoverable
						    style={{ width: 240 }}
						    cover={<img alt="example" src={tiago} />}
						  >
						    <Meta
						      title="Tiago Almeida"
						    />
						  </Card>
						</Col>
			    </Row>
					<br></br>
					<Row type="flex" justify="space-around">
			      <Col span={6}>
							<Card
						    hoverable
						    style={{ width: 240 }}
						    cover={<img alt="example" src={sam} />}
						  >
						    <Meta
						      title="Samuel Coppey"
						    />
						  </Card>
						</Col>
			      <Col span={6}>
							<Card
						    hoverable
						    style={{ width: 240 }}
						    cover={<img alt="example" src={jon} />}
						  >
						    <Meta
						      title="Jonathan Joachim"
						    />
						  </Card>
						</Col>
			      <Col span={6}>
							<Card
						    hoverable
						    style={{ width: 240 }}
						    cover={<img alt="example" src={christian} />}
						  >
						    <Meta
						      title="Christian Pereira"
						    />
						  </Card>
						</Col>
			    </Row>
				</Card>
				<br></br>
				<br></br>
				<Card>
					<Row>
						<h2>How it's begon</h2>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas bibendum neque et purus rhoncus, in lobortis elit condimentum. Aliquam sed tellus non ex blandit malesuada at sit amet ipsum. Proin pellentesque sit amet lectus at faucibus. Duis erat tellus, bibendum sed velit id, consectetur placerat massa. Maecenas quam magna, ullamcorper ut sapien non, malesuada volutpat est. Fusce justo lorem, fringilla quis rhoncus imperdiet, pellentesque vel felis. Duis tincidunt leo vel elit cursus, vel euismod ante vehicula. Integer ac rutrum sem.

Donec eu rhoncus arcu. Vivamus imperdiet magna augue, vel ultricies nisi sagittis a. Aliquam erat volutpat. Nulla nec consequat mauris. Nulla blandit quis eros eu venenatis. Donec facilisis nisi vitae consectetur interdum. Cras sit amet justo vitae neque sollicitudin pulvinar. Duis pulvinar mauris justo. Donec ullamcorper consectetur est, ut hendrerit ex sagittis ac.

Nulla vehicula accumsan metus, eu euismod dolor vestibulum dignissim. Aliquam non risus bibendum, suscipit nunc quis, finibus risus. Donec non quam nec nulla laoreet pellentesque at vel odio. Ut efficitur tortor a nulla aliquet laoreet. Interdum et malesuada fames ac ante ipsum primis in faucibus. Phasellus posuere tristique erat, finibus maximus leo euismod quis. Cras nec lorem vestibulum, posuere purus a, facilisis sem. Proin rhoncus at eros id porttitor. Nulla facilisi.</p>
					</Row>
				</Card>
					<br></br>
					<br></br>
					<Row>
						<Col span={8}></Col>
						<Col span={8}>
							<iframe width="560" height="315" align="center" src="https://www.youtube.com/embed/hOlz63vWDBw" frameborder="0" allowfullscreen></iframe></Col>
						<Col span={8}></Col>
					</Row>
			</div>
		);
	}
}

DashboardPage.contextTypes = {
	t: PropTypes.func.isRequired
};

function mapStateToProps(state) {
	return {
		lang: state.i18nState.lang
	};
}

export default connect(mapStateToProps)(DashboardPage);
