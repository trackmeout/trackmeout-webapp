import React, {Component} from "react";
import { Button, Modal} from "antd";

import PropTypes from "prop-types";
import ReactHtmlParser from "react-html-parser";
import { RESP_MODAL_CANCEL, RESP_MODAL_OK } from "../../Types";

class TmoModal extends Component {

	/* Constructions
    **********************************/
	constructor(props) {
		super(props);
		this.state = {
			loading: false,
			open: this.props.openModal
		};
	}

	componentWillReceiveProps(nextProps) {
		this.setState(
			{ open:nextProps.openModal }
		);
	}

	/* Fonctions du composant
    **********************************/

	handleOk = () => {
		this.props.sendResponseModal(RESP_MODAL_OK);

	};

	handleCancel = () => {
		this.props.sendResponseModal(RESP_MODAL_CANCEL);
	};

	handleSubmit = () => {
		this.props.sendResponseModal(RESP_MODAL_OK);
	};


	/* Render
    **********************************/
	render() {
		const { open, loading } = this.state;
		const { title, closable, buttons, valueContent, typeContent, heightValue, style, bodyStyle, width  } = this.props.data;

		let content;
		let parent = this;


		let arrButtons = buttons.map(function (button) {
			return <Button key={ button.key } name={ button.key } type={ button.type } size={button.size}
				onClick={parent[button.function]}
				loading={button.hasLoading ? loading : false}> {button.text} </Button>;
		});

		if (typeContent === "html") {
			content = ReactHtmlParser(valueContent);
		}
		else if (typeContent === "react-component"){
			content = valueContent;
		}

		return (
			<div>
				<Modal
					maskClosable={closable}
					bodyStyle={bodyStyle}
					style={style}
					width={width}
					title={title}
					wrapClassName="vertical-center-modal"
					visible={ open }
					onOk={ this.handleOk }
					onCancel={ this.handleCancel}
					footer={ arrButtons }
				>
					<div className="tmo-flex-container" style = {{ height: heightValue }}>
						{content}
					</div>
				</Modal>
			</div>
		);
	}
}

TmoModal.propTypes = {
	value: PropTypes.string,
	handleOk: PropTypes.func
};

export default TmoModal;
