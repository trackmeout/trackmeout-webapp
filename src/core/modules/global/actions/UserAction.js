import { USER_CREATED , USER_LOGGED_IN, USER_LOGGED_OUT } from "../Types";
import api from "../utils/ApiRequests";
import {normalize} from "normalizr";
import setAuthorizationHeader from "../utils/setAuthorizationHeader";
import { userSchema } from "../Schemas";

	const userCreated = data => ({
		type: USER_CREATED,
		data
	});

	export const userLoggedIn = user => ({
	  type: USER_LOGGED_IN,
	  user
	});

	export const userLoggedOut = () => ({
	  type: USER_LOGGED_OUT
	});

	export const createUser = (user) =>
 	api.user.create(user);

	export const login = (credentials) => dispatch =>
	    api.user.login(credentials).then(res => {
			if(res.hasOwnProperty('user')){
		    localStorage.bookwormJWT = res.user.token;
		    setAuthorizationHeader(res.user.token);
		    dispatch(userLoggedIn(res.user));
			}
			return res;
	  });

	export const logout = () => dispatch => {
	  localStorage.removeItem("bookwormJWT");
	  setAuthorizationHeader();
	  dispatch(userLoggedOut());
	};
