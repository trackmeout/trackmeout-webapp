import React, {Component} from "react";
import { Progress } from 'antd';

class TmoProgressBar extends Component {

render(){
	const nbDone = this.props.nbDone;
	const nbTotal = this.props.nbTotal;

	//calcul the percentage of tasks done
	function calculPercent(curentNumber, totalNumber) {
			//Init ProgressBar with a minimum of 1% for display
			var percentCalculated;
			if (totalNumber === 0 ){
				percentCalculated = 0;
			}else if (curentNumber === 0) {
				percentCalculated = 1
			}else{
				percentCalculated = Math.ceil( curentNumber * 100 / totalNumber )
			}
		return percentCalculated
	}

	function setStatus () {
		var percent;
		if (nbDone === 0){
			percent = 0;
		}else{
			percent = calculPercent(nbDone, nbTotal) ;
		}

		if (percent === 0)
		{
			return "exception" ;
		}else if(percent === 100){
				return "success";
			}else{
					return "active";
			}
	}

	function setText (){
		var txt = "";
		if(nbTotal === 0){
			txt = "-";
		}else{
			txt = nbDone + "/" + nbTotal;
		}
		return txt;
	}

		return(
			<div>
				<Progress type="circle" status={setStatus()} percent = {calculPercent(nbDone, nbTotal)} format={percent =>  { return setText()}} width={50}> </Progress>
			</div>
		);
	}
}

export default TmoProgressBar;
