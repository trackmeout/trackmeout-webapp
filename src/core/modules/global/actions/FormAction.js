import {normalize} from "normalizr";
import {FORM_DELETED, FORM_CREATED, FORM_TEMPLATE_FETCHED, FORM_BYID_FETCHED} from "../Types";
import api from "../utils/ApiRequests";
import { formSchema } from "../Schemas";

// data.entities.patients
const formDeleted = data => ({
	type: FORM_DELETED,
	data
});

const formCreated = data => ({
	type: FORM_CREATED,
	data
});

const formTemplatesFetched = data => ({
	type: FORM_TEMPLATE_FETCHED,
	data
});

const formByIdFetched = data => ({
	type: FORM_BYID_FETCHED,
	data
});

export const deleteForm = ( form ) =>
	api.forms.delete( form );
//.then(forms => dispatch(formDeleted(normalize(forms, [formSchema]))));

export const editForm = ( form ) =>
	api.forms
		.edit( form );

export const createForm = ( form ) =>
	api.forms.create( form );

export const fetchFormTemplateById = (formId) =>
	api.forms
		.fetchFormTemplateById(formId).then( (formTemplates) =>  formTemplates );

export const fetchFormById = (formId) =>
	api.forms
		.fetchFormsById(formId).then( (form) => form );

export const convertFormAsTemplate = ( form ) =>
	api.forms
		.convertAsTemplate(form);

export const fetchFormTemplates = () => dispatch =>
	api.forms
		.fetchFormTemplates()
		.then(formTemplates => {
			return dispatch(formTemplatesFetched(normalize(formTemplates, [formSchema])));
		});

export const fetchFormTemplatesWithPromise = () =>
	api.forms.fetchFormTemplates().then( res => { return res } );