import React, { Component } from "react";
import { Modal } from 'antd';
import {PropTypes} from "prop-types";
import {connect} from "react-redux";


class TmoCreateForm extends Component {

	render() {
		const data = this.props.data;
		const modalContent =  <div>
			<h1>{this.context.t("Information")}</h1>
			<h3>{this.context.t("Name : ") + data.lastName}</h3>
			<h3>{this.context.t("Firstname : ") + data.firstName}</h3>
      <h3>{this.context.t("Geburtsdatum : ")} </h3>
			<br></br>
			<h3>{this.context.t("Login : ") + data.login}</h3>
			<h3>{this.context.t("Password : ") + data.pass}</h3>
		</div>;
		return (
			<Modal.success
				title={this.context.t("Patient created successfully")}
				content={modalContent}
			>
			</Modal.success>
		);
	}

}

TmoCreateForm.contextTypes = {
	t: PropTypes.func.isRequired
};

function mapStateToProps(state) {
	return {
		lang: state.i18nState.lang
	};
}

export default connect(mapStateToProps)(TmoCreateForm);
