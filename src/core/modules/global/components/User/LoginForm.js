import React, {Component} from "react";
import { message, Form, Icon, Input, Button} from "antd";
import { login } from "../../actions/UserAction";
import { connect } from "react-redux";
import createHistory from 'history/createBrowserHistory'
import PropTypes from "prop-types";
import TmoCreateTask from "../processus/TmoCreateTask";
const FormItem = Form.Item;
const history = createHistory()

message.config({
  top: 50,
  duration: 2,
});

class LoginForm extends Component {

  constructor(props) {
		super(props);
    this.state = {
      data: {
        email: "",
        password: ""
      },
      loading: false,
      errors: {}
    };

  }


  onChange = e =>
    this.setState({
      data: { ...this.state.data, [e.target.name]: e.target.value }
    });

  onSubmit = (e) => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        this.props.login(values).then(res => {
          if(res.hasOwnProperty('user')){
            message.success(this.context.t("Connexion succes"));
            history.push('/dashboard');
            history.go();
          }
          else{
            message.error(this.context.t("Error while connecting, check your login data"));
          }
        });
    }
  });
  }

  handleSubmit = (e) => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        console.log('Received values of form: ', values);
      }
    });
  }
  render() {
    const { getFieldDecorator } = this.props.form;
    return (
      <Form onSubmit={this.handleSubmit}>
        <FormItem>
          {getFieldDecorator('email', {
            rules: [{ required: true, message: this.context.t("Please enter your email address") }],
          })(
            <Input prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="exemple@gmail.com" />
          )}
        </FormItem>
        <FormItem>
          {getFieldDecorator('password', {
            rules: [{ required: true, message: this.context.t("Please enter your email address") }],
          })(
            <Input prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />} type="password" placeholder="Mot de passe" />
          )}
        </FormItem>
        <FormItem>
          <Button type="primary" onClick={this.onSubmit} className="login-form-button">
            Log in
          </Button>
        </FormItem>
      </Form>
    );
  }
}

LoginForm.contextTypes = {
	t: PropTypes.func.isRequired
};

function mapStateToProps(state) {
	return {
		lang: state.i18nState.lang
	};
}


const WrappedLoginForm = Form.create()(LoginForm);

export default connect(null, {login})(WrappedLoginForm);
