import {normalize} from "normalizr";
import {PATIENTS_FETCHED, PATIENT_CREATED} from "../Types";
import api from "../utils/ApiRequests";
import { patientSchema } from "../Schemas";

// data.entities.patients
const patientsFetched = data => ({
	type: PATIENTS_FETCHED,
	data
});

const patientCreated = data => ({
	type: PATIENT_CREATED,
	data
});

export const fetchPatients = () => dispatch =>
	api.patients
		.fetchAll()
		.then(patients => dispatch(patientsFetched(normalize(patients, [patientSchema]))));

export const createPatient = (patient) => dispatch =>
	api.patients
		.create(patient);
// .then(patient => dispatch(patientCreated(normalize(patient, [patientSchema]))));
