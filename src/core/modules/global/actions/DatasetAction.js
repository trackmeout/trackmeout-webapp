import { normalize } from "normalizr";
import api from "../utils/ApiRequests";

export const fetchDatasets = () =>
		api.datasets.fetchAll()
			.then(datasets => { return datasets });
