import React, {Component} from "react";
import {
  Upload,
  Button,
  Divider,
  List,
  Icon,
  message
} from "antd";
import TmoModal from "../common/TmoModal";
import PropTypes from "prop-types";
import {saveDoc, deleteDoc, getDocById} from "../../actions/DocumentsAction";
import {editTask} from "../../actions/TaskAction";
import {RESP_MODAL_OK, RESP_MODAL_CANCEL} from "../../Types";
import {config} from "../../../../config";
import TmoImportProcess from "./TmoImportProcess";

class TmoUploadTask extends Component {

  /* Constructions
  **********************************/
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: false,
      fileList: [],
      storedFileList: this.props.data.documents,
      uploading: false,
      pdfVisible: false
    };
    //Data for task
    this.task = this.props.data;
    this.pdf = null;
  }

  componentWillReceiveProps(nextProps) {
    this.task = nextProps.data;
    this.setState({storedFileList: this.task.documents});
  }

  /* Fonctions
	**********************************/
  showModal = () => {
    this.setState({modalVisible: true});
  }
//delete a file document
  deleteFile = (e) => {
    deleteDoc(e);
    this.setState({
      storedFileList: this.state.storedFileList.filter(item => item !== e)
    }, function() {
      this.task.documents = this.state.storedFileList;
      editTask(Object.assign({}, this.task));
    });
  }
	//display a file
  viewFile = (e) => {
    window.open(config.backendAddress + "documents/" + e);
  }
//reset the modal after data are passed
  resetInfosModalTask = () => {
    this.setState({modalVisible: false, fileList: [], uploading: false});
    this.task = "";
    this.props.refreshPageFunction();
  }
	//get response from modal task
  getResponseModalTask = (resp) => {
    //Faire test
    switch (resp) {
      case RESP_MODAL_OK:
        this.handleUpload();
        break;
      case RESP_MODAL_CANCEL:
        this.resetInfosModalTask();
        break;
      default:
        break;
    }
  }
	//upload function
  handleUpload = async () => {
    const {fileList} = this.state;
    if (fileList.length > 0) {
      const formData = new FormData();
      fileList.forEach((file) => {
        formData.append("files[]", file);
      });

      this.modalTask.setState({loading: true});
      let res = await saveDoc(formData);
      if (res.hasOwnProperty("UPLOAD_OK")) {
        this.task.documents = this.state.storedFileList.concat(res["UPLOAD_OK"]);
        this.setState({fileList: []});
        this.modalTask.setState({loading: false});
        message.success("Upload successfully.");
        editTask(Object.assign({}, this.task));
        this.resetInfosModalTask();
        this.props.refreshPageFunction();
      } else {
        message.error("Upload failed.");
        this.modalTask.setState({loading: false});
      }
    } else {
      this.resetInfosModalTask();
      this.props.refreshPageFunction();
    }

  }

  /* Render
  ********************************* render the uploading component
	* */
  render() {
    const {modalVisible, uploading, storedFileList, pdfVisible} = this.state;

    const propsUpload = {
      onRemove: (file) => {
        this.setState(({fileList}) => {
          const index = fileList.indexOf(file);
          const newFileList = fileList.slice();
          newFileList.splice(index, 1);
          return {fileList: newFileList};
        });
      },
      beforeUpload: (file) => {
        this.setState(({fileList}) => ({
          fileList: [
            ...fileList,
            file
          ]
        }));
        return false;
      },
      fileList: this.state.fileList
    };

    const contentModalNewTask = (<div>
      <h4 style={{
          textAlign: "left"
        }}>{this.context.t("Current documents")}</h4>
      <List style={{
          marginTop: 20
        }} itemLayout="horizontal" dataSource={storedFileList} renderItem={item => (<List.Item style={{
            paddingBottom: 0
          }} actions={[
            <Icon type="eye" onClick={() => this.viewFile(item)}/>,
            <Icon type="delete" onClick={() => this.deleteFile(item)}/>
          ]}>
          <List.Item.Meta title={(<p style={{
                textAlign: "left"
              }}>{item.split("@")[1]}
            </p>)}/>
        </List.Item>)}/>
      <div style={{
          marginTop: 30,
          textAlign: "center"
        }}>
        <Divider>Upload new files</Divider>
        <Upload {...propsUpload} accept="application/pdf" multiple="multiple">
          <Button>
            <Icon type="upload"/>
            Select File
          </Button>
        </Upload>
      </div>
    </div>);

    const dataModal = {
      title: "Upload fichier",
      typeContent: "react-component",
      valueContent: contentModalNewTask,
      closable: false,
      buttons: [
        {
          key: "cancel",
          type: "",
          text: this.context.t("Cancel"),
          size: "large",
          function: "handleCancel",
          hasLoading: false
        }, {
          key: "ok",
          type: "primary",
          text: "Ok",
          size: "large",
          function: "handleSubmit",
          hasLoading: true
        }
      ]
    };

    return (<div>
      {
        modalVisible && (<TmoModal ref={(child) => {
            this.modalTask = child;
          }} data={dataModal} openModal={modalVisible} sendResponseModal={this.getResponseModalTask}/>)
      }
    </div>);
  }
}

TmoUploadTask.contextTypes = {
  t: PropTypes.func.isRequired
};

function mapStateToProps(state) {
  return {lang: state.i18nState.lang};
}

export default TmoUploadTask;
