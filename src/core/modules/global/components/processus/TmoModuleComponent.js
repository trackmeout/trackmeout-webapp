import React, {Component} from "react";
import TmoFormComponent from "./TmoFormComponent";
import { deleteModule } from "../../actions/ModuleAction";
import {DEFAULT, READ_MODE, TEMPLATE_MODE} from "../../Types";
import {connect} from "react-redux";
import _ from "lodash";
import PropTypes from "prop-types";
import {Icon} from "antd";
import {allPatientProcessesSelector} from "../../reducers/ProcessReducer";
import {fetchPatientProcesses} from "../../actions/ProcessesAction";

class TmoModuleComponent extends Component {

	/* Constructions
    **********************************/
	constructor(props) {
		super(props);

		this.state = {
			modeEditionVisible : false
		};

		this.currentMode = READ_MODE;
		this.isSelectable = this.props.onSelect !== undefined;

		if(this.props.currentMode !== undefined){
			this.currentMode = this.props.currentMode;
		}
	}

	// componentWillReceiveProps( nextProps ){
	// 	this.props = nextProps;
	// 	console.log("New props");
	// 	//this.settingColor();
	// }

	removeModule = (module) => {
		this.props.deleteModule(module);
		// message.success('Le module a bien été supprimée');
	}

    addConvertScrollEvent = ( element ) => {
        if(element){
            var mouseSpeed = 0.75;

            element.addEventListener("mousewheel", (e) =>{
                e.preventDefault();
                element.scrollLeft += (e.deltaY * mouseSpeed);
            });
        }
    };

	/* Render
    **********************************/
	render() {
		const _this = this;
		const { data } = this.props;
        this.moduleTemplatesElements = [];
		//Genere les tâches avant de les afficher

		const currentMode = !!_this.props.currentMode ? _this.props.currentMode : READ_MODE;
		const isPartOfOneTemplate  = !!_this.props.isPartOfOneTemplate;

		const forms = data.forms.map(function (form) {
			return ((form.isVisible || currentMode === TEMPLATE_MODE )  &&
					<TmoFormComponent hideBorders={true} onClickOnCard={_this.isSelectable} isPartOfOneTemplate={isPartOfOneTemplate} currentMode={currentMode} refreshPageFunction={_this.props.refreshPageFunction} key={form._id} data={form} />);
		});

		//CSS Styling
		const titleStyle = {
			paddingLeft: "20px",
			paddingTop: "10px",
			color: "#aeaeae"
		};
		//Popup to confirm the suppression of the module selected or cancel
		const confirmModal = {
			title: this.context.t("Do you really want to delete this module?"),
			btnShowModal: this.context.t("Delete"),
			btnLeft: this.context.t("Cancel"),
			btnRight: this.context.t("Delete the module"),
			textDesc: this.context.t("Deleting this module will delete all forms or child tasks.")
		};

		const h3Content = this.isSelectable ?  <span onClick={ () => this.props.onSelect(data) }><Icon type="arrow-left" /> {data.name}</span> : data.name;

		const classNameContainerStyle = this.isSelectable ? "process-module-template-item tmo-flex-container selectable" : "module-item";
		const classNameContainerFormsStyle = this.isSelectable ? "tmo-flex-container-row " : "tmo-float-content";
		const result = <div className={classNameContainerStyle}  >
							 <h3 style={ titleStyle }>{ h3Content }</h3>
							<div className={classNameContainerFormsStyle} ref={(child) => { _this.moduleTemplatesElements.push(child); }} >
								{ forms }
							</div>
							<div style={{clear: "both"}} ></div>
						</div>;


		//Active les scrolls
		if(this.isSelectable){
            setTimeout(() => {
                _.each(_this.moduleTemplatesElements, function (moduleTemplate) {
                    if(!!moduleTemplate){
                        _this.addConvertScrollEvent(moduleTemplate);
                    }
                });

            }, 100);
		}

		return (result);
	}
}

TmoModuleComponent.contextTypes = {
	t: PropTypes.func.isRequired
};

function mapStateToProps(state) {
	return {
		patientProcesses: allPatientProcessesSelector(state),
		lang: state.i18nState.lang
	};
}

const mapDispatchToProps = {
	fetchPatientProcesses,
};

export default connect(mapStateToProps, mapDispatchToProps)(TmoModuleComponent);
