import {createSelector} from "reselect";
import { MODULE_TEMPLATE_FETCHED } from "../Types";

//MODULE REDUCER
export default function modules(state = {}, action = {}) {
	switch (action.type) {
	case MODULE_TEMPLATE_FETCHED:
		return {...state, ...action.data.entities.modules};
	default:
		return state;
	}
}

// SELECTORS
export const modulesTemplateSelector = state => state.modules;
export const allModulesTemplateSelector = createSelector(modulesTemplateSelector, ModulesTemplateHash =>
	Object.values(ModulesTemplateHash)
);

export const modulesParentsTemplateSelector = state => state.processes;
export const allModuleTemplateFromProcess = createSelector(modulesParentsTemplateSelector, ModulesTemplateParentHash =>
	Object.values(ModulesTemplateParentHash)
);
