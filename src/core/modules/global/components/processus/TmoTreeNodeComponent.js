import React, {Component} from "react";
import {connect} from "react-redux";
import {Checkbox, Icon, Menu, Dropdown, Tooltip} from "antd";
import _ from "lodash";
import TmoTreeElementModalComposant, {
  IMPORT_MODULE_FROM_TEMPLATE,
  IMPORT_PROCESS_MODAL,
  CONVERT_PROCESS_MODAL,
  CONVERT_FORM_MODAL,
  CONVERT_MODULE_MODAL,
  EDIT_PROCESS_MODAL,
  ADD_MODULE_MODAL,
  IMPORT_FORM_MODAL_FROM_PROCESS,
  IMPORT_FORM_MODAL_FROM_MODULE,
  EDIT_MODULE_MODAL,
  ADD_FORM_MODAL,
  EDIT_FORM_MODAL,
  ADD_TASK_MODAL
} from "./TmoTreeElementModalComposant";
import TmoCreateTask from "../processus/TmoCreateTask";
import TmoImportForm from "../processus/TmoImportForm";
import TmoImportProcess from "../processus/TmoImportProcess";
import TmoImportModule from "../processus/TmoImportModule";
import TmoConfirmModal, {DELETE_FORM_MODAL, DELETE_MODULE_MODAL, ARCHIVE_PROCESS_MODAL} from "../common/TmoConfirmModal";
import PropTypes from "prop-types";
import {allPatientProcessesSelector} from "../../reducers/ProcessReducer";

export const INSTANCE_PROCESS = "INSTANCE_PROCESS";
export const INSTANCE_MODULE = "INSTANCE_MODULE";
export const INSTANCE_FORM = "INSTANCE_FORM";

class TmoTreeNodeComponent extends Component {
  constructor(props) {
    super(props);
    this.instanceData = this.props.nodeData;
    this.instanceType = this.props.instanceType;
    this.parentNode = this.props.parentNode;
    this.isIndeterminate = false;
    this.childrenElements = [];
    switch (this.instanceType) {
      case INSTANCE_PROCESS:
        this.state = {
          isCheckBoxChecked: this.instanceData.isSelected,
          isCollapsed: false
        };
        this.childrenData = this.instanceData.modules;
        break;
      case INSTANCE_MODULE:
        this.state = {
          isCheckBoxChecked: this.instanceData.isSelected,
          isCollapsed: false
        };

        console.log(this.instanceData.isSelected)

        this.childrenData = this.instanceData.forms;
        this.parentNode.childrenElements.push(this);
        break;
      case INSTANCE_FORM:
        this.state = {
          isCheckBoxChecked: this.instanceData.isSelected,
          isCollapsed: false
        };
        this.childrenData = [];
        this.parentNode.childrenElements.push(this);
        break;
      default:
    }
  }

  componentWillMount() {
    this.checkIfNeedToCheckAsChild();
    this.updateList(false);
  }
	//toogle the collapse function on click
  toggleCollapse = () => {
    this.setState({
      isCollapsed: !this.state.isCollapsed
    });
  };

	//check if child should be checked
  checkIfNeedToCheckAsChild = () => {
    let _this = this;
    if (_this.instanceType === INSTANCE_MODULE && _this.childrenElements.length === 0) {
      _this.instanceData.isSelected = true;
      _this.isIndeterminate = false;
      _this.instanceData.isVisible = true;
      return {
        isCheckBoxChecked: _this.instanceData.isSelected,
        isCollapsed: !_this.instanceData.isSelected
      };
    }

    _.each(_this.childrenElements, function(child) {
      child.setState((prevState) => {
        child.instanceData.isSelected = _this.instanceData.isSelected;
        child.isIndeterminate = false;
        child.instanceData.isVisible = _this.instanceData.isSelected;
        return {
          isCheckBoxChecked: _this.instanceData.isSelected,
          isCollapsed: !_this.instanceData.isSelected
        };
      });
      child.checkIfNeedToCheckAsChild();
    });
  };
  checkIfNeedToCheckAsParent = (element) => {
    const numberOfchildrenDataTotal = element.childrenElements.length;
    let numberOfchildrenDataSelected = 0;
    let numberOfchildrenDataIndeterminated = 0;

    if (numberOfchildrenDataTotal > 0) {
      _.each(element.childrenElements, function(child) {
        if (child.instanceData.isSelected === true) {
          numberOfchildrenDataSelected++;
        }
        if (child.isIndeterminate === true) {
          numberOfchildrenDataIndeterminated++;
        }
      });
      if (numberOfchildrenDataSelected === numberOfchildrenDataTotal) {
        element.instanceData.isSelected = true;
        element.isIndeterminate = false;
        element.setState((prevState) => {
          return {isCheckBoxChecked: true};
        });
      } else {
        element.isIndeterminate = numberOfchildrenDataSelected !== 0;
        element.instanceData.isSelected = false;

        element.setState((prevState) => {
          return {isCheckBoxChecked: false};
        });
      }
      if (numberOfchildrenDataIndeterminated > 0) {
        element.isIndeterminate = true;
      }
      element.instanceData.isVisible = element.isIndeterminate || element.instanceData.isSelected;
      element.setState((prevState) => {
        return {
          isCollapsed: !element.instanceData.isVisible
        };
      });
    }
  };
	//update a list of component
  updateList = (needToRefreshData = true) => {
    let currentElement = this;
    while (currentElement.parentNode) {
      currentElement = currentElement.parentNode;
      if (currentElement instanceof TmoTreeNodeComponent) {
        this.checkIfNeedToCheckAsParent(currentElement);
      }
    }
    if (needToRefreshData) {
      currentElement.updateProcesses();
    }
  };
	//toogle the checked component function
  toggleChecked = () => {
    const _this = this;
    this.instanceData.isSelected = !this.state.isCheckBoxChecked;
    this.instanceData.isVisible = this.instanceData.isSelected;
    this.isIndeterminate = false;
    this.setState((prevState) => {
      return {
        isCheckBoxChecked: this.instanceData.isSelected,
        isCollapsed: !this.instanceData.isSelected
      };
    });
    _this.checkIfNeedToCheckAsChild();

    setTimeout(function() {
      _this.updateList();
    }, 80);

  };

  visibilityChange = (visible) => {
    if (visible === false) {
      this.clickedNode.classList.remove("selected");
    }
  };
	//function enable right click module
  treeViewRightClick = () => {
    this.clickedNode.classList.add("selected");
  };
	//function openeing a tree modal
  openTreeModal = (element) => {
    this.clickedNode.classList.remove("selected");
    switch (element.item.props.modalType) {
      case CONVERT_PROCESS_MODAL:
        this.convertProcessModal.setState({isVisible: true});
        break;
      case CONVERT_MODULE_MODAL:
        this.convertModuleModal.setState({isVisible: true});
        break;
      case CONVERT_FORM_MODAL:
        this.convertFormModal.setState({isVisible: true});
        break;
      case EDIT_PROCESS_MODAL:
        this.editProcessModal.setState({isVisible: true});
        break;
      case ADD_MODULE_MODAL:
        this.addModuleModal.setState({isVisible: true});
        break;
      case IMPORT_MODULE_FROM_TEMPLATE:
        this.importModulesModal.setState({modalVisible: true, processSelected: null, searchValue: ""});
        break;
      case EDIT_MODULE_MODAL:
        this.editModuleModal.setState({isVisible: true});
        break;
      case ADD_FORM_MODAL:
        this.addFormModal.setState({isVisible: true});
        break;
      case DELETE_FORM_MODAL:
        this.deleteFormModal.setState({isVisible: true});
        break;
      case DELETE_MODULE_MODAL:
        this.deleteModuleModal.setState({isVisible: true});
        break;
      case ARCHIVE_PROCESS_MODAL:
        this.archiveProcessModal.setState({isVisible: true});
        break;
      case EDIT_FORM_MODAL:
        this.editFormModal.setState({isVisible: true});
        break;
      case ADD_TASK_MODAL:
        this.addTaskModal.showModalNewTask();
        break;
      case IMPORT_FORM_MODAL_FROM_PROCESS:
        this.importFormsModal.setState({modalVisible: true, moduleSelected: null, searchValue: ""});
        break;
      case IMPORT_FORM_MODAL_FROM_MODULE:
        this.parentNode.importFormsModal.setState({modalVisible: true, moduleSelected: element.item.props.moduleId, searchValue: ""});
        break;
      case IMPORT_PROCESS_MODAL:
        this.importProcessModal.showModal();
        break;
      default:
    }
  };

  componentWillReceiveProps(nextProps) {
    this.instanceData = nextProps.nodeData;
    this.parentNode = nextProps.parentNode;
    switch (this.instanceType) {
      case INSTANCE_PROCESS:
        this.childrenData = this.instanceData.modules;
        break;
      case INSTANCE_MODULE:
        this.childrenData = this.instanceData.forms;
        break;
      default:
    }

    this.setState({isCheckBoxChecked: this.instanceData.isSelected})

  }
	//render every menu item onclick with each modal component
  render() {
    const _this = this;
    let itemToRender,
      menu;
    const collapseIcon = this.state.isCollapsed === true
      ? <Icon type="caret-right"/>
      : <Icon type="caret-down"/>;
    const collapseDisplayStyle = this.state.isCollapsed === true
      ? "none"
      : "block";
    switch (this.instanceType) {
      case INSTANCE_PROCESS:
        menu = (<Menu onClick={this.openTreeModal}>
          <Menu.Item modalType={EDIT_PROCESS_MODAL} key="1">{this.context.t("Rename process")}</Menu.Item>
          <Menu.Item modalType={ARCHIVE_PROCESS_MODAL} key="2">{this.context.t("Archive process")}</Menu.Item>
          <Menu.Item modalType={IMPORT_PROCESS_MODAL} key="3">{this.context.t("Import process")}</Menu.Item>
          <Menu.Divider/>
          <Menu.Item modalType={ADD_MODULE_MODAL} key="4">{this.context.t("Add module")}</Menu.Item>
          <Menu.Item modalType={IMPORT_MODULE_FROM_TEMPLATE} key="5">{this.context.t("Import modules")}</Menu.Item>
          <Menu.Divider/>
          <Menu.Item modalType={IMPORT_FORM_MODAL_FROM_PROCESS} key="6">{this.context.t("Import forms")}</Menu.Item>
          <Menu.Divider/>
          <Menu.Item modalType={CONVERT_PROCESS_MODAL} key="7">{this.context.t("Define as a model")}</Menu.Item>
        </Menu>);
        itemToRender = this.childrenData.map(function(child) {
          return <TmoTreeNodeComponent refreshPageFunction={_this.props.refreshPageFunction} key={child.id} nodeData={child} instanceType={INSTANCE_MODULE} parentNode={_this}/>;
        });
        return (<li className="tree-menu-process-level tree-menu-process-level-process" key={_this.instanceData.id}>
          <div className="collapse-btn" onClick={this.toggleCollapse}>{collapseIcon}</div>
          <span >
            <Checkbox onClick={this.toggleChecked} indeterminate={this.isIndeterminate} checked={this.state.isCheckBoxChecked}/>
            <Dropdown onVisibleChange={this.visibilityChange} overlay={menu} trigger={["contextMenu"]}>
              <Tooltip placement="right" title={this.instanceData.name}>
                <span ref={(child) => {
                    _this.clickedNode = child;
                  }} onContextMenu={(() => this.treeViewRightClick(_this))} className="tree-menu-line" style={{
                    userSelect: "none",
                    width: "175px"
                  }}>{this.instanceData.name}</span>
              </Tooltip>
            </Dropdown>
          </span>
          <ul style={{
              display: collapseDisplayStyle
            }}>{itemToRender}</ul>
          <TmoImportProcess refreshPageFunction={_this.props.refreshPageFunction} ref={(child) => {
              this.importProcessModal = child;
            }}/>
          <TmoTreeElementModalComposant refreshPageFunction={_this.props.refreshPageFunction} ref={(child) => {
              _this.convertProcessModal = child;
            }} modalType={CONVERT_PROCESS_MODAL} modalData={_this.instanceData}/>
          <TmoTreeElementModalComposant refreshPageFunction={_this.props.refreshPageFunction} ref={(child) => {
              _this.editProcessModal = child;
            }} modalType={EDIT_PROCESS_MODAL} modalData={_this.instanceData}/>
          <TmoImportForm formTemplates={this.props.formTemplates} refreshPageFunction={_this.props.refreshPageFunction} ref={(child) => {
              _this.importFormsModal = child;
            }} modalType={IMPORT_FORM_MODAL_FROM_PROCESS} modalData={_this.instanceData}/>
          <TmoTreeElementModalComposant refreshPageFunction={_this.props.refreshPageFunction} ref={(child) => {
              _this.addModuleModal = child;
            }} modalType={ADD_MODULE_MODAL} modalData={_this.instanceData}/>
          <TmoImportModule moduleTemplates={this.props.moduleTemplates} refreshPageFunction={_this.props.refreshPageFunction} ref={(child) => {
              _this.importModulesModal = child;
            }} process={_this.instanceData} modalType={IMPORT_MODULE_FROM_TEMPLATE} modalData={_this.parentNode.patientProcesses}/>
          <TmoConfirmModal refreshPageFunction={_this.props.refreshPageFunction} ref={(child) => {
              _this.archiveProcessModal = child;
            }} modalData={_this.instanceData.id} modalType={ARCHIVE_PROCESS_MODAL}/>
        </li>);
      case INSTANCE_MODULE:
        menu = (<Menu onClick={this.openTreeModal}>
          <Menu.Item modalType={EDIT_MODULE_MODAL} key="1">{this.context.t("Rename module")}</Menu.Item>
          <Menu.Item modalType={DELETE_MODULE_MODAL} key="2">{this.context.t("Delete module")}</Menu.Item>
          <Menu.Divider/>
          <Menu.Item modalType={ADD_FORM_MODAL} key="3">{this.context.t("Add form")}</Menu.Item>
          <Menu.Item moduleId={_this.instanceData.id} modalType={IMPORT_FORM_MODAL_FROM_MODULE} key="4">{this.context.t("Import a form")}</Menu.Item>
          <Menu.Divider/>
          <Menu.Item modalType={CONVERT_MODULE_MODAL} key="5">{this.context.t("Define as a model")}</Menu.Item>
        </Menu>);

        itemToRender = this.childrenData.map(function(child) {
          return <TmoTreeNodeComponent refreshPageFunction={_this.props.refreshPageFunction} key={child.id} nodeData={child} instanceType={INSTANCE_FORM} parentNode={_this}/>;
        });
        return (<li className="tree-menu-process-level tree-menu-process-level-module" key={_this.instanceData.id}>
          <div className="collapse-btn" onClick={this.toggleCollapse}>{collapseIcon}</div>
          <span >
            <Checkbox onClick={this.toggleChecked} indeterminate={this.isIndeterminate} checked={this.state.isCheckBoxChecked}/>
            <Dropdown onVisibleChange={this.visibilityChange} overlay={menu} trigger={["contextMenu"]}>
              <Tooltip placement="right" title={this.instanceData.name}>
                <span ref={(child) => {
                    _this.clickedNode = child;
                  }} onContextMenu={(() => this.treeViewRightClick(_this))} className="tree-menu-line" style={{
                    userSelect: "none",
                    width: "155px"
                  }}>{this.instanceData.name}</span>
              </Tooltip>
            </Dropdown>
          </span>
          <ul style={{
              display: collapseDisplayStyle
            }}>{itemToRender}</ul>
          <TmoTreeElementModalComposant refreshPageFunction={_this.props.refreshPageFunction} ref={(child) => {
              _this.convertModuleModal = child;
            }} modalType={CONVERT_MODULE_MODAL} modalData={_this.instanceData}/>
          <TmoTreeElementModalComposant refreshPageFunction={_this.props.refreshPageFunction} ref={(child) => {
              _this.editModuleModal = child;
            }} modalType={EDIT_MODULE_MODAL} modalData={_this.instanceData}/>
          <TmoTreeElementModalComposant refreshPageFunction={_this.props.refreshPageFunction} ref={(child) => {
              _this.addFormModal = child;
            }} modalType={ADD_FORM_MODAL} modalData={_this.instanceData}/>
          <TmoConfirmModal refreshPageFunction={_this.props.refreshPageFunction} ref={(child) => {
              _this.deleteModuleModal = child;
            }} modalData={_this.instanceData} modalType={DELETE_MODULE_MODAL}/>
        </li>);
      case INSTANCE_FORM:
        menu = (<Menu onClick={this.openTreeModal}>
          <Menu.Item modalType={EDIT_FORM_MODAL} key="1">{this.context.t("Rename form")}</Menu.Item>
          <Menu.Item modalType={DELETE_FORM_MODAL} key="2">{this.context.t("Delete form")}</Menu.Item>
          <Menu.Divider/>
          <Menu.Item modalType={ADD_TASK_MODAL} key="3">{this.context.t("Add task")}</Menu.Item>
          <Menu.Divider/>
          <Menu.Item modalType={CONVERT_FORM_MODAL} key="4">{this.context.t("Define as a model")}</Menu.Item>
        </Menu>);

        return (<li className="tree-menu-process-level tree-menu-process-level-form" key={_this.instanceData.id}>
          <span >
            <Checkbox onClick={this.toggleChecked} indeterminate={false} checked={this.state.isCheckBoxChecked}/>
            <Dropdown onVisibleChange={this.visibilityChange} overlay={menu} trigger={["contextMenu"]}>
              <Tooltip placement="right" title={this.instanceData.name}>
                <span ref={(child) => {
                    _this.clickedNode = child;
                  }} onContextMenu={(() => this.treeViewRightClick(_this))} className="tree-menu-line" style={{
                    userSelect: "none",
                    width: "125px"
                  }}>{this.instanceData.name}</span>
              </Tooltip>
            </Dropdown>
          </span>
          <TmoTreeElementModalComposant refreshPageFunction={_this.props.refreshPageFunction} ref={(child) => {
              _this.convertFormModal = child;
            }} modalType={CONVERT_FORM_MODAL} modalData={_this.instanceData}/>
          <TmoTreeElementModalComposant refreshPageFunction={_this.props.refreshPageFunction} ref={(child) => {
              _this.editFormModal = child;
            }} modalType={EDIT_FORM_MODAL} modalData={_this.instanceData}/>
          <TmoCreateTask refreshPageFunction={_this.props.refreshPageFunction} ref={(child) => {
              _this.addTaskModal = child;
            }} parent={_this.instanceData.id}/>
          <TmoConfirmModal refreshPageFunction={_this.props.refreshPageFunction} ref={(child) => {
              _this.deleteFormModal = child;
            }} modalData={_this.instanceData} modalType={DELETE_FORM_MODAL}/>
        </li>);
      default:
        return <div>Not implemented</div>;
    }
  };
}

TmoTreeNodeComponent.contextTypes = {
  t: PropTypes.func.isRequired
}

function mapStateToProps(state) {
  return {lang: state.i18nState.lang, patientProcesses: allPatientProcessesSelector(state)};
}
export default connect(mapStateToProps)(TmoTreeNodeComponent);
