import React, {Component} from "react";
import {connect} from "react-redux";
import {allFormsTemplateSelector} from "../../../reducers/FormReducer";
import {fetchFormTemplateById, fetchFormTemplates} from "../../../actions/FormAction";
import TmoFormComponent from "../../processus/TmoFormComponent";
import {Button, Layout} from "antd";
import TmoTreeElementModalComposant, {ADD_FORM_TEMPLATE_MODAL} from "../../processus/TmoTreeElementModalComposant";
import {TEMPLATE_MODE} from "../../../Types";
import PropTypes from "prop-types";

class TmoFormsTabContainer extends Component {

  constructor(props) {
    super(props);
    this.state = {
      activeFormKey: null,
      activeForm: null
    };
  }

  componentWillMount = () => {
    const _this = this;
    _this.props.fetchFormTemplates().then((item) => {
      fetchFormTemplateById().then((form) => {});
    });
  };

  componentWillReceiveProps(nextProps) {
    this.patientProcesses = nextProps.patientProcesses;
  }

  refreshPageFunction = () => {
    const _this = this;
    _this.props.fetchFormTemplates().then((item) => {
      const key = !!item.data.entities.forms
        ? Object.keys(item.data.entities.forms)[0]
        : null;
    });
  };
//refreshing the page
  refreshPage = () => {
    const _this = this;
    setTimeout(function() {
      _this.refreshProcessList();
    }, 80);
  };

  render() {
		//rendering the forms templating component
    const _this = this;
    const {Sider, Content} = Layout;
    const forms = this.props.forms.map(function(form) {
      return <TmoFormComponent isPartOfOneTemplate={true} refreshPageFunction={_this.refreshPageFunction} key={form._id} data={form}/>;
    });

    return (<div id="template-editor-form-container">
      <div className="form-template-available-forms-list">
        <Button onClick={() => {
            this.addNewFormModal.setState({isVisible: true});
          }} style={{
            border: "2px",
            width: "auto",
            textAlign: "right",
            marginTop: "10px",
            float: "right",
            marginLeft: "100%"
          }} icon="plus" size={"default"}>{this.context.t("Add a form")}</Button>
        <TmoTreeElementModalComposant refreshPageFunction={_this.refreshPageFunction} ref={(child) => {
            _this.addNewFormModal = child;
          }} modalType={ADD_FORM_TEMPLATE_MODAL}/>
      </div>
      <div className="tmo-float-content">
        {forms}
        <div className="clear"></div>
      </div>

    </div>);
  }
}

TmoFormsTabContainer.contextTypes = {
  t: PropTypes.func.isRequired
};

function mapStateToProps(state) {
  return {forms: allFormsTemplateSelector(state), lang: state.i18nState.lang};
}

export default connect(mapStateToProps, {fetchFormTemplates})(TmoFormsTabContainer);

// export default connect (mapStateToProps, mapDispatchToProps)(TmoFormsTabContainer);
