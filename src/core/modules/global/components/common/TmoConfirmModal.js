import React, { Component } from 'react';
import {Button, Modal, Icon, message } from "antd";
import { deleteForm } from "../../actions/FormAction"
import { deleteModule } from "../../actions/ModuleAction";
import { archiveProcess } from "../../actions/ProcessesAction";
import { deleteTask } from "../../actions/TaskAction";
import PropTypes from "prop-types";

export const DELETE_TASK_MODAL = "DELETE_TASK_MODAL";
export const DELETE_FORM_MODAL = "DELETE_FORM_MODAL";
export const DELETE_MODULE_MODAL = "DELETE_MODULE_MODAL";
export const ARCHIVE_PROCESS_MODAL = "ARCHIVE_PROCESS_MODAL";
export const ARCHIVE_PROCESS_MODAL_TEMPLATE = "ARCHIVE_PROCESS_MODAL_TEMPLATE";
export const DATASET = "DATASET";

class TmoConfirmModal extends Component {
constructor(props) {

  super(props);
  this.modalType = this.props.modalType;
  this.state = {
    isVisible:false,
  };
}

showModal = () => {
  this.setState({
    isVisible: true
  })
}

handleCancel = (e) => {
  switch (this.modalType) {
    case DATASET:
      this.props.getDatasets();
      break;
    default:
  }
  this.setState({
    isVisible: false,
  });
}

//OnClick, do the selected fonction and send message to confirm
actionClick = () => {
  switch (this.modalType) {
    case DELETE_FORM_MODAL:
      deleteForm(this.props.modalData);
      message.success(this.context.t("The form has been deleted"));
      this.props.refreshPageFunction();
    break;
    case DELETE_MODULE_MODAL:
      deleteModule(this.props.modalData);
      message.success(this.context.t("The module has been deleted"));
      this.props.refreshPageFunction();
    break;
    case ARCHIVE_PROCESS_MODAL:
      archiveProcess(this.props.modalData);
      message.success(this.context.t("The process has been archived"));
      this.props.refreshPageFunction();
    break;
    case ARCHIVE_PROCESS_MODAL_TEMPLATE:
      archiveProcess(this.props.modalData());
      message.success(this.context.t("The process has been deleted"));
      this.props.refreshPageFunction();
    break;
    case DELETE_TASK_MODAL:
      deleteTask(this.props.modalData);
      message.success(this.context.t("The task- has been deleted"));
      this.props.refreshPageFunction();
    break;
    case DATASET:
      this.props.datasetVisible();
    break;
  default:
  }
  this.setState({
    isVisible: false,
  })
}

//Generate popup to confirm delete or cancel
generateModalData = () => {
  switch (this.modalType) {
  case DELETE_FORM_MODAL:
    return {
    			title: this.context.t("Do you really want to delete this form?"),
    			btnLeft: this.context.t("Cancel"),
    			btnRight: this.context.t("Delete the form"),
    			textDesc: this.context.t("Deleting this form will delete its child tasks"),
          type : "warning"
    		};
  case DELETE_MODULE_MODAL:
    return {
          title: this.context.t("Do you really want to delete this module?"),
          btnLeft: this.context.t("Cancel"),
          btnRight: this.context.t("Delete the module"),
          textDesc: this.context.t("Deleting this module will delete all forms or child tasks."),
          type : "warning"
        };
  case ARCHIVE_PROCESS_MODAL_TEMPLATE:
	  return {
		  title: this.context.t("Do you really want to delete the process template?"),
		  btnLeft: this.context.t("Cancel"),
		  btnRight: this.context.t("Delete the process"),
		  textDesc: this.context.t("If you confirm the deleting of this process, you will no longer have access to information related to it"),
		  type : "warning"
	  };
  case ARCHIVE_PROCESS_MODAL:
    return {
          title: this.context.t("Do you really want to archive the process of this patient?"),
          btnLeft: this.context.t("Cancel"),
          btnRight: this.context.t("Archive the process"),
          textDesc: this.context.t("If you confirm the archiving of this process, you will no longer have access to information related to it"),
          type : "warning"
        };
  case DELETE_TASK_MODAL:
    return {
        title: this.context.t("Do you really want to delete this task?"),
  			btnShowModal: this.context.t("Delete"),
  			btnLeft: this.context.t("Cancel"),
  			btnRight: this.context.t("Delete the task"),
  			textDesc: this.context.t("The selected task will be permanently deleted."),
        type : "warning"
      };
  case DATASET:
    return {
      title: this.context.t("Do you want a new data set?"),
      btnLeft: this.context.t("Recovering data set"),
      btnRight: this.context.t("New data set"),
      type: "info"
      };
  default:
  }
}

  render() {
    const { isVisible } = this.state;
    const modalData = this.generateModalData();
    return (
      <div>
      <Modal
        visible={isVisible}
        title={modalData.title}
        onCancel={this.handleCancel}
        closable={false}
        maskClosable={false}
        footer={[
            <Button key="cancel" onClick={this.handleCancel}>{modalData.btnLeft}</Button>,
            <Button key="supprimer" type="primary" onClick={this.actionClick}>{modalData.btnRight}</Button>,
          ]}>
          { modalData.type === "info" ?  <Icon type="question-circle-o" style={{ fontSize: 52, flex: 1, alignItems: 'center', display:'block' }} /> : <Icon type="warning" style={{ fontSize: 52, color: '#ff4500', flex: 1, alignItems: 'center', display:'block' }} /> }

          <br></br>
          <b>{modalData.textDesc}</b>
      </Modal>
</div>
    );
  }

}

TmoConfirmModal.contextTypes = {
	t: PropTypes.func.isRequired
}


export default TmoConfirmModal;
