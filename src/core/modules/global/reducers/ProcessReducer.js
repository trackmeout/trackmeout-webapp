import {createSelector} from "reselect";
import { PROCESS_FETCHED, UPDATE_FILTER_LIST, FETCH_FILTER_LIST, PROCESS_EMPTY, PROCESS_TEMPLATE_FETCHED} from "../Types";

//PROCESSUS REDUCER
export default function patientProcesses(state = {}, action = {}) {
	switch (action.type) {
		case UPDATE_FILTER_LIST:
			return action.data.entities.processes;
		case PROCESS_EMPTY:
			return {};
        case PROCESS_TEMPLATE_FETCHED:
            return {...state, ...action.data.entities.processes};
		case PROCESS_FETCHED:
			if(action.data.entities.processes!==undefined)
				return action.data.entities.processes;
			else {
				return {};
			}
		default:
			return state;
	}
};


// SELECTORS
export const processTemplateSelector = state => state.modules;
export const allProcessesTemplateSelector = createSelector(processTemplateSelector, ModulesTemplateHash =>
    Object.values(ModulesTemplateHash)
);
export const patientProcessesSelector = state => state.patientProcesses;
export const allPatientProcessesSelector = createSelector(patientProcessesSelector, ProcessHash =>
	Object.values(ProcessHash)
);
