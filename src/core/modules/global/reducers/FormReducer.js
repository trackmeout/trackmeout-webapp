import {createSelector} from "reselect";
import { FORM_TEMPLATE_FETCHED, FORM_BYID_FETCHED } from "../Types";

//FORM REDUCER
export default function forms(state = {}, action = {}) {
	switch (action.type) {
	case FORM_TEMPLATE_FETCHED:
		return {...state, ...action.data.entities.forms};
	case FORM_BYID_FETCHED:
		return {...state, ...action.data.entities.forms};
	default:
		return state;
	}
}

// SELECTORS
export const formsTemplateSelector = state => state.forms;
export const allFormsTemplateSelector = createSelector(formsTemplateSelector, FormsTemplateHash =>
	Object.values(FormsTemplateHash)
);
export const formsByIdSelector = state => state.forms;
export const formByIdSelector = createSelector(formsByIdSelector, FormsTemplateHash =>
	Object.values(FormsTemplateHash)
);
