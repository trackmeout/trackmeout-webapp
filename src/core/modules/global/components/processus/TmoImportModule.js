import React, {Component} from "react";
import { Checkbox,  Icon,Col, Row, message } from "antd";
import _ from "lodash";
import TmoModal from "../common/TmoModal";
import {RESP_MODAL_CANCEL, RESP_MODAL_OK} from "../../Types";
import {TEMPLATE_MODE} from "../../Types";

import { addModulesFromTemplate } from "../../actions/ModuleAction";

import TmoFormComponent from "./TmoFormComponent";
import TmoTreeElementModalComposant from "./TmoTreeElementModalComposant";
import PropTypes from "prop-types";


class TmoImportModule extends Component {
	//Class to import module from template

	/* Constructions
  **********************************/
	constructor(props) {
		super(props);
		this.state = {
			modalVisible: false,
			moduleSelected: false,
			searchValue: "",
			moduleChecked: [],
		};
	}

	componentWillReceiveProps(nextProps) {
		this.processModules 	= JSON.parse(JSON.stringify(nextProps.modalData));
		this.process 			= JSON.parse(JSON.stringify(nextProps.process));
		this.moduleTemplates 	= JSON.parse(JSON.stringify(nextProps.moduleTemplates));
	}

	//Confirm if the import is done or failed
	getResponseModalTask = (resp) => {
		const _this = this;
		//Faire test
		switch (resp) {
		case RESP_MODAL_OK:
			addModulesFromTemplate(_this.checked, _this.props.process.id).then( res => {
				if(res === true){
					message.success(this.context.t("Import done"));
					_this.props.refreshPageFunction();
					this.setState({modalVisible: false});
				}
				else{
					message.error(this.context.t("Import failed"));
				}
			}).catch(( res ) =>{
				console.error(res);
			});

			break;
		case RESP_MODAL_CANCEL:
			this.setState({modalVisible: false});
			break;
		default:
			break;
		}
	};

	selectModule = (module) => {
		const { moduleSelected } = this.state;
		if(moduleSelected && module.id === moduleSelected.id) { return; }

		this.setState({moduleSelected: module});

	};


	onChange = (checkedValues) => {
		this.checked = checkedValues;
	}

	addFormOnModule = ( form ) => {
		console.log("Do nothing...");
	};


	getFormOfModuleTemplates = () => {

		const _this = this;
		var results =  JSON.parse(JSON.stringify(this.moduleSelected.forms));
		var currentIds = [];
		_.each(this.state.moduleSelected.forms, ( current ) => {
			currentIds.push(current.id);
		});


		return results;
	}

	getFormTemplatesNotUsed = () => {

		const _this = this;
		var results =  JSON.parse(JSON.stringify(this.moduleTemplates));
		var currentIds = [];
		_.each(this.state.moduleSelected.forms, ( current ) => {
			currentIds.push(current.id);
		});

		results = _.remove(results, function( currentForm ) {
			return !_.includes(currentIds,currentForm.id);
		});

		results = _.filter(results, function( currentForm ) {
			return _this.state.searchValue.trim() === "" || _.includes(currentForm.name.toLowerCase() ,_this.state.searchValue.toLowerCase());
		});

		return results;
	};

	addConvertScrollEvent = ( element ) => {
		if(element){
			var mouseSpeed = 0.75;

			element.addEventListener("mousewheel", (e) =>{
				e.preventDefault();
				element.scrollLeft += (e.deltaY * mouseSpeed);
			});
		}
	};


	countFormsInModule = ( module ) => {
		if(module.forms.length === 0){
			return true;
		}else {
			return false;
		}
	}

	render() {
		const _this = this;

		if(!this.processModules){
			return <div style={{display: "none"}}></div>;
		}

		const {modalVisible, moduleSelected} = this.state;
		var { modules } = this.processModules;
		// var { modules } = _this.process;

		var { moduleTemplates } = _this;

		if( _.isString(moduleSelected) ){
			var newModuleSelected = _.find(modules, function(currentModule) { return currentModule.id == moduleSelected; });
			this.setState({
				moduleSelected: newModuleSelected
			});
			this.forceUpdate();
		}

		let list=[];
		const modulesToRenderRow = moduleTemplates.map(function ( module ) {
			const isNewFormClass = module.isTemplate ? "module-new" : "";
			const isSelectedClass = moduleSelected && module.id === moduleSelected.id ? "selected" : "";

			let disabled = _this.countFormsInModule(module);
			if(!disabled) {
				if (!isSelectedClass) {
					//Generate list

					const isNewFormClass = module.isTemplate ? "module-new" : "";
					list.push(<li className={isNewFormClass} key={module.id} onClick={() => _this.selectModule(module)}>
						<Col key={module.id}><Checkbox defaultChecked={false} onClick={() => _this.selectModule(module)}
																				  key={module._id}
																				  value={module}>{module.name}</Checkbox>
						</Col>
					</li>);

				} else {

					const isSelectedClass = moduleSelected && module.id === moduleSelected.id ? "selected" : "";
					list.push(<li className={isSelectedClass} key={module.id} onClick={() => _this.selectModule(module)}>

						<Col key={module.id}><Checkbox defaultChecked={false} onClick={() => _this.selectModule(module)}
																				  key={module._id}
																				  value={module}></Checkbox>{module.name}</Col>
						<ul className="forms">
							{formsToRender}
						</ul>
					</li>);
				}
			}

		});

		const modulesToRender = (
			<Checkbox.Group  onChange={this.onChange}>
				<Row className="tmo-flex-wrapper-menu module-list">
					{ list }
				</Row>
			</Checkbox.Group>

		);

		var formsToRender;
		if(!moduleSelected){
			formsToRender = <div className="aligner"><p className="align-center text-not-selectable" style={{fontSize: "26px", color: "#d6d6d6"}}>{this.context.t("Please select a module to view its forms")}</p></div>;
		}else{

			/*var formTemplatesToRender = this.getFormTemplatesNotUsed().map(function (form) {
				return (<TmoModuleComponent  onClickOnCard={() => {_this.addFormOnModule(form);}} refreshPageFunction={_this.refreshPageFunction} key={form._id} data={form}/>);
			});*/

			var formTemplatesToRender = moduleSelected.forms.map(function (form) { // this.getFormOfModuleTemplates(moduleSelected.id).map(function (form) {
				return (<TmoFormComponent isPartOfOneTemplate={true} onClickOnCard={() => {_this.addFormOnModule(form);}} refreshPageFunction={_this.refreshPageFunction} key={form._id} data={form}/>);
			});

			formsToRender = (
				<div className="forms-template-list">
					{/*<input type="text" value={this.state.searchValue} onChange={this.onSearchChange} placeholder="Recherche textuelle" style={{width: "100%"}}/>*/}
					<div className="tmo-flex-wrapper-content" ref={(child) => { _this.wrapperContent = child; }} >
						{ formTemplatesToRender }
					</div>
				</div>
			);
		}

		const contentModalChoice = (
			<div className="tmo-flex-wrapper"  >
				<ul className="tmo-flex-wrapper-menu module-list">
					{ modulesToRender }
				</ul>
				{formsToRender}
			</div>
		);

		//Popup to confirm import or cancel
		var processName = this.process.name;
		const dataModal = {
			title: this.context.t("Import modules into ") + processName,
			typeContent: "react-component",
			valueContent: contentModalChoice,
			width: "90%",
			bodyStyle: {
				minHeight: "600px"
			},
			closable: false,
			buttons: [
				{
					key: "cancel",
					type: "",
					text: this.context.t("Cancel"),
					size: "large",
					function: "handleCancel",
					hasLoading: false
				},
				{
					key: "ok",
					type: "primary",
					text: this.context.t("Save all changes"),
					size: "large",
					function: "handleOk",
					hasLoading: false
				}
			]
		};

		return (
			<div>
				{modalVisible && (<TmoModal input="dada" data={dataModal} openModal={modalVisible}
					sendResponseModal={this.getResponseModalTask}/>)}
			</div>
		);
	}

	componentDidUpdate(){
		this.addConvertScrollEvent(this.wrapperContent);
	}
}

TmoImportModule.contextTypes = {
	t: PropTypes.func.isRequired
};

function mapStateToProps(state) {
	return {
		lang: state.i18nState.lang
	};
}

export default TmoImportModule;
