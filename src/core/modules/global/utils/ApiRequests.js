import axios from "axios";
import {config} from "../../../config";
import _ from "lodash";
import {LIST, DEFAULT, ERROR, SUCCESS} from "../Types";

axios.defaults.headers.common = {
  "X-Requested-With": "XMLHttpRequest"
};
axios.defaults.baseURL = (process.env.NODE_ENV !== "production")
  ? config.backendAddress
  : "";

export default {
  //all processus request to link to the backend requests
  processes: {

    /**
   * get all processes, by patient ID
   *
   * @param {type} id Description
   *
   * @return {type} Description
   */
    fetchPatientProcessesByPatientId: (id) => axios.get("processes/owner/" + id).then(res => {
      let processes = res.data.processes;
      if (processes !== undefined) {

        _.each(processes, function(currentProcess) {
          currentProcess.isSelected = true;
          currentProcess.isVisible = true;
          let totalProcessesSelected = 0;

          _.each(currentProcess.modules, function(currentModule) {
            currentModule.isSelected = true;
            currentModule.isVisible = true;
            let totalModulesSelected = 0;

            _.each(currentModule.forms, function(currentForm) {
              currentForm.isSelected = true;
              currentForm.isVisible = true;

              let arrTemp = [];
              let size = _.size(currentForm.tasks);

              currentForm.tasks.map(function(task) {
                if (task.type === LIST) {
                  const defaultValueID = task.defaultValueID;
                  let item = _.find(task.childDataSet[0].dataset, function(o) {
                    return o.id === defaultValueID;
                  });
                  arrTemp.push(item.status);
                } else {
                  arrTemp.push(task.state);
                }
              });

              var sizeSuccess = _.countBy(arrTemp, function(c) {
                return c === SUCCESS;
              });
              var sizeDefault = _.countBy(arrTemp, function(c) {
                return c === DEFAULT;
              });
              var sizeError = _.countBy(arrTemp, function(c) {
                return c === ERROR;
              });

              if (size > 0) {
                if (sizeSuccess[true] === size) {
                  currentForm.colorStatus = "cardSuccess";
                  currentForm.isSelected = false;
                  currentForm.isVisible = false;
                } else if (sizeDefault[true] === size) {
                  currentForm.colorStatus = "cardDefault";
                } else if (sizeError[true] > 0) {
                  currentForm.colorStatus = "cardError";
                } else {
                  currentForm.colorStatus = "cardWarning";
                }
              } else {
                currentForm.colorStatus = "cardDefault";
              }

              if (currentForm.isSelected === true) {
                totalModulesSelected++;
              }
            });

            if (totalModulesSelected === 0) {
              currentModule.isVisible = false;
              currentModule.isSelected = false;
            }

            if (currentModule.isSelected === true) {
              totalProcessesSelected++;
            }
          });

          if (totalProcessesSelected === 0) {
            currentProcess.isVisible = false;
            currentProcess.isSelected = false;
          }

        });
        return processes;
      } else {
        return [];
      }
    }),
    /**
 * create processes
 *
 * @param {type} id Description
 *
 * @return {type} Description
 */
    create: process => {
      axios.post("/processes", {process}).then(res => res.data.process);
    },
    /**
		 * create processes from a template proceses
		 *
		 * @param {type} id Description
		 *
		 * @return {type} Description
		 */
    addProcessesFromTemplate: (processes, patientID) => axios.post("/processes/add-processes-from-template", {processes, patientID}).then(res => res.data.success),
    /**
		 * get all process tempaltes
		 *
		 * @param {type} id Description
		 *
		 * @return {type} Description
		 */
    fetchProcessTemplates: () => axios.get("/processes/are-templates").then(res => res.data.processes),

    /**
		 * convert a process to template process
		 *
		 * @param {type} id Description
		 *
		 * @return {type} Description
		 */
    convertAsTemplate: process => {
      return axios.post("/processes/convert-as-template", {process});
    },
    /**
		 * archive a process
		 *
		 * @param {type} id Description
		 *
		 * @return {type} Description
		 */
    archive: processId => {
      axios.put("/processes/archive", {processId}).then(res => res.data.process);
    },
    /**
		 * add a module to a process
		 *
		 * @param {type} id Description
		 *
		 * @return {type} Description
		 */
    addModuleToProcess: data => axios.post("/processes/add-module-from-template", {data}), //Ajout un module a un process
    /**
			 * add module template to a normal process
			 *
			 * @param {type} id Description
			 *
			 * @return {type} Description
			 */
    addModuleTempToProcessTemp: (id, data) => axios.post("/processes/add-module-temp-to-proc-temp", {id, data}), //Ajout un module a un process
    /**
			 * edit a process
			 *
			 * @param {type} id Description
			 *
			 * @return {type} Description
			 */
    edit: process => {
      axios.put("/processes", {process}).then(res => res.data.process);
    }
  },

  modules: {

    /**
   * get all modules templates
   *
   * @return {type} Description
   */
    fetchModuleTemplates: () => axios.get("/modules/templates").then(res => res.data.modules),
    /**
		 * get a module template by his parent ID
		 *
		 * @param {type} id Description
		 *
		 * @return {type} Description
		 */
    fetchModuleTemplatesByParentsId: (parentId) => axios.get("/modules/templates/parent/" + parentId).then(res => res.data.modules),
    /**
		 * get a module template by his own ID
		 *
		 * @param {type} id Description
		 *
		 * @return {type} Description
		 */
    fetchModuleTemplateById: (id) => axios.get("/modules/templates/" + id).then((res) => {
      return res.data.modules;
    }),

    /**
                      * convert a module normal to a template
                      *
                      * @param {type} module Description
                      *
                      * @return {type} Description
                      */
    convertAsTemplate: module => {
      return axios.post("/modules/convert-as-template", {module});
    },

    /**
                           * add a module to a process from a module tempaltes
                           *
                           * @param {type} modules   Description
                           * @param {type} processID Description
                           *
                           * @return {type} Description
                           */
    addModulesFromTemplate: (modules, processID) => axios.post("/modules/add-modulesToProcess-from-template", {modules, processID}).then(res => res.data.success), //Ajout plusieurs modules a un process

    /**
                    * add form to a module
                    *
                    * @param {type} data Description
                    *
                    * @return {type} Description
                    */
    addFormToModule: data => axios.post("/modules/add-form-from-template", {data}), //Ajout un module a un formulaire

    /**
                      * add forms to a modules
                      *
                      * @param {type} data Description
                      *
                      * @return {type} Description
                      */
    addFormsToModules: data => axios.post("/modules/add-forms-from-template", {data}), //Ajout plusieurs modules a plusieurs formulaires

    /**
           * create a new module
           *
           * @param {type} module Description
           *
           * @return {type} Description
           */
    create: module => axios.post("/modules", {module}).then(res => res.data.module),

    /**
         * edit a module
         *
         * @param {type} module Description
         *
         * @return {type} Description
         */
    edit: module => axios.put("/modules", {module}).then(res => res.data.module),

    /**
           * delete a module
           *
           * @param {type} module Description
           *
           * @return {type} Description
           */
    delete: module => axios({method: "delete", url: "/modules", data: {
        module
      }})
  },

  patients: {

    /**
             * get all patients
             *
             * @return {type} Description
             */
    fetchAll: () => axios.get("patients").then(res => res.data.patients),

    /**
           * create a patient
           *
           * @param {type} patient Description
           *
           * @return {type} Description
           */
    create: patient => axios.post("/patients", {patient}).then(res => res.data.patient)
  },

  datasets: {

    /**
             * get all datasets
             *
             * @return {type} Description
             */
    fetchAll: () => axios.get("/datasets/").then(res => res.data.dataSet)
  },

  tasks: {

    /**
           * delete a task
           *
           * @param {type} task Description
           *
           * @return {type} Description
           */
    delete: task => axios({method: "delete", url: "/tasks", data: {
        task
      }}),

    /**
           * create a tasks
           *
           * @param {type} task Description
           *
           * @return {type} Description
           */
    create: task => {
      axios.post("/tasks", {task}).then(res => res.data.task);
    },

    /**
         * edit a task
         *
         * @param {type} task Description
         *
         * @return {type} Description
         */
    edit: task => {
      axios.put("/tasks", {task}).then(res => res.data.task);
    }
  },

  forms: {

    /**
     * get templates formular
     *
     * @return {type} Description
     */
    fetchFormTemplates: () => axios.get("/forms/templates").then(res => res.data.forms),

    /**
     * get forms by ID
     *
     * @param {type} id Description
     *
     * @return {type} Description
     */
    fetchFormsById: (id) => axios.get("/forms/owner" + id).then((res) => {
      return res.data.form;
    }),

    /**
    * get form templates by ID
    *
    * @param {type} id Description
    *
    * @return {type} Description
    */
    fetchFormTemplateById: (id) => axios.get("/forms/templates/" + id).then((res) => {
      return res.data.form;
    }),

    /**
     * delete a form
     *
     * @param {type} form Description
     *
     * @return {type} Description
     */
    delete: form => axios({method: "delete", url: "/forms", data: {
        form
      }}),

    /**
   * create a form
   *
   * @param {type} form Description
   *
   * @return {type} Description
   */
    create: form => {
      axios.post("/forms", {form}).then(res => res.data.form);
    },

    /**
   * edit a form
   *
   * @param {type} form Description
   *
   * @return {type} Description
   */
    edit: form => {
      axios.put("/forms", {form}).then(res => res.data.form);
    },

    /**
    * convert a form as template form
    *
    * @param {type} form Description
    *
    * @return {type} Description
    */
    convertAsTemplate: form => {
      return axios.post("/forms/convert-as-template", {form});
    }
  },

  documents: {

    /**
    * save documents
    *
    * @param {type} documents Description
    *
    * @return {type} Description
    */
    saveDocuments: documents => axios.post("/documents", documents).then(res => res.data),

    /**
     * delete documents
     *
     * @param {type} document Description
     *
     * @return {type} Description
     */
    delete: document => axios({method: "delete", url: "/documents", data: {
        document
      }}),

    /**
    * get documents by id
    *
    * @param {type} id Description
    *
    * @return {type} Description
    */
    fetchDocumentById: (id) => axios.get("/documents/" + id).then((res) => {
      return res.data;
    })
  },

  user: {

    /**
     * create a new user
     *
     * @param {type} user Description
     *
     * @return {type} Description
     */
    create: user => {
      return axios.post("/users/signup/", {user}).then(res => res.data);
    },

    /**
    * authentification
    *
    * @param {type} credentials Description
    *
    * @return {type} Description
    */
    login: credentials => axios.post("/users/login/", {credentials}).then(res => res.data)
  }
};
