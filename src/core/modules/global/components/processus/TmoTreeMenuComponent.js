import React, { Component } from "react";
import  TmoTreeNodeComponent, { INSTANCE_PROCESS } from "./TmoTreeNodeComponent";
import { updateFilter, archiveProcess } from "../../actions/ProcessesAction";
import { connect } from "react-redux";
import {allPatientProcessesSelector} from "../../reducers/ProcessReducer";

class TmoTreeMenuComponent extends Component {
	constructor(props) {
		super(props);
		this.patientProcesses = this.props.patientProcesses;
		this.state = {
			isCollapsed: false
		};
	}

    componentWillReceiveProps(nextProps) {
        this.patientProcesses = nextProps.patientProcesses;
    }

    //Update the process
	updateProcesses = () => {
		this.props.updateFilter( this.patientProcesses );
	};

	render() {
		const _this = this;
		const processes = this.patientProcesses.map(function ( process ) {
			return <TmoTreeNodeComponent moduleTemplates={_this.props.moduleTemplates}  formTemplates={_this.props.formTemplates} refreshPageFunction={_this.props.refreshPageFunction} key={process.id} nodeData={ process } instanceType={ INSTANCE_PROCESS } parentNode={_this} />;
		});

		return 	<ul style={{padding: "20px 0px"}} className="noselect">{ processes }</ul>;
	}
}


function mapStateToProps(state) {
	this.patientProcesses = state.patientProcesses;
	return {
		patientProcesses: allPatientProcessesSelector(state)
	};
}

const mapDispatchToProps = {
	updateFilter,
	archiveProcess
};

export default connect(mapStateToProps, mapDispatchToProps)(TmoTreeMenuComponent);
