import React, {Component} from "react";
import {connect} from "react-redux";
import PropTypes from "prop-types";
import SignUpForm from "../components/User/SignUpForm";


class SignupPage extends Component {
	//Render the SignUpForm component
	render() {
		return (
			<div align="center" style={{width: 500, margin: "auto", paddingTop: "100px"}}>
					<h1>{this.context.t("Create account")}</h1>
				 <SignUpForm />
			</div>
		);
	}
}

SignupPage.contextTypes = {
	t: PropTypes.func.isRequired
};

function mapStateToProps(state) {
	return {
		lang: state.i18nState.lang
	};
}

export default connect(null)(SignupPage);
