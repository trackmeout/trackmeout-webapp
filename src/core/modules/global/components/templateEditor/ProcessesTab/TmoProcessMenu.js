import React, {Component} from "react";
import { Menu, Icon, Card, Button } from "antd";
const SubMenu = Menu.SubMenu;
const MenuItemGroup = Menu.ItemGroup;

class TmoProcessMenu extends Component {

  state = {
  	collapsed: false,
  }

toggleCollapsed = () => {
	this.setState({
		collapsed: !this.state.collapsed,
	});
}
  handleClick = (e) => {
  	console.log("click ", e);
  }

  renderMenu = () => {
  	for (var i = 0; i < 3; i++) {
  		return <Menu.Item key={i} onClick={this.toggleCollapsed} ><span>{"processus " + i}</span></Menu.Item>;
  	}
  }

  render() {
  	return (
  		<div id="template-editor-module-container">

  			<div className="module-template-available-modules-list">
  				<div style={{minHeight: "580px"}}>
  					<Button type="primary" onClick={this.toggleCollapsed} style={{ marginBottom: 16 }}>
  						<Icon type={this.state.collapsed ? "menu-unfold" : "menu-fold"} />
  					</Button>
  					<Menu
  						mode="inline"
  						inlineCollapsed={this.state.collapsed}
  						style={{marginTop: "10px"}}>
  						{this.renderMenu()}
  					</Menu>
  				</div>
  			</div>
  		</div>

  	);
  }
}
export default TmoProcessMenu;
