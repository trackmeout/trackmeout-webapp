var Async = require("../lib/Async");
var assert = require("chai").assert;
var expect = require("chai").expect;

describe ("wait", function(){
	before(function () {
		// do something before the test
	});

	beforeEach(function () {
		// do something before the each test
	});

	it("should exit", function(){
		expect(Async.wait).to.be.a("function");
	});

	it("should wait", function(done){
		Async.wait(300, function(test){
			expect(test).to.equal(18);
			done();
		});
	});
});