import { USER_CREATED, USER_LOGGED_OUT, USER_LOGGED_IN } from "../Types";

//USER REDUCER
export default function users(state = {}, action = {}) {
	switch (action.type) {
		case USER_LOGGED_IN:
			return action.user;
		case USER_LOGGED_OUT:
			return {};
		case USER_CREATED:
			return {...state, ...action.data.entities.users};
		default:
			return state;
	}
}
