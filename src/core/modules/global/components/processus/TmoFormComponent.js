import React, {Component} from "react";
import { deleteForm, editForm } from "../../actions/FormAction";
import { createTask } from "../../actions/TaskAction";
import TmoTaskComponent from "./TmoTaskComponent";
import TmoConfirmModal, {DELETE_FORM_MODAL} from "../common/TmoConfirmModal";
import TmoTreeElementModalComposant, { EDIT_FORM_MODAL, ADD_TASK_MODAL, EDIT_FORM_TASKS} from "./TmoTreeElementModalComposant";
import TmoCreateTask from "../processus/TmoCreateTask";
import {DEFAULT, EDIT_MODE, READ_MODE, TEMPLATE_MODE} from "../../Types";
import { Card, Dropdown, Icon, Menu } from "antd";
import _ from "lodash";
import PropTypes from "prop-types";
import {connect} from "react-redux";


class TmoFormComponent extends Component {

	/* Constructions
    **********************************/
	constructor(props) {
		super(props);

		this.state = {
			modeEditionVisible : false
		};
		this.colorStatus = DEFAULT;
		this.currentMode = READ_MODE;
		this.isPartOfOneTemplate = !!this.props.isPartOfOneTemplate;

		this.isClicable = !!this.props.onClickOnCard;
		this.hideBorders = !!this.props.hideBorders;

		if(this.props.currentMode !== undefined){
			this.currentMode = this.props.currentMode;
		}
	}

	componentWillReceiveProps( nextProps ){
		this.props = nextProps;
		this.settingColor();
	}

	componentWillMount() {
		this.settingColor();
	}

	/* Functions
    **********************************/
	settingColor = () => {
		this.colorStatus = this.props.data.colorStatus;
	};

    clickOnCard = () => {
    	if(this.isClicable){
    		this.props.onClickOnCard();
    	}
    };

    //Make the selected elemented visible
    openTreeModal = (element) => {
    	switch ( element.item.props.modalType ){
    	case DELETE_FORM_MODAL:
    		this.deleteFormModal.setState({ isVisible : true });
    		break;
    	case EDIT_FORM_MODAL:
    		this.editFormModal.setState({ isVisible : true });
    		break;
    	case ADD_TASK_MODAL:
    		this.addTaskModal.showModalNewTask();
    		break;
    	case EDIT_FORM_TASKS:
    		this.setState({ modeEditionVisible : !this.state.modeEditionVisible });
    		break;
    	default:

    	}
    };

	/* Render
    **********************************/
    render() {
    	const _this = this;
    	const { name, tasks, _id, isPartOfOneTemplate, isTemplate } = this.props.data;
    	const { modeEditionVisible } = this.state;
    	let sizeTasks = _.size( tasks );

    	//Generate tasks
    	const tasksList = tasks.map(function ( task ) {
    		return <TmoTaskComponent isPartOfOneTemplate={_this.isPartOfOneTemplate} mode={READ_MODE} key={ task._id } data={ task } nameForm={ name } modeEdit={ modeEditionVisible } refreshPageFunction={ _this.props.refreshPageFunction }/>;
    	});

    	//CSS Styling
    	const cardStyle = {
    		width: modeEditionVisible && sizeTasks > 0 ? 400 : 400,
    		marginTop: "20px",
    		marginLeft: "20px"
    	};

    	const iconStyle = {
    		fontSize: 18,
    		color: "#999"
    	};

    	//Menu Dropdown
    	const menu = (
    		<Menu onClick={this.openTreeModal}>
    			<Menu.Item  modalType={ EDIT_FORM_MODAL } key="1">{this.context.t("Rename form")}</Menu.Item>
    			<Menu.Item modalType={ DELETE_FORM_MODAL } key="2">{this.context.t("Delete form")}</Menu.Item>
    			{ sizeTasks > 0 ? <Menu.Item modalType={ EDIT_FORM_TASKS } key="3" >{ modeEditionVisible ? this.context.t("Hide edit mode") : this.context.t("Edit tasks form")  }</Menu.Item> : "" }
    			<Menu.Divider/>
    			<Menu.Item modalType={ ADD_TASK_MODAL } key="4"  >{this.context.t("Add task")}</Menu.Item>
    		</Menu>
    	);

    	const dropDown =(<Dropdown overlay={ menu } trigger={["click"]} placement="bottomRight">
    		<a className="ant-dropdown-link" href="">
    			<Icon type="ellipsis" style={ iconStyle } className={ "ellipsis-rot" }/>
    		</a>
    	</Dropdown>);

    	var cardClassName = isPartOfOneTemplate ? " " : this.colorStatus;
    	var containerClassName = "";

    	if(this.isClicable){
    		cardClassName += " template-form";
    	}

    	if(this.hideBorders){
            cardClassName += " no-borders";
		}

    	return (
    		<div  className={ containerClassName }  style={ cardStyle }>
    			<Card onClick={this.clickOnCard} title={ name }  className={ cardClassName } extra={this.isClicable ? <div></div> : dropDown } >
    				{ tasksList }
    			</Card>
    			<TmoConfirmModal refreshPageFunction={ _this.props.refreshPageFunction } ref={(child) => { _this.deleteFormModal = child; }} modalData={ this.props.data } modalType={ DELETE_FORM_MODAL } />
    			<TmoCreateTask  isPartOfOneTemplate={this.isPartOfOneTemplate}  mode={ READ_MODE } refreshPageFunction={ _this.props.refreshPageFunction } ref={(child) => { this.addTaskModal = child; }} parent={ _id }/>
    			<TmoTreeElementModalComposant refreshPageFunction={ _this.props.refreshPageFunction }  ref={(child) => { _this.editFormModal = child; }}  modalType={ EDIT_FORM_MODAL } modalData={ this.props.data }/>
    		</div>
    	);
    }
}

TmoFormComponent.contextTypes = {
	t: PropTypes.func.isRequired
};

function mapStateToProps(state) {
	return {
		lang: state.i18nState.lang
	};
}

export default TmoFormComponent;
