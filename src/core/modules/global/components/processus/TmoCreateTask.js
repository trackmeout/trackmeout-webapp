import React, { Component } from "react";
import { Select, Button, Row, Col, Divider, List, Icon, Badge, Checkbox, Tooltip, Tabs, Spin, Input, Upload, message} from "antd";
import TmoModal from "../common/TmoModal";
import TmoInputText from "../common/TmoInputText";
import PropTypes from "prop-types";
import { createTask, editTask } from "../../actions/TaskAction";
import { fetchDatasets } from "../../actions/DatasetAction";
import { saveDoc } from "../../actions/DocumentsAction";
import { LIST, STRING, DEFAULT, ERROR, SUCCESS, WARNING, RESP_MODAL_OK, RESP_MODAL_CANCEL, EDIT_MODE, TEMPLATE_MODE } from "../../Types";
import _ from "lodash";
import {connect} from "react-redux";

class TmoCreateTask extends Component {

	/* Constructions
  **********************************/
	constructor( props ) {
		super( props );


		const isPartOfTemplate = !!this.props.isPartOfOneTemplate;

		this.state = {
			modalVisible: false,
			modalChoiceVisible: false,
			modalDataSet: false,
			loading: false,
			newDataSetList:[],
			storedDataSetList:[],
			iconDateSetList:"",
			iconGray:true,
			iconOrange:false,
			iconRed:false,
			statusResolved:false,
			errorChoiceDefault:false,
			errorDataSetEmpty:false,
			errorNotDatasetSelected: false,
			fileList: [],
			paneActivedDataset: "10",
			paneActivedMain:"1"
		};
		//Data for task
		this.task = {};
		this.task.label = "";
		this.task.isPartOfOneTemplate = isPartOfTemplate;
		this.task.type = STRING;
		this.task.parentForm = this.props.parent;
		this.task.state = DEFAULT;
		this.task.textAide = "";
		//Data choice (dataset element)
		this.choice = {};
		this.choice.label = "";
		this.choice.state = DEFAULT;
		this.choice.default = "";
		//Mode edition, data
		this.choiceEdited = null;
		this.taskEdited = null;
		//Inputs
		this.inputModalChoice = null;
		this.inputModalTask = null;
		//All DATASETS
		this.allDataSets = null;
		this.selectedDataSetList=null;
	}

	componentWillReceiveProps( nextProps ) {

		//VERIFIER CHRISTIAN
		if( this.props.mode === EDIT_MODE  && !!this.props.data){
			this.props = nextProps;
			this.taskEdited = this.props.data;
			this.task.label = this.taskEdited.label;
			this.task.parentForm = this.taskEdited.parentForm;
			this.task._id = this.taskEdited._id;
			this.task.type = this.taskEdited.type;
			this.task.state = this.taskEdited.state;
			this.task.textAide = this.taskEdited.textAide;
			if( this.task.type === LIST ){
				this.task.currentDataSetId = this.taskEdited.currentDataSetId;
				this.setState({
					dataSetVisible : true,
					newDataSetList : this.taskEdited.childDataSet[0].dataset,
				});
				this.choice.default="";
			}
		}
	}
	/* Fonctions
	**********************************/
	showModalNewTask = () => {
		this.setState({ modalVisible: true });
	}

	//delete the task selected
  deleteChoice = ( value ) => {
  	this.setState({ newDataSetList: this.state.newDataSetList.filter(item => item.id !== value)});
  	this.resetCombo();
  }

  	//edit the task selected
  editChoice = ( choice ) => {
  	this.setState({ modalChoiceVisible: true });
  	this.resetCombo();
  	this.choiceEdited = choice;
  	this.choice.label = this.choiceEdited.title;
  	this.choice.state = this.choiceEdited.status;
  	switch ( this.choiceEdited.status ) {
	  	case DEFAULT:
			  this.setState({
	  			iconGray: true,
	  			iconRed: false,
	  			iconOrange: false
			 	});
  			break;
	  	case WARNING:
	  		this.setState({
	  			iconGray: false,
	  			iconRed: false,
	  			iconOrange: true
	  		});
	  		break;
	  	case ERROR:
	  		this.setState({
	  			iconGray: false,
	  			iconRed: true,
	  			iconOrange: false
	  		});
	  		break;
	  	case SUCCESS:
	  		this.setState({ statusResolved: true });
	  		break;
  	default:
  		break;
  	}
  }

  addChoice = () => {
  	this.setState({ modalChoiceVisible: true});
  }

  handleTypeTask = (value) => {
  	if( value === LIST ){
  		this.task.type = value;
  		this.setState({ dataSetVisible:true });
  	}
  	else{
  		this.task.type = value;
  		this.setState({
  			dataSetVisible: false
			 });
  	}
  }

	setDatasetVisible = () => {
		this.setState({ dataSetVisible:true });
	}

	switchTabMain  = (e) => {;
		this.setState({
			paneActivedMain: e
		});
	}

	switchTabDataset = (e) => {
		this.setState({
			paneActivedDataset: e,
			errorNotDatasetSelected: false,
			errorDataSetEmpty: false,
			errorChoiceDefault: false
		}, () => {
			if(this.allDataSets === null && this.state.paneActivedDataset === "11"){
				this.setState({ loading: true });
				fetchDatasets().then((datasets) => {
					this.allDataSets = datasets;
					this.setState({
						loading: false,
						storedDataSetList: datasets
				  });
				});
			}
		});
	}

	getDataInputModalChoice = ( newValue ) =>{
		this.choice.label = newValue;
	}

	getDataInputModalTask = ( newValue ) =>{
		this.task.label = newValue;
	}

	showErrorDefaultChoice = () =>{
		this.setState({ errorChoiceDefault: true });
	}

	showErrorNoDataSet = () =>{
		this.setState({ errorDataSetEmpty: true });
	}

	showErrorNoDatasetSelected = () =>{
		this.setState({ errorNotDatasetSelected: true });
	}

	resetCombo = () => {
		this.choice.default = "";
		let element = document.getElementsByClassName("ant-select-selection__clear");
		for (var i = 0; i < element.length; i++) {
			element[i].click();
		}
	}

	resetInfosModalTask = () => {
		this.setState({
			modalVisible: false,
			dataSetVisible: false,
			newDataSetList: [],
			statusResolved: false,
			errorNotDatasetSelected: false,
			iconDateSetList: "",
			fileList: [],
			uploading: false,
			paneActivedMain : "1",
			paneActivedDataset : "10"
		});
		this.task.label = "";
		this.allDataSets = null;
		this.task.type = STRING;
		this.task.textAide = "";
		this.props.refreshPageFunction();
	}

	resetInfosModalChoice = () => {
		this.setState({
			modalChoiceVisible: false,
			iconGray: true,
			iconOrange: false,
			iconRed: false,
			statusResolved: false,
			errorDataSetEmpty: false,

		});
		this.choice.state = DEFAULT;
		this.choice.label = "";
		this.choiceEdited = null;
	}


	getResponseModalTask= ( resp ) =>{
		//Faire test
		switch ( resp ) {
		case RESP_MODAL_OK:
			if( this.task.label !== "" ){
				if( this.task.type !== LIST ){
					this.task.value = "-";
					if( this.props.mode === EDIT_MODE ){
						if( this.task.type === this.taskEdited.type ){
							this.task.value = this.taskEdited.value;
							this.task.state = this.taskEdited.state;
						}
						else{
							if(this.taskEdited.type === LIST){
								this.task.defaultValueID = null;
								this.task.currentDataSetId = null;
							}
							this.task.state = DEFAULT;
						}
						if(Object.keys(this.state.fileList).length > 0){
							this.handleUpload();
						}
						else{
							editTask( Object.assign({}, this.task) );
							this.resetInfosModalTask();
						}
					}
					else{
						if(Object.keys(this.state.fileList).length > 0){
							this.handleUpload();
						}
						else{
							createTask( Object.assign({}, this.task) );
							this.resetInfosModalTask();
						}
					}
				}
				else if( this.state.newDataSetList.length > 0 || this.state.paneActivedDataset === "11"){
					if(this.selectedDataSetList !== undefined || this.state.paneActivedDataset === "10"){
						if( this.choice.default !== "" && this.choice.default !== undefined ){
							this.task.defaultValueID =  this.choice.default;
							if(this.state.paneActivedDataset === "10")
								this.task.childDataSet = { dataset: this.state.newDataSetList };
							else if(this.state.paneActivedDataset === "11"){
								this.task.currentDataSetId = this.selectedDataSetList._id ;
							}
							if(this.props.mode === EDIT_MODE){
								if( this.task.type === this.taskEdited.type ){
									this.task.value = this.taskEdited.value;
									this.task.state = this.taskEdited.state;
								}
								else{
									this.task.state = DEFAULT;
								}
								if(Object.keys(this.state.fileList).length > 0){
										  this.handleUpload();
								}
								else{
									editTask( Object.assign({}, this.task) );
									this.resetInfosModalTask();
								}
							}
							else{
								if(Object.keys(this.state.fileList).length > 0){
									this.handleUpload();}
								else{
									createTask( Object.assign({}, this.task) );
									this.resetInfosModalTask();
								}
							}

						}else{
							this.showErrorDefaultChoice();
							this.setState({ paneActivedMain : "1" });
						}}
					else {
						this.showErrorNoDatasetSelected();
						this.setState({ paneActivedMain : "1" });}
				}
				else{
					this.showErrorNoDataSet();
					this.setState({ paneActivedMain : "1" });
				}
			}
			else{
				this.setState({ paneActivedMain : "1"});
				this.inputModalTask.showError();
			}
			break;
		case RESP_MODAL_CANCEL:
			this.resetInfosModalTask();
			break;
		default:
			break;
		}
	}

	getResponseModalChoice= ( resp ) =>{
		//Responde click modal
		switch ( resp ) {
		case RESP_MODAL_OK:
			if( this.choice.label !== "" ){
				//Status resolved checked
				if( this.state.statusResolved )
					this.choice.state = SUCCESS;
				if( this.choiceEdited === null ){
					//Data to send
					let data = {
						id: Date.now(),
						title: this.choice.label,
						status: this.choice.state
					};
					this.setState({ newDataSetList: [...this.state.newDataSetList, data] });
				}
				else{
					//Modify dataSet
					let data=this.state.newDataSetList;
					for ( let i = 0 ; i < data.length; i++ ) {
						if( data[i].id === this.choiceEdited.id ){
							data[i].title = this.choice.label;
							data[i].status = this.choice.state;
						}
					}
					this.setState({ newDataSetList: data});
				}
				this.resetInfosModalChoice();
			}
			else
				this.inputModalChoice.showError();
			break;
		case RESP_MODAL_CANCEL:
			this.resetInfosModalChoice();
			break;
		default:
			break;
		}
	}

	//Set the color of the task depending of it's status
	colorPicker = e => {
		switch ( e.target.id ) {
		case "red" :
		case "iconRed":
			this.setState({
				iconRed: true,
				iconGray: false,
				iconOrange: false
			});
			this.choice.state = ERROR;
			break;
		case "orange":
		case "iconOrange":
			this.setState({
				iconRed: false,
				iconGray: false,
				iconOrange: true
			});
			this.choice.state = WARNING;
			break;
		case "gray":
		case "iconGray":
			this.setState({
				iconRed: false,
				iconGray: true,
				iconOrange: false
			});
			this.choice.state = DEFAULT;
			break;
		default:
			break;
		}
	}

	setEtatResolved = () => {
		this.setState({ statusResolved: !this.state.statusResolved });
	}

	setDefaultChoice = ( value ) => {
		this.choice.default = value;
		this.setState({ errorChoiceDefault: false });
	}

	cardSelected = (value) => {
		this.setState({
			iconDateSetList : value,
			errorNotDatasetSelected : false
		});
		this.resetCombo();
	}

	changeTextArea = (e) => {
		this.task.textAide = e.target.value;
	}

  handleUpload = async () => {
  	const { fileList } = this.state;
  	const formData = new FormData();
  	fileList.forEach((file) => {
  		formData.append("files[]", file);
  	});

  	this.modalTask.setState({
  		loading: true});

  	let res = await saveDoc(formData);
  	if(res.hasOwnProperty( "UPLOAD_OK")){
  		this.task.documents = res["UPLOAD_OK"];
	    this.setState({ fileList: []});
	    this.modalTask.setState({ loading: false});
	        message.success(this.context.t("Upload successful"));
  		if(this.props.mode === EDIT_MODE){
  			editTask( Object.assign({}, this.task) );
  		}
  		else{
			   createTask( Object.assign({}, this.task) );
		  }
			this.resetInfosModalTask();
  	}
  	else{
			    message.error(this.context.t("Upload failed"));
  			this.modalTask.setState({ loading : false
  		});
  	}
  }

	/* Render
  **********************************/
  render() {


  	const { modalVisible, modalChoiceVisible, iconDateSetList, errorNotDatasetSelected, dataSetVisible, storedDataSetList, newDataSetList, iconRed, iconOrange, iconGray, statusResolved, errorChoiceDefault, errorDataSetEmpty } = this.state;
  	const { mode } = this.props;
  	const Option = Select.Option;
  	const TabPane = Tabs.TabPane;
  	const { TextArea } = Input;

  	/********************* Modal Create Task ********************************/
  	const dataLabel = {
				 label: this.context.t("Wording"),
				 messageError: this.context.t("No value has been set"),
				 hintColor: "#999",
				 focusLabelColor: "#108ee9",
				 input: this.task.label,
				 size:18
			 };

  	const optionsComboNewDataset = [];
  	if(newDataSetList !== null){
  		_.each( newDataSetList, function (currentItem) {
  			optionsComboNewDataset.push(<Option value={ currentItem.id } key={ currentItem.id }><Badge status={ currentItem.status } />{ currentItem.title }</Option>);
  		});
  	}

  	const optionsComboStoredDataset = [];
  	this.selectedDataSetList = _.find(storedDataSetList, {_id:iconDateSetList});
  	if(this.selectedDataSetList !== undefined){
  		_.each( this.selectedDataSetList.dataset, function (currentItem) {
  			optionsComboStoredDataset.push(<Option value={ currentItem.id } key={ currentItem.id }><Badge status={ currentItem.status } />{ currentItem.title }</Option>);
  		});
  	}

  	const propsUpload = {
  		onRemove: (file) => {
  			this.setState(({ fileList }) => {
  				const index = fileList.indexOf(file);
  				const newFileList = fileList.slice();
  				newFileList.splice(index, 1);
  				return {
  					fileList: newFileList,
  				};
  			});
  		},
  		beforeUpload: (file) => {
  			this.setState(({ fileList }) => ({
  				fileList: [...fileList, file],
  			}));
  			return false;
  		},
  		fileList: this.state.fileList,
  	};

  	//Popup to create a task
  	const contentModalNewTask = (
  		<Tabs activeKey={this.state.paneActivedMain} size= "large" style={{ marginTop:-30, textAlign: "center" }} onTabClick={this.switchTabMain}>
		    <TabPane tab={<span><Icon type="profile" />{this.context.t("Details")}</span>} key="1" style={{ marginTop:20 }}>
  				<TmoInputText  data = { dataLabel } sendData={ this.getDataInputModalTask }  ref={(child) => { this.inputModalTask = child; }}></TmoInputText>
  				<h3 style= {{ textAlign: "left" }}>{this.context.t("Field type")}</h3>
  				<Select defaultValue={ this.task.type } onChange={ this.handleTypeTask }>
  					<Option value="String">{this.context.t("Text")}</Option>
  					<Option value="Date">{this.context.t("Date")}</Option>
  					<Option value="List">{this.context.t("List of choices")}</Option>
  				</Select>
  				{ dataSetVisible && (
  					<div className="jeuxDonnes">
  						<Divider style={{ marginTop: 10 }}>{this.context.t("Data set")}</Divider>
  						<Tabs activeKey={this.state.paneActivedDataset} size= "large" style={{ marginTop: 0 }} onTabClick={this.switchTabDataset}>
								 <TabPane style={{textAlign: "center", marginTop:0 }} tab={<span ><Icon type="plus-circle-o" />{this.context.t("New")}</span>} key="10">
  								<Button type="dashed" icon="plus" style={{float: "right"}} onClick={ () => this.addChoice() }>{this.context.t("New choice")}</Button>
  								<List  style={{ marginTop:50 }}
  									itemLayout="horizontal"
  									dataSource={ newDataSetList }
  									renderItem={ item => (
  										<List.Item style={{paddingBottom:0}} actions={[<Icon type="edit" onClick={ () => this.editChoice(item)} />, <Icon type="delete" onClick={ () => this.deleteChoice( item.id ) }/>]}>
  											<List.Item.Meta
  												title={(<p style= {{ textAlign: "left" }}><Badge status={ item.status } />{ item.title } </p>)}
  											/>
  										</List.Item>
  									)}
  								/>
  								<label className="error" style={{visibility: errorDataSetEmpty ? "visible" : "hidden", position:"relative"}}>
									{this.context.t("No affected data set")}
  								</label>
  								{
  									Object.keys( newDataSetList ).length > 0 &&
											<div>
												<Row style = {{ marginTop:30 }}>
													<Col span={12} style= {{ textAlign: "left" }}> <h3> {this.context.t("Default value")}</h3> </Col>
													<Col span={12}>
														<Select allowClear={ true } style={{ width: "100%" }} onChange={ this.setDefaultChoice } ref={(child) => { this.combo = child; }} >
															{optionsComboNewDataset}
														</Select>
													</Col>
												</Row>
												<label className="error" style={{ visibility: errorChoiceDefault ? "visible" : "hidden", position:"relative" }}>
													{this.context.t("Default value not affected")}
												</label>
											</div>
  								}
  							</TabPane>
  							<TabPane tab={<span><Icon type="download" />{this.context.t("Recover")}</span>}  key="11">
  								<Spin spinning={this.state.loading}>
  									<div id="listDataset">
  										<List  style={{ marginTop:10 }}
  											grid={{ column: 2 }}
  											dataSource={ storedDataSetList }
  											renderItem={ item => (
  												<List.Item key={ item._id }>
  													<div className="myCard" style={{ borderColor: iconDateSetList === item._id ? "green" : ""}} onClick={() => 	this.cardSelected(item._id)}>
  														{ item.dataset.map((object, i) => <p style= {{ textAlign: "left" }} key={ object.id }><Badge status={ object.status } />{ object.title } </p>)}
  														{ iconDateSetList === item._id && <Icon className="iconJeuxDonnes"  type="check" />}
  													</div>
  												</List.Item>
  											)}
  										></List>
										</div>
  									<label className="error" style={{ visibility: errorNotDatasetSelected ? "visible" : "hidden",  position:"relative" }}>
										{this.context.t("No data set selected")}
  									</label>
  									{ iconDateSetList !== "" &&
												<div>
													<Row style = {{ marginTop:30 }}>
														<Col span={12} style= {{ textAlign: "left" }}> <h3> {this.context.t("Default value")} </h3> </Col>
														<Col span={12}>
															<Select allowClear={ true } style={{ width: "100%" }} onChange={ this.setDefaultChoice }  >
																{ optionsComboStoredDataset }
															</Select>
														</Col>
													</Row>
													<label className="error" style={{ visibility: errorChoiceDefault ? "visible" : "hidden", position:"relative" }}>
														{this.context.t("Default value not affected")}
													</label>
												</div>
  									}
  								</Spin>
  							</TabPane>
  						</Tabs>
  					</div>
  				) }

  			</TabPane>
		    <TabPane tab={<span><Icon type="question-circle-o" />{this.context.t("Help")}</span>} key="2">
  				<TextArea rows={4} defaultValue={this.task.textAide} onChange={this.changeTextArea}/>
  			</TabPane>
		  </Tabs>

  	);

  	const dataModal = {
				 title: mode === EDIT_MODE ? this.context.t("Edit task") : this.context.t("Create a new task"),
				 typeContent: "react-component",
				 valueContent: contentModalNewTask,
				 closable: false,
				 buttons:[
					 {
						 key: "cancel",
						 type: "",
						 text: this.context.t("Cancel"),
						 size:"large",
						 function: "handleCancel",
						 hasLoading: false
					 },
					 {
						 key: "ok",
						 type: "primary",
						 text:"Ok",
						 size:"large",
						 function: "handleSubmit",
						 hasLoading: true
					 }
				 ]
			 };

  	/********************* Modal Create Choice ********************************/
		 const dataInput = {
			 label: this.context.t("New value"),
			 messageError: this.context.t("No value has been set"),
			 hintColor: "#999",
			 focusLabelColor: "#108ee9",
			 widthInput: "450px",
			 input: this.choice.label
		 };

		 let timer = 1;

		 const contentModalChoice = (
			  <div>
		 			<TmoInputText data = { dataInput } sendData={ this.getDataInputModalChoice }  ref={(child) => { this.inputModalChoice = child; }}/>
  			<Checkbox style={{ marginBottom:25 }} checked={ statusResolved } onClick={ this.setEtatResolved } >Statut résolu</Checkbox>
  			{
  				!statusResolved && (
  					<div>
  						<h3>Statut couleur</h3>
  						<div style={{ textAlign:"center", marginLeft:184 }} >
  							<Tooltip title={this.context.t("State error")} mouseEnterDelay= { timer }>
  								<div id='red' style={{ backgroundColor:"#f5222d", width:35, height: 35, borderBottomLeftRadius:7, borderTopLeftRadius:7, float:"left" }} onClick={ this.colorPicker }>
  									{ iconRed && <Icon id="iconRed" type="check" style={{ fontSize: 25, color: "white", paddingTop:5 }}/>}
  								</div>
  							</Tooltip>
  							<Tooltip title={this.context.t("State in progress")} mouseEnterDelay={ timer }>
  								<div id='orange' style={{ backgroundColor:"#ff9800", width:35, height: 35, float:"left" }} onClick={ this.colorPicker }>
  									{ iconOrange && <Icon id="iconOrange" type="check" style={{ fontSize: 25, color: "white", paddingTop:5 }}/> }
  								</div>
  							</Tooltip>
  							<Tooltip title= {this.context.t("State not started")} mouseEnterDelay={ timer }>
  								<div id='gray' style={{ backgroundColor:"#d9d9d9", width:35, height: 35, borderBottomRightRadius:7, borderTopRightRadius:7, float:"left" }} onClick={ this.colorPicker }>
  									{ iconGray && <Icon id="iconGray" type="check" style={{ fontSize: 25, color: "white", paddingTop:5 }}/>}
  								</div>
  							</Tooltip>
  						</div>
  						<div style={{ clear:"both" }}/>
  					</div>
  				)
  			}
  		</div>
  	);

		 const dataModalCreateChoice = {
		 		 title: this.context.t("Create new choice"),
		 		 typeContent: "react-component",
		 		 valueContent: contentModalChoice,
		 		 closable: false,
		 		 buttons:[
		 			 {
		 				 key: "cancel",
		 				 type: "",
		 				 text: this.context.t("Cancel"),
		 				 size:"large",
		 				 function: "handleCancel",
		 				 hasLoading: false
		 			 },
		 			 {
		 				 key: "ok",
		 				 type: "primary",
		 				 text:"Ok",
		 				 size:"large",
		 				 function: "handleOk",
		 				 hasLoading: false
		 			 }
		 		 ]
		 	 };
  	return (
  		<div>
			{ modalVisible && (<TmoModal input={ this.choice.label } ref={(child) => { this.modalTask = child;}} data= { dataModal } openModal={ modalVisible } sendResponseModal = { this.getResponseModalTask }/>) }
  			{ modalChoiceVisible && (<TmoModal data= { dataModalCreateChoice } openModal={ modalChoiceVisible } sendResponseModal = { this.getResponseModalChoice }/>) }
  		</div>
  	);
  }
}

TmoCreateTask.contextTypes = {
	t: PropTypes.func.isRequired
};

function mapStateToProps(state) {
	return {
		lang: state.i18nState.lang
	};
}

export default TmoCreateTask;
