import api from "../utils/ApiRequests";


export const saveDoc = async(documents) => {
	const res = await api.documents.saveDocuments(documents);
	return res;
}

export const deleteDoc = (document) => {
	api.documents.delete(document);
}

export const getDocById = (id) =>
	api.documents.fetchDocumentById(id)
					.then( res => { return res } );
