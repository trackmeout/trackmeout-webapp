import React, { Component } from "react";
import { Route, Switch, Link } from "react-router-dom";
import DashboardPage from "../../pages/DashboardPage";
import PatientPage from "../../pages/PatientPage";
import ProcessusPage from "../../pages/ProcessusPage";
import SignupPage from "../../pages/SignupPage";
import TemplateEditorPage from "../../pages/TemplateEditorPage";
import AboutPage from "../../pages/AboutPage";
import { Avatar, Breadcrumb, Icon, Layout, Menu, Dropdown } from "antd";
import logo from "../../media/img/logo.png";
import LoginPage from "../../pages/LoginPage";
import {setLanguage} from "redux-i18n";
import {PropTypes} from "prop-types";
import {connect} from "react-redux";
import UserRoute from "../../routes/UserRoute";
import { logout } from "../../actions/UserAction";
import { bindActionCreators } from 'redux';
const {Header, Sider, Content, Footer} = Layout;

class TmoDashboard extends Component {

	constructor(props) {
		super(props);
		//
    this.pages = [
      {"Name":"","ActiveComponent": React.createFactory(DashboardPage) },
      {"Name":"Liste des patients","ActiveComponent": React.createFactory(()=><PatientPage setPage={this.onClickMenu} setPatient={this.setPatient}  />) },
    	{"Name":"","ActiveComponent": React.createFactory(TemplateEditorPage) },
    	{"Name":"","ActiveComponent": React.createFactory(SignupPage) },
      {"Name":"Liste des patients","ActiveComponent": React.createFactory(()=><ProcessusPage patientID={this.state.userClicked} />) },
      {"Name":"","ActiveComponent": React.createFactory(AboutPage) },
    ]

		this.state = {
			collapsed: false,
			pages: this.pages[0],
      userClicked : "",
      nameUserClicked : ""
		};
		this.selectedKey = 0;
		this.onCollapse = this.onCollapse.bind(this);
		this.onClickMenu = this.onClickMenu.bind(this);
	}

	componentWillMount() {
		if(this.props.lang === "" || this.props.lang === "en")
			this.props.dispatch(setLanguage("Français"));
	}


/**
 * Unknown - Description
 *
 * @param {type} collapsed Description
 *
 * @return {type} Description
 */
	onCollapse = (collapsed) => {
		this.setState({collapsed});
	}


	/**
	* Unknown - Description
	*
	* @return {type} Description
	*/
	onClickMenuUser = () => {
		this.props.logout();
	}


/**
* when you change language, change the language
*
* @param {type} e Description
*
* @return {type} Description
*/
	onChangeLanguage = (e) => {
		this.props.dispatch(setLanguage(e.target.value));
	}


/**
* action when clicking on a menu
*
* @param {type} e Description
*
* @return {type} Description
*/
	onClickMenu = (e) => {
		this.selectedKey = e.key;
		console.log(this.selectedKey);
    	if(this.selectedKey !== 5){
      this.setState({
        nameUserClicked: "",
        pages: this.pages[this.selectedKey-1]
      });
    }
    else{
      this.setState({pages: this.pages[this.selectedKey-1]});
    }
	}


/**
* set patient state
*
* @param {type} id   Description
* @param {type} name Description
*
* @return {type} Description
*/
  setPatient = (id, name) => {
		this.setState({
      userClicked: id,
      nameUserClicked: name,
      pages: this.pages[4]
    });
	}


 /**
  * method handling the breadcrumb
  *
  * @return {type} Description
  */
	handleBread = () => {
		const _this = this;
		this.pages.findIndex(function(item, i){
		   if(item.Name == _this.state.pages.Name){
			 		_this.onClickMenu({"key":i+1});
					return true;
				}
		});

	}

	 render() {
		const user = {
			firstName: this.props.firstname,
			lastName: this.props.lastname,
		};

		const menuUser = (
		  <Menu onClick={this.onClickMenuUser}>
		    <Menu.Item key="1">Logout</Menu.Item>
		  </Menu>
		);

		 if(this.selectedKey == 2 || this.selectedKey == 4){
			 var name = this.context.t("List of patients");
		 }

		return (
			<Layout>
				<Header style={{background: "#fff", padding: 0}}>
					<div className=" headerLogoMenu ">
						<img src={logo} alt="logo" className=" logoTmo "/>
					</div>
					<div className=" headerUser ">
						<select className="select-style" style={{marginRight: "30px", width:"80px" }} value={this.props.lang} onChange={this.onChangeLanguage}>
							{ this.props.languages.map(lang => <option key={lang} value={lang}> {lang} </option>) }
						</select>
						<Avatar style={{verticalAlign: "middle", marginRight: "5px"}}>{this.props.firstname[0].toUpperCase()+this.props.lastname[0].toUpperCase()}</Avatar>
						<Dropdown overlay={menuUser} >
							<a style={{marginRight: "40px"}}> {user.firstName} {user.lastName}</a>
						</Dropdown>
					</div>
				</Header>
				<Layout style={{minHeight: "calc(100vh - 70px)"}}>

					<Sider
						collapsible
						breakpoint="lg"
						collapsed={this.state.collapsed}
						onCollapse={this.onCollapse}
					>
						<div className="logo"/>
						//Menu of the site
						<Menu theme="dark" mode="inline" defaultSelectedKeys={['1']} onClick= {this.onClickMenu}>
							<Menu.Item key="1" >
								<div>
									<Icon type="line-chart" />
									<span className="nav-text">{(this.context.t("Dashboard"))}</span>
								</div>
							</Menu.Item>
							<Menu.Item key="2">
								<div>
									<Icon type="user"/>
									<span className="nav-text">{(this.context.t("Patients"))}</span>
								</div>
							</Menu.Item>
							<Menu.Item key="3">
								<div>
									<Icon type="edit" />
									<span className="nav-text">{(this.context.t("Management"))}</span>
								</div>
							</Menu.Item>
							<Menu.Item key="4" >
								<div>
									<Icon type="team" />
									<span className="nav-text">{(this.context.t("Add User"))}</span>
								</div>
							</Menu.Item>
							<Menu.Item key="6" >
								<div>
									<Icon type="question-circle-o" />
									<span className="nav-text">{(this.context.t("About"))}</span>
								</div>
							</Menu.Item>
						</Menu>
					</Sider>
					<Layout>
						<Content style={{ padding: "30px" , paddingTop: "60px"}}>
							<Breadcrumb style={{ marginBottom: "15px" }}>
								<Breadcrumb.Item onClick={this.handleBread} style={{ cursor: "pointer" }}>{name}</Breadcrumb.Item>
                { this.state.nameUserClicked !== "" && <Breadcrumb.Item>{this.state.nameUserClicked}</Breadcrumb.Item>}
							</Breadcrumb>
							{this.state.pages.ActiveComponent()}
						</Content>
					</Layout>
				</Layout>
			</Layout>
		);
	}
}

TmoDashboard.contextTypes = {
	t: PropTypes.func.isRequired,
};

function mapStateToProps(state) {
	return {
		lang: state.i18nState.lang,
		firstname: state.user.firstname,
		lastname: state.user.lastname,

	};
}

function mapDispatchToProps(dispatch) {
    const actionCreators = bindActionCreators({
        logout,
    }, dispatch);
    return { ...actionCreators, dispatch };
}


export default connect(mapStateToProps, mapDispatchToProps) (TmoDashboard);
