import React, {Component} from "react";
import { Button, message } from "antd";
import { editProcess } from "../../actions/ProcessesAction";
import { createModule } from "../../actions/ModuleAction";
import { createForm } from "../../actions/FormAction";
import  TmoInputText  from "../common/TmoInputText";
import PropTypes from "prop-types";
import {connect} from "react-redux";

export const EDIT_PROCESS = "EDIT_PROCESS";

class TmoNewTreeElementComponent extends Component {

	/* Constructions
    **********************************/
	constructor(props) {
		super(props);
		this.type = this.props.type;
		this.parentId = this.props.parentId;
		this.name = "";
		this.input = null;

	}

	getData = (newValue) =>{
		this.name = newValue;
	}

	/* Fonctions du composant
    **********************************/
	//Return text on click
		getBtnName = ()=> {
			switch (this.type) {
			case EDIT_PROCESS : // renommer le processus
				return this.context.t("Rename process");
			case "module":
				return this.context.t("Create module");
			case "form":
				return this.context.t("Create form");
			default:
			}

		}
		getPlaceHolder = ()=> {
			let data = {
					 hintColor: "#999",
					 focusLabelColor: "#108ee9",
					 widthInput: "250px"
				 };
			switch (this.type) {
			//if input is empty, send error
			case "process":
				data.label = this.context.t("Process name");
				data.messageError = this.context.t("No value has been set");
				return data;
			case "module":
				data.label = this.context.t("Module name");
				data.messageError = this.context.t("No value has been set");
				return data;
			case "form":
				data.label = this.context.t("Form name");
				data.messageError = this.context.t("No value has been set");
				return data;
			default:
			}
		};

		getBtnOnClick = ()=> {

			switch (this.type) {
			case "process":
				if(this.name !== ""){
					const process = {
			    		name: this.name,
			    		owner: this.parentId,
			    	};
					//edit process and confirm if success or send error message if failed
					editProcess( process );
					message.success(this.context.t("The process : ") + process.name + this.context.t(" was created correctly."));
					this.props.closeFunction();
					this.input.setInputValue("");
					//this.props.refreshFunction();
				}else{
					this.input.showError();
				}
				return;

			case "module":
				if(this.name !== ""){
					const module = {
						name: this.name,
						parentProcess: this.parentId,
					};
					//create module and confirm if success or send error message if failed
					createModule( module );
					message.success(this.context.t("The module : ") + module.name + this.context.t(" was created correctly."));
					this.props.closeFunction();
					this.input.setInputValue("");
				}else{
					this.input.showError();
				}
				return;

			case "form":
				if(this.name !== ""){
					const form = {
						name: this.name,
						parentModule: this.parentId,
					};
					//create form and confirm if success or send error message if failed
					createForm( form );
					message.success(this.context.t("The form : ") + form.name + this.context.t(" was created correctly."));
					this.props.closeFunction();
					this.input.setInputValue("");
					//this.props.refreshFunction();
				}else{
					this.input.showError();
				}
				return;
			default:
			}
		}

	/* Render
      **********************************/
  	render() {
			const inputNewTreeElement = <TmoInputText  ref={(child) => { this.input = child; }} data = {this.getPlaceHolder()} sendData={ this.getData } />;
  		return (
  			<div>
					<div style={{ marginBottom: 16, marginTop: 15 }}>
						{inputNewTreeElement}
    			</div>
					 <Button type="primary"  onClick={this.getBtnOnClick}>{this.getBtnName()}</Button>
  			</div>
  		);
  	}
}

TmoNewTreeElementComponent.contextTypes = {
	t: PropTypes.func.isRequired
};

function mapStateToProps(state) {
	return {
		lang: state.i18nState.lang
	};
}

export default connect(mapStateToProps) (TmoNewTreeElementComponent);
