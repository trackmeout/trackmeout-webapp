import React, { Component } from "react";
import { Select, Button, Modal, Form, Input, DatePicker } from 'antd';
import TmoCreateForm from "./TmoCreateForm";
import randomstring from "randomstring";
import moment from 'moment';
import 'moment/locale/fr-ch';
import {PropTypes} from "prop-types";
import {connect} from "react-redux";
import {setLanguage} from "redux-i18n";
import { createPatient, fetchPatients} from "../../actions/PatientAction";
import { allPatientsSelector } from "../../reducers/PatientsReducer";
import createHistory from 'history/createBrowserHistory';
const history = createHistory();
const FormItem = Form.Item;
const Option = Select.Option;
moment.locale('fr')

class TmoNewPatient extends Component {
	constructor(props) {
		super(props);
		this.state = {
			visible: false,
			success: false,
			patient: {},
		};
	}

    showModal = () => {
    	this.setState({
    		visible: true,
    		success:false,
    	});
    }

		okConfirm = () => {
    	this.setState({
    		visible: false,
    		success:false,
    	});
			this.props.fetchPatients();
    }

    handleCancel = () => {
    	const form = this.form;
    	form.resetFields();
    	this.setState({
    		visible: false,
    		success:false, });
    }

    //Generate a login and a password
    handleCreate = () => {
    	const form = this.form;
			const _this = this;
    	form.validateFields((err, values) => {
    		if (err) {
    			return;
    		}
				var bd = values.birthDate;
				var dateBD = bd.toDate();
				var newDate = moment(dateBD).format("DD.MM.YYYY");
    		var firstName = values.firstName;
    		var lastName = values.lastName;
    		values["login"]=firstName.substring(0, 1)+"."+lastName;
    		values["pass"]=randomstring.generate(8);
    		values["birthDate"]=newDate;

    		form.resetFields();

				this.props.createPatient(values);

				this.setState({
					visible: false,
	    		success: true,
					patient:values,
				});
    	});
    }


    saveFormRef = (form) => {
    	this.form = form;
    }

    render() {
			const data = this.state.patient;
			const modalContent =  <div>
				<h1>{this.context.t("Information")}</h1>
				<h3>{this.context.t("Name : ") + data.lastName}</h3>
				<h3>{this.context.t("Firstname : ") + data.firstName}</h3>
				<h3>{this.context.t("Date de naissance : ") + data.birthDate}</h3>
				<br></br>
				<h3>{this.context.t("Login : ") + data.login}</h3>
				<h3>{this.context.t("Password : ") + data.pass}</h3>
			</div>;
    	const successVisible = this.state.success;
			const CollectionCreateForm = Form.create()(
				//Popup to create a new patient
				(props) => {
					const { visible, onCancel, onCreate, form } = props;
					const { getFieldDecorator } = form;
					return (
						<Modal
							visible={visible}
							title= {this.context.t("Adding a new patient")}
							okText={this.context.t("Create")}
							onCancel={onCancel}
							onOk={onCreate}
						>
							<Form layout="vertical">
								<FormItem label={this.context.t("Name")}>
									{getFieldDecorator("lastName", {
										rules: [{ required: true, message: this.context.t("Please enter a name") }],
									})(
										<Input />
									)}
								</FormItem>
								<FormItem label={this.context.t("Firstname")}>
									{getFieldDecorator("firstName", {
										rules: [{ required: true, message: this.context.t("Please entre a firstname") }],
									})(
										<Input />
									)}
								</FormItem>
								<FormItem label={this.context.t("Birthdate")}>
									{getFieldDecorator("birthDate", {
										rules: [{ required: true, message: this.context.t("Please enter a date of birth") }],
									})(
			              <DatePicker format="DD-MM-YYYY" placeholder={this.context.t("Select a date")}/>
									)}
								</FormItem>
								<FormItem label={this.context.t("Gender")}
								>
									{getFieldDecorator("gender", {
										rules: [{ required: true, message: this.context.t("Please select your gender!") }],
									})(
										<Select
											placeholder={this.context.t("Select sex")}
										>
											<Option value="0">{this.context.t("Man")}</Option>
											<Option value="1">{this.context.t("Woman")}</Option>
										</Select>
									)}
								</FormItem>
							</Form>
						</Modal>
					);
				}
			);
    	return (
    		<div>
    			<Button type="default" style={{position: "absolute", right: "30px", "top": "80px"}} onClick={this.showModal}>{this.context.t("Add a patient")}</Button>
    			<CollectionCreateForm
    				ref={this.saveFormRef}
    				visible={this.state.visible}
    				onCancel={this.handleCancel}
    				onCreate={this.handleCreate}
    			/>
					<Modal
						visible={this.state.success}
						title= {this.context.t("Adding a new patient")}
						// title={this.context.t("Patient created successfully")}
						onOk={this.okConfirm}
					>
					{modalContent}
    			</Modal>
    		</div>
    	);
    }
}

TmoNewPatient.contextTypes = {
	t: PropTypes.func.isRequired
};

function mapStateToProps(state) {
	return {
		patients: allPatientsSelector(state),
		lang: state.i18nState.lang
	};
}

export default connect(mapStateToProps, {createPatient, fetchPatients}) (TmoNewPatient);
