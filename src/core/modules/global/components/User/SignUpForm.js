import React from "react";
import { message, Form, Input, Tooltip, Icon, Cascader, Select, Row, Col, Checkbox, Button, AutoComplete } from 'antd';
import { createUser } from "../../actions/UserAction";
import InlineError from "../User/InlineError";
import PropTypes from "prop-types";
import SignupPage from "../../pages/SignupPage";

const FormItem = Form.Item;
const Option = Select.Option;
const AutoCompleteOption = AutoComplete.Option;

message.config({
  top: 50,
  duration: 2,
});

class RegistrationForm extends React.Component {
  state = {
    confirmDirty: false,
    user : null,
    errors: {}
  };

  onSubmit = (e) => {
    e.preventDefault();
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        createUser(values).then(res => {
          if(res.hasOwnProperty('user')){
            message.success(this.context.t("User created correctly"));
            this.props.form.resetFields();
          }
          else{
            message.error(this.context.t("This email already exists"));
          }
        });
      }
    });
  }

  handleConfirmBlur = (e) => {
    const value = e.target.value;
    this.setState({ confirmDirty: this.state.confirmDirty || !!value });
  }

  compareToFirstPassword = (rule, value, callback) => {
    const form = this.props.form;
    if (value && value !== form.getFieldValue('password')) {
      callback(this.context.t("The two passwords are not alike."));
    } else {
      callback();
    }
  }

  validateToNextPassword = (rule, value, callback) => {
    const form = this.props.form;
    if (value && this.state.confirmDirty) {
      form.validateFields(['confirm'], { force: true });
    }
    callback();
  }

  render() {
      const { errors } = this.state;
    const { getFieldDecorator } = this.props.form;
    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 8 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 16 },
      },
    };
    const tailFormItemLayout = {
      wrapperCol: {
        xs: {
          span: 24,
          offset: 0,
        },
        sm: {
          span: 16,
          offset: 8,
        },
      },
    };

    return (
      <Form onSubmit={this.handleSubmit}>

        <FormItem {...formItemLayout} label="E-mail">
          {getFieldDecorator('email', {
            rules: [{
              type: 'email', message: this.context.t("Please enter a valid email address."),
            }, {
              required: true, message: this.context.t("Please enter an e-mail address."),
            }],
          })(
            <Input />
          )}
        </FormItem>

        <FormItem {...formItemLayout} label={this.context.t("Name")}>
          {getFieldDecorator('lastName', {
            rules: [{
              message: this.context.t("Please enter your name."), whitespace: true
            }, {
              required: true, message: this.context.t("Please put your name."),
            }],
          })(
            <Input />
          )}
        </FormItem>

        <FormItem {...formItemLayout} label={this.context.t("Firstname")}>
          {getFieldDecorator('firstName', {
            rules: [{
              message:  this.context.t("Please enter your firstname."), whitespace: true
            }, {
              required: true, message: this.context.t("Please put your firstname."),
            }],
          })(
            <Input />
          )}
        </FormItem>

        <FormItem {...formItemLayout} label={(<span>
                {this.context.t("Password")}&nbsp;
              <Tooltip title= {this.context.t("Min 7 characters")}>
                <Icon type="question-circle-o" />
              </Tooltip>
            </span> )}>
          {getFieldDecorator('password', {
            rules: [{
              required: true,  min:7, message: this.context.t("Please enter a valid password"),
            }, {
              validator: this.validateToNextPassword,
            }],
          })(
            <Input type="password"/>
          )}
        </FormItem>

        <FormItem {...formItemLayout} label={this.context.t("Confirm password")}>
          {getFieldDecorator('confirm', {
            rules: [{
              required: true, message: this.context.t("Please confirm your password"),
            }, {
              validator: this.compareToFirstPassword,
            }],
          })(
            <Input type="password" onBlur={this.handleConfirmBlur} />
          )}
        </FormItem>

        <FormItem {...tailFormItemLayout}>
          <Button type="primary" onClick={this.onSubmit}>{this.context.t("Create account")}</Button>
        </FormItem>


      </Form>
    );
  }
}

RegistrationForm.contextTypes = {
	t: PropTypes.func.isRequired
};

function mapStateToProps(state) {
	return {
		lang: state.i18nState.lang
	};
}


export default (Form.create())(RegistrationForm);
