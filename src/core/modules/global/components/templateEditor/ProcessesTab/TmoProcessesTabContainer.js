/* eslint-disable no-restricted-globals */
import React, {Component} from "react";
import {Menu, Icon, Card, Button, Dropdown } from "antd";
import {connect} from "react-redux";
import TmoTreeElementModalComposant, {
	ADD_FORM_TO_MODULE_TEMPLATE_MODAL, ADD_MODULE_TEMPLATE_MODAL,
	ADD_PROCESS_TEMPLATE_MODAL, ADD_MODULE_TO_PROCESS_TEMPLATE_MODAL, EDIT_PROCESS_MODAL, CONVERT_MODULE_MODAL,
	CONVERT_FORM_MODAL, CONVERT_PROCESS_MODAL
} from "../../processus/TmoTreeElementModalComposant";
import TmoProcessMenu from "./TmoProcessMenu";
import TmoModuleComponent from "../../processus/TmoModuleComponent";
import {processTemplateSelector} from "../../../reducers/ProcessReducer";
import {modulesTemplateSelector} from "../../../reducers/ModulesReducer";
import {formsByIdSelector, formsTemplateSelector} from "../../../reducers/FormReducer";
import {fetchProcessTemplates, addModuleTempToProcessTemp} from "../../../actions/ProcessesAction";
import {fetchFormTemplates} from "../../../actions/FormAction";
import {fetchModuleTemplatesByParentsId, fetchModuleTemplates, addFormToModule} from "../../../actions/ModuleAction";
import {fetchFormById} from "../../../actions/FormAction";
import {Collapse} from "react-collapse";
import _ from "lodash";
import PropTypes from "prop-types";
import TmoFormComponent from "../../processus/TmoFormComponent";
import {TEMPLATE_MODE} from "../../../Types";
import {ARCHIVE_PROCESS_MODAL_TEMPLATE, DELETE_MODULE_MODAL} from "../../common/TmoConfirmModal";
import TmoConfirmModal from "../../common/TmoConfirmModal";

var modulesList = null;

class TmoProcessesTabContainer extends Component {
	constructor(props) {
		super(props);
		this.state = {
			selectedProcess: null,
			selectedModule: null,
			isLoadingProcesses: true,
			isLoadingModules: true,
			isLoadingForms: true,
		};
		this.addNewProcessModal = null;
		this.addNewModuleModal = null;
		this.processTemplates = [];
		this.moduleTemplates = [];
		this.formTemplates = [];
		this.availableModules = [];
		this.shouldOpenProcess = true;
		this.currentProcess = null;
	}

	componentWillMount = () => {
		this.refreshPageFunction();
	};

	onProcessSelect = (item) => {

		console.log(this.shouldOpenProcess);
		if(this.shouldOpenProcess) {

			const _this = this;
			let processSelected = null;
			_.each(_this.processTemplates, (currentProcess) => {
				if (currentProcess._id === item.key) {
					processSelected = currentProcess;
				}
			});

			fetchModuleTemplatesByParentsId(item.key).then((modules => {
				_this.availableModules = modules;
				this.setState({
					selectedProcess: processSelected,
					selectedModule: null
				});
			}));
		}
		this.shouldOpenProcess = true;
	};

	backToProcess = () => {
		this.setState({
			selectedProcess: null,
			selectedModule: null
		});
	};

	onModulesSelect = (item) => {
		console.log("dddd");
		let moduleSelected = null;
		this.availableModules.map((currentModule) => {
			if (currentModule._id === item.key) {
				moduleSelected = currentModule;
			}
		});

		if (this.state.selectedModule && this.state.selectedModule.id == item.key) {
			moduleSelected = null;
		}

		this.setState({selectedModule: moduleSelected});
	};
	refreshPageFunction = () => {
		console.log("refresh");
		const _this = this;
		_this.props.fetchProcessTemplates().then((item) => {
			_this.processTemplates = item.data.entities.processes;
			_this.setState({isLoadingProcesses: false});
		});

		_this.props.fetchFormTemplates().then(item => {
			_this.formTemplates = _.values(item.data.entities.forms);
			_this.setState({isLoadingForms: false});
		});

		_this.props.fetchModuleTemplates().then(item => {
			_this.moduleTemplates = _.values(item.data.entities.modules);
			_this.setState({isLoadingModules: false});
		});
		if (!!this.state.selectedProcess) {
			fetchModuleTemplatesByParentsId(this.state.selectedProcess.id).then((modules => {
				_this.availableModules = modules;
				if (!!_this.state.selectedModule) {
					var moduleSelected = null;
					_this.availableModules.map((currentModule) => {
						if (currentModule._id === _this.state.selectedModule._id) {
							moduleSelected = currentModule;
						}
					});
					_this.setState({selectedModule: moduleSelected});
				} else {
					_this.setState({selectedModule: null});
				}


			}));
		}
	};

	onTryToAddModule = (moduleData) => {
		const _this = this;
		let id = this.state.selectedProcess.id;
		let data = moduleData;
		addModuleTempToProcessTemp(id, data);
		setTimeout(() => {
			_this.refreshPageFunction();
		}, 80);

	};

	onTryToAddFormToModule = (formData) => {
		const _this = this;
		addFormToModule({moduleId: this.state.selectedModule.id, formId: formData._id}).then((res) => {
			if (res.data.success === true) {
				_this.refreshPageFunction();
			}
		});
	};

	getCurrentProcess = () => {
		return this.currentProcess;
	}

	addConvertScrollEvent = (element) => {
		if (element) {
			var mouseSpeed = 0.75;
			element.addEventListener("mousewheel", (e) => {
				e.preventDefault();
				element.scrollLeft += (e.deltaY * mouseSpeed);
			});
		}
	};

	treeViewRightClick = (cp) => {
		console.log("3");
		this.currentProcess = cp;
		console.log(cp);

	};

	openTreeModal = (element) => {
		console.log("2");
		console.log("openTreeModal : " ,this.currentProcess.id);
		console.log("openTreeModal ", element);

		switch (element.item.props.modalType) {
		case EDIT_PROCESS_MODAL:

			this.editProcessModal.setState({
				isVisible: true,
			});
			break;
		case ARCHIVE_PROCESS_MODAL_TEMPLATE:
			this.deleteProcessModal.setState({
				isVisible: true,
			});
			break;
		default:
			console.error("NON GERE")
		}

		this.shouldOpenProcess = false;
	}

	render() {
		console.log("RENDER");
		const _this = this;
		let cardTitle;

		const menu = (
			<Menu onClick={this.openTreeModal}>
				<Menu.Item modalType={EDIT_PROCESS_MODAL} key="1">{this.context.t("Rename process")}</Menu.Item>
				<Menu.Item modalType={ARCHIVE_PROCESS_MODAL_TEMPLATE} key="2">{this.context.t("Delete process")}</Menu.Item>
			</Menu>
		);

		if (this.state.isLoadingProcesses || this.state.isLoadingModules || this.state.isLoadingForms) {
			return <div>{this.context.t("Loading... ")}</div>;
		}

		//Generate list
		let processesToShow = [];
		let modulesToShow = [];
		_.each(_this.processTemplates, (currentProcess) => {
			processesToShow.push(


				<Menu.Item key={currentProcess.id}
							   onClick={this.toggleCollapsed}>
					<Dropdown  onVisibleChange={this.visibilityChange} overlay={menu} trigger={["contextMenu"]}>
						<span key={currentProcess.id} onContextMenu={(() => this.treeViewRightClick(currentProcess))} className="tree-menu-line" style={{ userSelect: "none",  width:"175px" }}>{currentProcess.name}</span>
					</Dropdown>
				</Menu.Item>


			);
		});

		const formTemplatesIdsAlreadyAdded = [];
		let currentModuleForms = [];
		let modulesTab;

		const currentFormTemplates = this.formTemplates.map(function (form) {
			return (
				<TmoFormComponent isPartOfOneTemplate={true} onClickOnCard={() => _this.onTryToAddFormToModule(form)}
								  refreshPageFunction={_this.refreshPageFunction} key={form._id} data={form}/>);
		});

		if (this.state.selectedProcess !== null) {
			modulesToShow = this.availableModules.map((currentModule) => {
				return <Menu.Item key={currentModule.id}>{currentModule.name}</Menu.Item>;
			});

			cardTitle = (
				<div><a onClick={this.backToProcess}><Icon type="left"/> {this.state.selectedProcess.name}</a></div>);

			if (this.state.selectedModule) {

				console.log(this.state.selectedModule.forms);
				currentModuleForms = this.state.selectedModule.forms.map(function (form) {

					_.each(_this.formTemplatesList, function (currentTemplate) {
						formTemplatesIdsAlreadyAdded.push(currentTemplate._id);
					});

					return (<TmoFormComponent isPartOfOneTemplate={true} refreshPageFunction={() => {
					}} key={form._id} data={form}/>);
				});
			}


			if (!!this.state.selectedModule) {
				modulesTab = (
					<div style={{overflow: "auto"}} className="process-template-selected-module-container">
						<div className="tmo-relative-full-container">
							<div className="module-template-forms-in-module-list">
								<Card title={this.state.selectedModule.name}
									  bordered={false}
									  extra={<Button onClick={() => {
										  this.addNewFormToModuleModal.setState({isVisible: true});
									  }} icon="plus"
													 size={"default"}>{this.context.t("Add a form to this module")}</Button>}>

									<TmoTreeElementModalComposant refreshPageFunction={_this.refreshPageFunction}
																  param={this.state.selectedModule._id}
																  ref={(child) => {
																	  _this.addNewFormToModuleModal = child;
																  }} modalType={ADD_FORM_TO_MODULE_TEMPLATE_MODAL}
																  modalData={this.state.selectedModule.id}/>

									<div ref={(child) => {
										_this.wrapperContent1 = child;
									}} className="tmo-flex-wrapper-content">
										{currentModuleForms}
									</div>

								</Card>
							</div>
							<div className="module-template-forms-template-list tmo-flex-container-row">
								<Card title={this.context.t("Forms available")} bordered={false}>
									<div ref={(child) => {
										_this.wrapperContent2 = child;
									}} className="tmo-flex-wrapper-content">
										{currentFormTemplates}
									</div>
								</Card>
							</div>
						</div>
					</div>
				);
			} else {

				//Charge les modules a afficher
				modulesTab = this.moduleTemplates.map(function (module) {
					return (<TmoModuleComponent isPartOfOneTemplate={true} refreshPageFunction={_this.refreshPage}
						key={module._id} currentMode={TEMPLATE_MODE}
						onSelect={_this.onTryToAddModule} data={module}/>);
				});
			}
		} else {
			cardTitle = this.context.t("Process Templates");
			modulesTab = <div style={{height: "100%", fontSize: "18px"}} className="aligner"><span
				className="align-center">{this.context.t("Veuillez choisir un processus")}</span></div>;
		}

		const btnAddModule = <Button onClick={() => {
			this.addNewModuleModal.setState({isVisible: true});
		}} style={{border: "none", width: "100%", textAlign: "left", marginTop: "10px"}}
									 icon="plus"
									 size={"default"}> {this.context.t("Nouveau Module")}</Button>;

		let selectedModuleKey = !!this.state.selectedModule ? this.state.selectedModule._id : null;

		if (!!this.state.selectedModule) {
			setTimeout(function () {
				_this.addConvertScrollEvent(_this.wrapperContent1);
				_this.addConvertScrollEvent(_this.wrapperContent2);
			}, 100);
		}

		return (
			<div id="template-editor-process-container" className="tmo-flex-container">
				<div className="tmo-flex-wrapper">
					<div className="tmo-flex-wrapper-menu">
						<Card title={cardTitle} bordered={false}>
							<div>

								<Collapse isOpened={!this.state.selectedProcess}>
									<Menu
										forceSubMenuRer={true}
										selectedKeys={[]}
										onClick={this.onProcessSelect}
										mode="inline"
										style={{marginTop: "10px"}}>
										{processesToShow}
									</Menu>
									<Button onClick={() => {
										this.addNewProcessModal.setState({isVisible: true});
									}} style={{border: "none", width: "100%", textAlign: "left", marginTop: "10px"}}
									icon="plus"
									size={"default"}> {this.context.t("Nouveau Processus")}</Button>
								</Collapse>

								<Collapse isOpened={!this.state.moduleSelected}>
									<Menu
										onClick={this.onModulesSelect}
										selectedKeys={[selectedModuleKey]}
										mode="inline"
										style={{marginTop: "10px"}}>
										{modulesToShow}
									</Menu>
									{!!this.state.selectedProcess ? btnAddModule : ""}
								</Collapse>

								<TmoTreeElementModalComposant refreshPageFunction={_this.refreshPageFunction}
															  ref={(child) => {
																  _this.editProcessModal = child;
															  }}  modalType={EDIT_PROCESS_MODAL} modalData={ this.getCurrentProcess }/>

								<TmoConfirmModal refreshPageFunction={_this.refreshPageFunction} ref={(child) => {_this.deleteProcessModal = child;}} modalData={_this.getCurrentProcess} modalType={ARCHIVE_PROCESS_MODAL_TEMPLATE} />

								<TmoTreeElementModalComposant refreshPageFunction={this.refreshPageFunction}
															  ref={(child) => {
																  _this.addNewProcessModal = child;
															  }} modalType={ADD_PROCESS_TEMPLATE_MODAL}/>

								<TmoTreeElementModalComposant refreshPageFunction={this.refreshPageFunction}
															  parentElement={this}
															  ref={(child) => {
																  _this.addNewModuleModal = child;
															  }} modalType={ADD_MODULE_TO_PROCESS_TEMPLATE_MODAL}/>
							</div>
						</Card>
					</div>
					<div className="forms-template-list">
						{modulesTab}
					</div>
				</div>
			</div>
		);
	}
}

TmoProcessesTabContainer.contextTypes = {
	t: PropTypes.func.isRequired
};

function mapStateToProps(state) {
	return {
		processes: processTemplateSelector(state),
		modules: modulesTemplateSelector(state),
		forms: formsTemplateSelector(state),
		// formsFromModule: formsByIdSelector(state),
	};
}

export default connect(mapStateToProps, {
	fetchProcessTemplates,
	fetchFormTemplates,
	fetchModuleTemplates
})(TmoProcessesTabContainer);
