import React, {Component} from "react";
import { Tabs } from "antd";
import { connect } from "react-redux";
import TmoModulesTabContainer from "../components/templateEditor/ModulesTab/TmoModulesTabContainer";
import TmoFormsTabContainer from "../components/templateEditor/FormsTab/TmoFormsTabContainer";
import TmoProcessesTabContainer from "../components/templateEditor/ProcessesTab/TmoProcessesTabContainer";
import PropTypes from "prop-types";
import {allPatientsSelector} from "../reducers/PatientsReducer";

const TabPane = Tabs.TabPane;
const PROCESSES_TAB = "PROCESSES_TAB";
const MODULES_TAB = "MODULES_TAB";
const FORMS_TAB = "FORMS_TAB";



class TemplateEditorPage extends Component {

	constructor(props) {
		super(props);
		this.defaultActiveTab = PROCESSES_TAB;
	}
	//Switch tab function
	switchTab = (key) => {
		console.log(key);
	};

	render() {
		return (
			//render of the tabpane with each component (tasks, process, modules)
			<div className="tmo-relative-full-container">
				<div id="template-editor">
					<Tabs defaultActiveKey={this.defaultActiveTab} onChange={this.switchTab} tabBarStyle={{background:"white"}}>
						<TabPane tab={this.context.t("Process management")} key={PROCESSES_TAB}>
							<TmoProcessesTabContainer/>
						</TabPane>
						<TabPane tab={this.context.t("Module management")} key={MODULES_TAB}>
							<TmoModulesTabContainer />
						</TabPane>
						<TabPane tab={this.context.t("Forms management")} key={FORMS_TAB}>
							<TmoFormsTabContainer/>
						</TabPane>
					</Tabs>
				</div>
			</div>

		);
	}
}

TemplateEditorPage.contextTypes = {
	t: PropTypes.func.isRequired
}

function mapStateToProps(state) {
	return {
		lang: state.i18nState.lang
	};
}

export default connect(mapStateToProps)(TemplateEditorPage);
