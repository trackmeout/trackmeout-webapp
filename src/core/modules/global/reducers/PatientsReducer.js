import {createSelector} from "reselect";
import { PATIENTS_FETCHED, PATIENT_CREATED } from "../Types";

//PATIENT REDUCER
export default function patients(state = {}, action = {}) {
	switch (action.type) {
	case PATIENTS_FETCHED:
		return {...state, ...action.data.entities.patients};
	case PATIENT_CREATED:
		return {...state, ...action.data.entities.patients};
	default:
		return state;
	}
}

// SELECTORS
export const patientsSelector = state => state.patients;
export const allPatientsSelector = createSelector(patientsSelector, PatientsHash =>
	Object.values(PatientsHash)
);
